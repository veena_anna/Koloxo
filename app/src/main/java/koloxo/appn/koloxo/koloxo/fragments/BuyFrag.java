package koloxo.appn.koloxo.koloxo.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Webserives.CountryData;
import koloxo.appn.koloxo.koloxo.Webserives.PojoCountryName;
import koloxo.appn.koloxo.koloxo.Webserives.PojoitemLogin;
import koloxo.appn.koloxo.koloxo.activity.LoginPage;
import koloxo.appn.koloxo.koloxo.activity.MapsActivity_detalprprty;
import koloxo.appn.koloxo.koloxo.adapter.CountrylistAdapter;
import koloxo.appn.koloxo.koloxo.adapter.Spinner_Adapter;
import retrofit2.Call;

import static android.content.Context.LOCATION_SERVICE;
import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainActivity.android_id;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;


public class BuyFrag extends Fragment implements LocationListener {
    Spinner staticSpinner;
    ImageView buy_nxt2,img_location;
    //String[] brewarray = {"Select the country","Dubai","Saudi Arabia","Oman","London"};
    String[] brewarray;
    LocationManager locationManager;

    double lat=0,lon=0;


    public BuyFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buy, container, false);
        staticSpinner = view.findViewById(R.id.static_spinner);
        buy_nxt2 = (ImageView) view.findViewById(R.id.buy_nxt2);

        img_location= (ImageView) view.findViewById(R.id.img_location);



        final Call<JsonObject> countrycall = new Retrofit_Helper().getRetrofitBuilder().getCountry();

        countrycall.enqueue(new ResponseHandler(true, getActivity(), new ResponseCallback() {
            @Override
            public void getResponse(int code, JsonObject jsonObject) {
                Log.v("LoginActivity", jsonObject.toString());
                PojoCountryName countryName = new GsonBuilder().create().fromJson(jsonObject, PojoCountryName.class);
                if (countryName.getErrorCode().equals("0")) {
                    if(countryName.getData().size()>0) {

                        List<CountryData> countryData = countryName.getData();

                        countryData.add(0,new CountryData("0","Select the country",""));

                        CountrylistAdapter countrylistAdapter = new CountrylistAdapter(getActivity(), countryData);

                        staticSpinner.setAdapter(countrylistAdapter);

                    }
                    else {


                        Dialoghelper.showSnackbar(getActivity(), "No data found");

                    }


                       // brewarray = new String[countryName.getData().size()];
//                        for (int i = 0; i < countryName.getData().size(); i++) {
//                            String data = countryName.getData().get(i).getCountry();
//                            brewarray[i] = "\"" + data + "\"";
//
//                        }}
//
//                    // Apply the adapter to the spinner
//                    staticSpinner.setAdapter(new Spinner_Adapter(getActivity(),brewarray));
                   // session_class.createUserRegisterSession(loginData.getData().getUserId(), loginData.getData().getFirstName(), loginData.getData().getLastName(), loginData.getData().getMobileNumber(), loginData.getData().getEmail(), loginData.getData().getImage(), MyConstants.LoggedIn.TRUE,loginData.getData().getImage());
                    Log.d("Koloxo_login", "onResponse: message " + countryName.getMessage());
                    // Toast.makeText(LoginPage.this, "Login Success", Toast.LENGTH_SHORT).show();

                }
                else if (countryName.getErrorCode().equals("1"))
                {
                    Dialoghelper.showSnackbar(getActivity(), countryName.getMessage());
                }
                // session_class.createUserLoginSession(response.body().getData().getUserId());
            }


            @Override
            public void getError(Call<JsonObject> call, String message) {
                Dialoghelper.showSnackbar(getActivity(), message);
            }
        }, countrycall));



        buy_nxt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Search Results");
                //Title.setTypeface(Koloxo);
              /*  Intent intent=new Intent(getActivity(), MapsActivity_detalprprty.class);
                startActivity(intent);*/
               getHpBaseActivity().pushFragments(new MapsActivity_detalprprty(),true,true);
               // Toast.makeText(getActivity(),"View to map",Toast.LENGTH_SHORT).show();
            }
        });



        img_location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.ACCESS_FINE_LOCATION)!=PackageManager.PERMISSION_GRANTED) {

                    RequestRunTimePermission();
                }
                else {

                    getLocation();
                }

            }
        });





        // Inflate the layout for this fragment
        return view;

    }

    // Requesting run time permission method starts from here.
    public void RequestRunTimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {

            Toast.makeText(getActivity(), "READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {

        switch (RC) {

            case 1:

                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {



getLocation();



                    Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(getActivity(), "Permission Canceled", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }



    public void getLocation()
    {
        try {


            LocationManager locManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
if(locManager.isProviderEnabled(LocationManager
        .GPS_PROVIDER)) {


    locManager.requestLocationUpdates(LocationManager
            .GPS_PROVIDER, 5000, 10, BuyFrag.this);
}
else {

    Dialoghelper.showSnackbar(getActivity(),"Please turn on your location");

}

        }catch (SecurityException e)
        {

        }

    }


    @Override
    public void onLocationChanged(Location location) {

        if(location!=null)
        {
            Log.e("Current location",location.getLatitude()+","+location.getLongitude());

            lat=location.getLatitude();
            lon=location.getLongitude();






        }


    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}
