package koloxo.appn.koloxo.koloxo.activity;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.Webserives.PojoProfile;
import koloxo.appn.koloxo.koloxo.adapter.PropertyImgAdapter;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.Webserives.ApiUtils.BASE_URL_PIC;

public class Profile_startup1 extends android.support.v4.app.Fragment {

ImageView Profile_startup1;
    CircleImageView start_profle_propic;
TextView start_profle_name1,start_profle_name2,start_profle_country,start_profle_email,start_profle_mobile;
    Session_class session_class;

    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("My Profile");
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_profile_startup1, container, false);
        session_class=new Session_class(getActivity());
        start_profle_name1=(TextView)view.findViewById(R.id.start_profle_name1);
        start_profle_name2=(TextView)view.findViewById(R.id.start_profle_name2);
        start_profle_country=(TextView)view.findViewById(R.id.start_profle_country);
        start_profle_email=(TextView)view.findViewById(R.id.start_profle_email);
        start_profle_mobile=(TextView)view.findViewById(R.id.start_profle_mobile);
        start_profle_propic=(CircleImageView)view.findViewById(R.id.start_profle_propic);
        Profile_startup1=(ImageView)view.findViewById(R.id.complete_profile_1);

        final Call<JsonObject> profilecall = new Retrofit_Helper().getRetrofitBuilder().getProfilePage(session_class.getId());


        profilecall.enqueue(new ResponseHandler(true, getActivity(), new ResponseCallback() {
            @Override
            public void getResponse(int code, JsonObject jsonObject) {
                Log.d("profiletest", "getResponse: "+ jsonObject.toString());
                PojoProfile pojoProfile=new GsonBuilder().create().fromJson(jsonObject, PojoProfile.class);

                if(pojoProfile.getErrorCode().equals("0")) {
                    start_profle_name1.setText(pojoProfile.getData().getFirstName());
                    start_profle_name2.setText(pojoProfile.getData().getLastName());
                    start_profle_country.setText(pojoProfile.getData().getCountryResidence());
                    start_profle_email.setText(pojoProfile.getData().getEmail());
                    start_profle_mobile.setText(pojoProfile.getData().getMobileNumber());
                    Picasso.with(getActivity()).load(BASE_URL_PIC+session_class.getImag_login()).into(start_profle_propic);



                        Log.d("terms_case", "onResponse: message " + pojoProfile.getMessage());
                    }



                Log.d("terms", "onResponse: message " + pojoProfile.getErrorCode());

            }
            @Override
            public void getError(Call<JsonObject> call, String message) {
                Dialoghelper.showSnackbar(getActivity(), message);
            }
        }, profilecall));


        Profile_startup1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Reputed Profile");
                getHpBaseActivity().pushFragments(new Profile_startup2(), true, true);
            }
        });




return view;
    }
}
