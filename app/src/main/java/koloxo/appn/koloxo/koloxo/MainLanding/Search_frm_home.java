package koloxo.appn.koloxo.koloxo.MainLanding;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.fragments.BuyFrag;
import koloxo.appn.koloxo.koloxo.fragments.RentFrag;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;


public class Search_frm_home extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @SuppressLint("ResourceAsColor")
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Search");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
      //  listener.onFragSelected("Search One");

        View view = inflater.inflate(R.layout.act_search_frm_land, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);

        bottom_bar.setVisibility(View.VISIBLE);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
       CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) view.findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle("Hai Koloxo,welcome");
      /*  collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));*/
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.white));



        return view;
    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new BuyFrag(), "Buy");
        adapter.addFragment(new RentFrag(), "Rent");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }
        @Override
        public int getItemPosition(Object object)
        {
            return POSITION_NONE;
        }
        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
