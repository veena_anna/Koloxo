package koloxo.appn.koloxo.koloxo.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.Webserives.APIs;
import koloxo.appn.koloxo.koloxo.Webserives.ApiUtils;
import koloxo.appn.koloxo.koloxo.Webserives.CountryData;
import koloxo.appn.koloxo.koloxo.Webserives.PojoCountryName;
import koloxo.appn.koloxo.koloxo.Webserives.PojoTerms;
import koloxo.appn.koloxo.koloxo.Webserives.PojoitemReg;
import koloxo.appn.koloxo.koloxo.adapter.CountrylistAdapter;
import koloxo.appn.koloxo.koloxo.adapter.Spinner_Adapter;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.MainActivity.android_id;

public class RegisterPage extends Activitybase {
    TextView sign_in;
    SpannableString spannableString;
    Button registerNow;
    Spinner country_of_residence;
    String[] List_country= new String[]{"Country of Residence","Dubai","saudi"};
    //for web set start
    CheckBox simpleCheckBox;
    APIs apIs;
    Session_class session_class;
    EditText firstname,lastname,mobileno,pass1,confrmpass,email_reg;
    String firstname1,lastname1,mobileno1,pass11,confrmpass1,email_reg1;
    String spin_val;
    String selected_country;
    String selectedText;
    TextView terms_cond;

    //boolean setting
    boolean check_terms;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_page);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        session_class=new Session_class(RegisterPage.this);
        initializingVar();

        country_of_residence=(Spinner) findViewById(R.id.country_of_residence);
        final Call<JsonObject> countrycall = new Retrofit_Helper().getRetrofitBuilder().getCountry();

        countrycall.enqueue(new ResponseHandler(true,RegisterPage.this, new ResponseCallback() {
            @Override
            public void getResponse(int code, JsonObject jsonObject) {
                Log.v("LoginActivity", jsonObject.toString());
                PojoCountryName countryName = new GsonBuilder().create().fromJson(jsonObject, PojoCountryName.class);
                if (countryName.getErrorCode().equals("0")) {
                    if(countryName.getData().size()>0) {

                        List<CountryData> countryData = countryName.getData();

                     countryData.add(0,new CountryData("0","Country of Residence",""));
                       // country_of_residence.setPrompt("Country of Residence");
                        CountrylistAdapter countrylistAdapter = new CountrylistAdapter(RegisterPage.this, countryData);

                        country_of_residence.setAdapter(countrylistAdapter);

                    }
                    else {


                        Dialoghelper.showSnackbar(RegisterPage.this, "No data found");

                    }



                    Log.d("Koloxo_login", "onResponse: message " + countryName.getMessage());
                    // Toast.makeText(LoginPage.this, "Login Success", Toast.LENGTH_SHORT).show();

                }
                else if (countryName.getErrorCode().equals("1"))
                {
                    Dialoghelper.showSnackbar(RegisterPage.this, countryName.getMessage());
                }
                // session_class.createUserLoginSession(response.body().getData().getUserId());
            }


            @Override
            public void getError(Call<JsonObject> call, String message) {
                Dialoghelper.showSnackbar(RegisterPage.this, message);
            }
        }, countrycall));


        //country_of_residence.setAdapter(new Spinner_Adapter(this,List_country));
 country_of_residence.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
     @Override
     public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
          selectedText=List_country[i];
         if(!selectedText.equals("Country of Residence")) {
             selected_country=selectedText;
           //  Toast.makeText(getApplicationContext(), selected_country, Toast.LENGTH_SHORT).show();
         }
     }

     @Override
     public void onNothingSelected(AdapterView<?> adapterView) {

     }
 });

        spannableString=new SpannableString("Sign in here");
        spannableString.setSpan(new UnderlineSpan(),0,spannableString.length(),0);
        sign_in.setText(spannableString);
        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(RegisterPage.this,LoginPage.class);
                startActivity(intent);
                finish();
            }
        });




        simpleCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (simpleCheckBox.isChecked()) {

                    check_terms=true;

                }
                else {
                    check_terms=false;


                    Dialoghelper.showSnackbar(RegisterPage.this, "Please accept terms and condition");
                }


            }
        });
        registerNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (firstname.getText().length() == 0)
                    firstname.setError("First name required!");
                if (lastname.getText().length() == 0)
                    lastname.setError("Last name required!");
                if (mobileno.getText().length() == 0)
                    mobileno.setError("Mobile no. required!");
                if (email_reg.getText().length() == 0)
                    email_reg.setError("Please enter email ID!");
                if (pass1.getText().length() == 0)
                    pass1.setError("Password required!");
                if (confrmpass.getText().length() == 0)
                    confrmpass.setError("Confirm Password  required!");
                if(selectedText.equals("Country of Residence"))
                    Dialoghelper.showSnackbar(RegisterPage.this, "Select Country of Residence");
                if(check_terms==true) {


                    apIs = ApiUtils.getSOService();
                    if (!(firstname.getText().length() == 0) && !(lastname.getText().length() == 0) && !(mobileno.getText().length() == 0) && !(email_reg.getText().length() == 0) && !(pass1.getText().length() == 0) && !(confrmpass.getText().length() == 0)) {


                        firstname1 = firstname.getText().toString();
                        lastname1 = lastname.getText().toString();
                        email_reg1 = email_reg.getText().toString();
                        if (emailValidator(email_reg1) == true) {

                        } else {
                            email_reg.setError("Invalid Email");
                        }

                        mobileno1 = mobileno.getText().toString();
                              if(isValidMobile(mobileno1)==true)
                              {

                              }
                              else {
                                  mobileno.setError("Invalid Mobile");
                              }
                        pass11 = pass1.getText().toString();
                        confrmpass1 = confrmpass.getText().toString();
                        if (pass11.equals(confrmpass1)) {


                            String s = firstname1 + lastname1 + mobileno1 + email_reg1 + pass11 + confrmpass1 + spin_val;


                            Log.d("spin", "onResponse: message " + s);
                            final Call<JsonObject> regcall = new Retrofit_Helper().getRetrofitBuilder().getRegistartion(firstname1, lastname1, mobileno1, email_reg1, pass11, confrmpass1, selected_country,"android",android_id);
                            regcall.enqueue(new ResponseHandler(true, RegisterPage.this, new ResponseCallback() {
                                @Override
                                public void getResponse(int code, JsonObject jsonObject) {
                                    PojoitemReg reg_data = new GsonBuilder().create().fromJson(jsonObject, PojoitemReg.class);
                                    if (reg_data.getErrorCode().equals("0")) {

                                        session_class.registerSuccessset(reg_data.getData().getUserId(), reg_data.getData().getOtp());
                                        Intent intent = new Intent(getApplicationContext(), Otp_Screenpage.class);
                                        intent.putExtra("Email_of_registereduser",email_reg1);
                                     //   session_class.createUserRegisterSession(firstname1, lastname1, selected_country, email_reg1, mobileno1, MyConstants.LoggedIn.TRUE);

                                        startActivity(intent);
                                        finish();

                                        Log.d("Koloxo_login", "ch: error code (0) " + reg_data.getData().getOtp() + " message : " + reg_data.getMessage());
                                    } else {
                                        Log.d("Koloxo_login", "ch: error code (1)  " + reg_data.getData().getUserId() + " message : " + reg_data.getMessage());

                                        Toast.makeText(RegisterPage.this, reg_data.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void getError(Call<JsonObject> call, String message) {
                                    Dialoghelper.showSnackbar(RegisterPage.this, message);
                                }
                            }, regcall));


                        } else {
                            pass1.setError("MisMatch");
                            confrmpass.setError("MisMatch");
                        }


                    } else {
                        if (firstname.getText().length() == 0)
                            firstname.setError("First name required!");
                        if (lastname.getText().length() == 0)
                            lastname.setError("Last name required!");
                        if (mobileno.getText().length() == 0)
                            mobileno.setError("Mobile no. required!");
                        if (email_reg.getText().length() == 0)
                            email_reg.setError("Please enter email ID!");
                        if (pass1.getText().length() == 0)
                            pass1.setError("Password required!");
                        if (confrmpass.getText().length() == 0)
                            confrmpass.setError("Confirm Password  required!");
                        if(selectedText.equals("Country of Residence"))
                            Dialoghelper.showSnackbar(RegisterPage.this, "Select Country of Residence");
                    }


                }
                else
                {
                    Dialoghelper.showSnackbar(RegisterPage.this, "Please accept Terms & Condition");
                }

            }
        });




        //Terms click
        terms_cond.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                Log.d("termTest", " button clicked");
                final Call<JsonObject> termscall = new Retrofit_Helper().getRetrofitBuilder().getterms();


                termscall.enqueue(new ResponseHandler(true, RegisterPage.this, new ResponseCallback() {
                    @Override
                    public void getResponse(int code, JsonObject jsonObject) {
                        Log.d("termTest", "getResponse: "+ jsonObject.toString());
                        PojoTerms  pojoTerms=new GsonBuilder().create().fromJson(jsonObject, PojoTerms.class);

                        if(pojoTerms.getErrorCode().equals("0")) {

                            DialogPop_rating dialogPop_rating=new DialogPop_rating(RegisterPage.this);
                            dialogPop_rating.intiate_terms_condition(RegisterPage.this,pojoTerms.getData().get(0).getContent());
                            Log.d("terms_case", "onResponse: message " + pojoTerms.getData().get(0).getContent());
                        }

                        Log.d("terms", "onResponse: message " + pojoTerms.getErrorCode());

                    }
                    @Override
                    public void getError(Call<JsonObject> call, String message) {
                        Dialoghelper.showSnackbar(RegisterPage.this, message);
                    }
                 }, termscall));
            }
        });
    }

    public void initializingVar()
    {  sign_in=(TextView)findViewById(R.id.sigup_clik);
        registerNow=(Button)findViewById(R.id.registerNow);
        country_of_residence=(Spinner) findViewById(R.id.country_of_residence);
        firstname=(EditText) findViewById(R.id.first_name);
        lastname=(EditText) findViewById(R.id.last_name);
        mobileno=(EditText) findViewById(R.id.mobile_no);
        pass1=(EditText) findViewById(R.id.pass_reg);
        confrmpass=(EditText) findViewById(R.id.confirmpass_reg);
        email_reg=(EditText) findViewById(R.id.email_reg);
        terms_cond=(TextView) findViewById(R.id.terms_cond);
        simpleCheckBox=(CheckBox) findViewById(R.id.simpleCheckBox);

    }
    public boolean emailValidator(String email)
    {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                mobileno.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }

  /*  @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        // On selecting a spinner item
        String item = adapterView.getItemAtPosition(position).toString();

        // Showing selected spinner item
        Toast.makeText(adapterView.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }*/
}
