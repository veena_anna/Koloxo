package koloxo.appn.koloxo.koloxo;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.SideMenu.SideMenuPage;
import koloxo.appn.koloxo.koloxo.activity.LoginPage;
import koloxo.appn.koloxo.koloxo.activity.MySearch_frm_home;

public class MainActivity extends Activitybase {
    //Initializing
    ImageView search_icn_main;
    static ImageView MenuButton;
    SideMenuPage sideMenuPage;
    ImageView side_l2_img_pic,profile_edit_ic, profile_edit;;
    TextView home_side,mydash,myreq,myhis,mynotify,mywish,mysearches_side;
    RelativeLayout login_side,Land_layout3,log_out;
    TextView land_add_text2;
    ImageView active,locat;
         TextView   locat_address;
         Button landing_notify;


    TextView support_call;
    Session_class session_class;
   public static String android_id;

    int sample;
    //SwipeRefreshLayout swipeRefreshLayout;

    @Override
    protected void onResume() {
        super.onResume();
        Check_login_section();
       // swipeRefreshLayout.setRefreshing(false);


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
       android_id = Settings.Secure.getString(MainActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Log.d("Device", "Device_id: "+android_id);
      search_icn_main =(ImageView)findViewById(R.id.search_main_id);
        MenuButton = (ImageView) findViewById(R.id.menu_id);
        MenuButton.setImageResource(R.drawable.ic_main_menu);
        Land_layout3=(RelativeLayout)findViewById(R.id.Land_layout3);
        land_add_text2=(TextView) findViewById(R.id.land_add_text2);
       // swipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.swiperefresh_main);
session_class=new Session_class(MainActivity.this);
       // MenuButton.setColorFilter(Color.BLACK);
        /*MenuButton.setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.black),
               PorterDuff.Mode.MULTIPLY);*/

        sideMenuPage=new SideMenuPage(MainActivity.this);

        sideMenuPage.setupSlidemenu(MainActivity.this);

        sideMenuPage.setAnimations(MainActivity.this);

        initializevar();
Check_login_section();

        home_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                sideMenuPage.mDrawerLayout.closeDrawers();
            }
        });
        support_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("XXXX", "onClick: cal clicked");

                String uri = "tel:" + "+34 662 121 468";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent, "Call with"));
                } else {
                    Log.d("XXXX", "onClick:  no App installed");
                    //MyDialogHelper.showSnackbar(context, "No dialer app Installed");
                }




            }
        });



      /*  MenuButton.setImageResource(R.drawable.ic_menu);
        MenuButton.setColorFilter(ContextCompat.getColor(getApplicationContext(), android.R.color.black),
                PorterDuff.Mode.MULTIPLY);*/


    }
    public void initializevar()
    {
        session_class=new Session_class(MainActivity.this);
        home_side=(TextView)findViewById(R.id.side_l3_home);
         mydash=(TextView)findViewById(R.id.side_l3_dash);
         myreq=(TextView)findViewById(R.id.side_l3_rqt);
         myhis=(TextView)findViewById(R.id.side_l3_his);
         mynotify=(TextView)findViewById(R.id.side_l3_notify);
         mywish=(TextView)findViewById(R.id.side_l3_wish);
         mysearches_side=(TextView)findViewById(R.id.side_l3_srch);
        login_side=(RelativeLayout)findViewById(R.id.side_l4);
        log_out=(RelativeLayout)findViewById(R.id.side_l5);
        landing_notify=(Button)findViewById(R.id.landing_notify);
        side_l2_img_pic=(ImageView) findViewById(R.id.side_l2_img_pic);

        profile_edit = (ImageView) findViewById(R.id.side_l2_img_pic_fade);
        profile_edit_ic = (ImageView) findViewById(R.id.ic_profile_edit);


        active=(ImageView) findViewById(R.id.active);
        locat=(ImageView) findViewById(R.id.ic_address_side);
        locat_address=(TextView)findViewById(R.id.addes_side);

        support_call=(TextView) findViewById(R.id.support_call);

    }
    public void Check_login_section()
    {
        if (session_class.getId()==null)
        {sample=MyConstants.LoggedIn.FALSE;

        }
        else {
            sample=MyConstants.LoggedIn.TRUE;
        }



        if(sample== MyConstants.LoggedIn.TRUE)
        {
            active.setVisibility(View.VISIBLE);
            locat.setVisibility(View.VISIBLE);

            locat_address.setVisibility(View.VISIBLE);
            landing_notify.setVisibility(View.VISIBLE);
            log_out.setVisibility(View.VISIBLE);
            login_side.setVisibility(View.INVISIBLE);

            //  profile_edit.setVisibility(View.VISIBLE);
            // profile_edit_ic.setVisibility(View.VISIBLE);
            // profile_edit.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
            //profile_edit_ic.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));

            mydash.setClickable(true);
            myreq.setClickable(true);
            myhis.setClickable(true);
            mynotify.setClickable(true);
            mywish.setClickable(true);

            mydash.setTextColor(Color.parseColor("#ffffff"));
            myreq.setTextColor(Color.parseColor("#ffffff"));
            myhis.setTextColor(Color.parseColor("#ffffff"));
            mynotify.setTextColor(Color.parseColor("#ffffff"));
            mywish.setTextColor(Color.parseColor("#ffffff"));

            mysearches_side.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourKeyhome","MySearchFromhome");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();
                    //Toast.makeText(getApplicationContext(),"You clicked mysearches_side",Toast.LENGTH_SHORT).show();
                }
            });
            Land_layout3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourValueKey","ADD PROPERTY");
                    startActivity(intent);
                }
            });

            mydash.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourKeyhome","MyDashFromhome");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();
                }
            });
            myreq.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourKeyhome","MyRequestFromhome");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();
                }
            });
            myhis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourKeyhome","MyHistoryFromhome");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();
                }
            });
            mynotify.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourKeyhome","MyNotifyFromhome");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();
                }
            });
            mywish.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourKeyhome","MyWishlistFromhome");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();
                }
            });
            log_out.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    session_class.logoutUser();
                    sideMenuPage.mDrawerLayout.closeDrawers();
                    Check_login_section();

                }
            });
        }

        else {
            active.setVisibility(View.INVISIBLE);
            locat.setVisibility(View.INVISIBLE);

            locat_address.setVisibility(View.INVISIBLE);
            landing_notify.setVisibility(View.INVISIBLE);
            login_side.setVisibility(View.VISIBLE);
            log_out.setVisibility(View.INVISIBLE);
            profile_edit.setVisibility(View.INVISIBLE);
            profile_edit_ic.setVisibility(View.INVISIBLE);

            mydash.setClickable(false);
            myreq.setClickable(false);
            myhis.setClickable(false);
            mynotify.setClickable(false);
            mywish.setClickable(false);

            mydash.setTextColor(Color.parseColor("#8F73EB"));
            myreq.setTextColor(Color.parseColor("#8F73EB"));
            myhis.setTextColor(Color.parseColor("#8F73EB"));
            mynotify.setTextColor(Color.parseColor("#8F73EB"));
            mywish.setTextColor(Color.parseColor("#8F73EB"));


            login_side.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(getApplicationContext(), LoginPage.class);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();


                }
            });
            mysearches_side.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, MainLandPage.class);
                    intent.putExtra("YourKeyhome","MySearchFromhome");
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();
                    //Toast.makeText(getApplicationContext(),"You clicked mysearches_side",Toast.LENGTH_SHORT).show();
                }
            });
            Land_layout3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent=new Intent(MainActivity.this, LoginPage.class);
                    startActivity(intent);

                }
            });


            side_l2_img_pic.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(getApplicationContext(),LoginPage.class);
                    startActivity(intent);
                    sideMenuPage.mDrawerLayout.closeDrawers();

                }
            });


        }
    }
    public  void SEARCH_MAIN_LAND(View view)
    {
        Intent search_intent=new Intent(MainActivity.this, MainLandPage.class);
        search_intent.putExtra("YourValueKey","SEARCH PAGE");
        startActivity(search_intent);
        //finish();
    }

    public void MenuButton(View view){

        /*Picasso.with(getApplicationContext()).load(R.drawable.ic_menu).into(MenuButton);*/


           sideMenuPage.mDrawerLayout.openDrawer(Gravity.RIGHT);
      //  MenuButton.setImageResource(R.drawable.ic_menu);



        }





}
