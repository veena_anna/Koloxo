package koloxo.appn.koloxo.koloxo.adapter.MyHistory_innr;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 27/3/18.
 */

public class Property_review_list_adapt extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;
    DialogPop_rating dialogPop_rating;

    public Property_review_list_adapt(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        CircleImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        RelativeLayout post_review_id;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
      Property_review_list_adapt.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
       if (convertView == null) {
    convertView= mInflater.inflate(R.layout.property_review_list_row, parent,false);
    dialogPop_rating=new DialogPop_rating(context);
    holder = new Property_review_list_adapt.ViewHolder();
           holder.post_review_id = (RelativeLayout) convertView.findViewById(R.id.post_review_id);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
         holder.imageView = (CircleImageView) convertView.findViewById(R.id.propert_reviewlis_img);
convertView.setTag(holder);
        }
        else {            holder = (Property_review_list_adapt.ViewHolder) convertView.getTag();
        }
        holder.post_review_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              dialogPop_rating.initiatePopupWindow_Lawyers(context);
            }
        });
      Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());
       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}