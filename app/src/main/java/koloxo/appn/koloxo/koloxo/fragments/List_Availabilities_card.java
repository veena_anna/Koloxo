package koloxo.appn.koloxo.koloxo.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.BaseActivity;
import koloxo.appn.koloxo.koloxo.Fragmentbase;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.List_availability_adapter;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

public class List_Availabilities_card extends android.support.v4.app.Fragment {


 ListView list_availablity;
 List_availability_adapter list_availability_adapter;
 ImageView filter_id;
 CircleImageView fab;

public static final Integer[] images = { R.drawable.splash_pic_1,
        R.drawable.splash_pic_2, R.drawable.splash_pic_3, R.drawable.splash_pic_1 };
    List<Side_mysearchobj> rowItems;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Dubai");
    }
/* @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container,
                          Bundle savedInstanceState) {
     // Inflate the layout for this fragment
     View view1= inflater.inflate(R.layout.activity_list__availabilities_card, container, false);*/
@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container,
                         Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    View view1= inflater.inflate(R.layout.activity_list__availabilities_card, container, false);
     list_availablity=(ListView)view1. findViewById(R.id.list_availabity_list);

     filter_id=(ImageView)view1. findViewById(R.id.filter_id);
     fab=(CircleImageView) view1. findViewById(R.id.fab);


     rowItems = new ArrayList<Side_mysearchobj>();
     for (int i = 0; i < images.length; i++) {
         Side_mysearchobj item = new Side_mysearchobj(images[i]);
         rowItems.add(item);

     }
     list_availability_adapter = new List_availability_adapter(getActivity(),rowItems);
     list_availablity.setAdapter(list_availability_adapter);
     filter_id.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             Title.setText("Filter");
            getHpBaseActivity().pushFragments(new Filter_availability(),true,true);
            // Intent intent=new Intent(getActivity(),Filter_availability.class);
            // startActivity(intent);

         }
     });
       /* FloatingActionButton fab = (FloatingActionButton) view1.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    getHpBaseActivity().onBackPressed();
                }
            });

return view1;
}


}
