package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.Property_details;
import koloxo.appn.koloxo.koloxo.activity.Property_details1;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;

/**
 * Created by appzoc-php on 15/3/18.
 */

public class List_availability_adapter extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;

    public List_availability_adapter(Context context,List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems=items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        List_availability_adapter.ViewHolder holder = null;


        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
     if (convertView == null) {
         convertView = mInflater.inflate(R.layout.list_availabilty_row, parent,false);
        holder = new List_availability_adapter.ViewHolder();
//      holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
       holder.imageView = (ImageView) convertView.findViewById(R.id.availbty_img1);
        convertView.setTag(holder);
  }
      else {
         holder = (List_availability_adapter.ViewHolder) convertView.getTag(); }

        // Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,Property_details.class);
                context.startActivity(intent);
               // getHpBaseActivity().pushFragments(new Property_details1(),true,true);
            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}