package koloxo.appn.koloxo.koloxo.adapter.Mydash_inner;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 7/3/18.
 */

public class MyRequest_b_s  extends BaseAdapter

    {
        Context context;
        List<Side_mysearchobj> rowItems;

    public MyRequest_b_s(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
        private class ViewHolder {
            ImageView imageView;
            TextView txtTitle;
            TextView txtDesc;
        }

    public View getView(int position, View convertView, ViewGroup parent) {
        MyRequest_b_s.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
       if (convertView == null) {
        convertView= mInflater.inflate(R.layout.side_mydash_inr_myrqst_bs, parent,false);
        holder = new MyRequest_b_s.ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.request_b_s_img);
        convertView.setTag(holder);
       }
       else {
           holder = (MyRequest_b_s.ViewHolder) convertView.getTag();
       }

      Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());
       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}