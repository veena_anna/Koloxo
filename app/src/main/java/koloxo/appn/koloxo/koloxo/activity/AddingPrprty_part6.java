package koloxo.appn.koloxo.koloxo.activity;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;


import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Added_properties;
import koloxo.appn.koloxo.koloxo.adapter.Post_proprty_part6;


import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class AddingPrprty_part6 extends android.support.v4.app.Fragment  {
ImageView add_prpty_nxt6;


    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Property Images");
    }
    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_prprty_part6);*/
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_adding_prprty_part6, container, false);
        RecyclerView recyclerView;
        String[] header = {"Balconies","Bedrooms", "Bathroom","Gardens", "Other Photos","Upload Exterior Image"};
            recyclerView = view.findViewById(R.id.recyclerView);
        add_prpty_nxt6=(ImageView)view.findViewById(R.id.add_prpty_nxt6);
        bottom_bar.setVisibility(View.INVISIBLE);


        add_prpty_nxt6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               //BaseActivity.getHpBaseActivity().clearAllBackStackEntries();
                Title.setText("Added Properties");
                bottom_bar.setVisibility(View.VISIBLE);
                getHpBaseActivity().pushFragments(new Added_properties(),true,true);
              //Toast.makeText(getActivity(),"Property Posted",Toast.LENGTH_SHORT).show();
            }
        });
            List<Pojo_postprpty_part3> mypojos = new ArrayList<>();
            for (int i = 0; i < header.length; i++) {
                Pojo_postprpty_part3 mypojo = new Pojo_postprpty_part3();
                mypojo.setName(header[i]);
                mypojo.setSelected(0);
                mypojos.add(mypojo);
            }

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(new Post_proprty_part6(getActivity(), mypojos));
            return view;
        }
    }

