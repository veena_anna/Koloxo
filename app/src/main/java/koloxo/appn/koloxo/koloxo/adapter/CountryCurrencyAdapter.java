package koloxo.appn.koloxo.koloxo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Webserives.CountryData;

/**
 * Created by appzoc on 20/4/18.
 */

public class CountryCurrencyAdapter extends BaseAdapter {

    Context context;
    List<CountryData>countryData;

    public CountryCurrencyAdapter(Context context, List<CountryData> countryData) {
        this.context = context;
        this.countryData = countryData;
    }

    @Override
    public int getCount() {
        return countryData.size();
    }

    @Override
    public Object getItem(int position) {
        return countryData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflter=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflter.inflate(R.layout.layout_currency, parent,false);

        TextView names = (TextView) view.findViewById(R.id.textView11);

        names.setText(countryData.get(position).getCurrency());


        return view;
    }
}
