package koloxo.appn.koloxo.koloxo.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.text.InputType;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.Webserives.PojoOTPVerify;
import koloxo.appn.koloxo.koloxo.Webserives.PojoResendotp;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.MainActivity.android_id;

public class Otp_Screenpage extends Activitybase {
    EditText ed1, ed2, ed3, ed4;
    AppCompatImageButton back_space;
    RelativeLayout back_fb_otp,full_layout;
    SpannableString spannableString;
    TextView resendid;
    Session_class session_class;
    String registerd_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp__screenpage);
        ed1 = findViewById(R.id.otp_ed_1);
        ed2 = findViewById(R.id.otp_ed_2);
        ed3 = findViewById(R.id.otp_ed_3);
        ed4 = findViewById(R.id.otp_ed__4);
        resendid = findViewById(R.id.resendid);
        Intent intent = getIntent();

        registerd_email = intent.getExtras().getString("Email_of_registereduser");
        spannableString=new SpannableString("Resend");
        spannableString.setSpan(new UnderlineSpan(),0,spannableString.length(),0);
        resendid.setText(spannableString);

        back_fb_otp=(RelativeLayout)findViewById(R.id.back_fb_otp);
        full_layout=(RelativeLayout)findViewById(R.id.full_layout);

        full_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                        String ed11=ed1.getText().toString();
                        String ed22=ed2.getText().toString();
                        String ed33=ed3.getText().toString();
                        String ed44=ed4.getText().toString();
                        String comp_str= ed11+ed22+ed33+ed44;

                        String otp_val1= String.valueOf(session_class.getOtp_val());
                        Log.d("otp", "ch: error code (0) " + comp_str+otp_val1  );
                      // Toast.makeText(getApplicationContext(),comp_str+otp_val1,Toast.LENGTH_SHORT).show();
                        if(comp_str.equals(otp_val1)) {


                            final Call<JsonObject> otpverify = new Retrofit_Helper().getRetrofitBuilder().getOTPVerify(registerd_email, otp_val1);
                            otpverify.enqueue(new ResponseHandler(true, Otp_Screenpage.this, new ResponseCallback() {
                                @Override
                                public void getResponse(int code, JsonObject jsonObject) {
                                    Log.v("LoginActivity", jsonObject.toString());
                                    PojoOTPVerify otpVerify1 = new GsonBuilder().create().fromJson(jsonObject, PojoOTPVerify.class);
                                    if (otpVerify1.getErrorCode().equals("0")) {

                                        Intent intent = new Intent(getApplicationContext(), Register_success_page.class);
                                        session_class.createUserRegisterSession(otpVerify1.getData().getUserId(),otpVerify1.getData().getFirstName(),otpVerify1.getData().getLastName(),otpVerify1.getData().getCountryResidence(),otpVerify1.getData().getMobileNumber(),otpVerify1.getData().getEmail(), MyConstants.LoggedIn.TRUE,otpVerify1.getData().getImage());
                                        startActivity(intent);
                                        finish();

                                    }
                                    Log.d("Koloxo_login", "onResponse: message " + otpVerify1.getMessage());
                                    Toast.makeText(Otp_Screenpage.this, "Login Success", Toast.LENGTH_SHORT).show();
                                    // session_class.createUserLoginSession(response.body().getData().getUserId());
                                }


                                @Override
                                public void getError(Call<JsonObject> call, String message) {
                                    Dialoghelper.showSnackbar(Otp_Screenpage.this, message);
                                }
                            }, otpverify));





                        }
                        else {
                            // Toast.makeText(getApplicationContext(),"OTP Error",Toast.LENGTH_SHORT).show();
                            Dialoghelper.showSnackbar(Otp_Screenpage.this, "OTP Error");
                        }


            }
        });
        resendid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Call<JsonObject> otpresendcall1 = new Retrofit_Helper().getRetrofitBuilder().getResentotp(session_class.getEmail_reg());


                otpresendcall1.enqueue(new ResponseHandler(true, Otp_Screenpage.this, new ResponseCallback() {
                    @Override
                    public void getResponse(int code, JsonObject jsonObject) {
                        PojoResendotp pojoresendotp = new GsonBuilder().create().fromJson(jsonObject, PojoResendotp.class);
                        if (pojoresendotp.getErrorCode().equals("0")) {
                            Integer val= session_class.getUser_id();

                            session_class.registerSuccessset(val, pojoresendotp.getOtp());

                        }
                        Log.d("Koloxo_login", "onResponse: message " + pojoresendotp.getOtp());
                        Dialoghelper.showSnackbar(Otp_Screenpage.this, "OTP Resend");
                        //Toast.makeText(Otp_Screenpage.this, "OTP Resend", Toast.LENGTH_SHORT).show();
                        // session_class.createUserLoginSession(response.body().getData().getUserId());
                    }

                    @Override
                    public void getError(Call<JsonObject> call, String message) {
                        Dialoghelper.showSnackbar(Otp_Screenpage.this, message);
                    }
                }, otpresendcall1));
            }
        });
        back_fb_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        back_space = findViewById(R.id.back_space);
        ed1.setInputType(InputType.TYPE_NULL);
        ed2.setInputType(InputType.TYPE_NULL);
        ed3.setInputType(InputType.TYPE_NULL);
        ed4.setInputType(InputType.TYPE_NULL);


        back_space.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onbackSpace();
            }
        });
    }

    private void onbackSpace() {
        if (ed4.getText().length() != 0) {
            ed4.setText("");
        } else if (ed3.getText().length() != 0) {
            ed3.setText("");
        } else if (ed2.getText().length() != 0) {
            ed2.setText("");
        } else if (ed1.getText().length() != 0) {
            ed1.setText("");
        }
    }

    public void buttonClicked(View view) {
        switch (view.getId()) {
            case R.id.num_1:

                if (ed1.getText().length() == 0) {
                    ed1.setText("1");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("1");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("1");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("1");
                }
                break;
            case R.id.num_2:

                if (ed1.getText().length() == 0) {
                    ed1.setText("2");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("2");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("2");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("2");
                }
                break;
            case R.id.num_3:

                if (ed1.getText().length() == 0) {
                    ed1.setText("3");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("3");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("3");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("3");
                }
                break;
            case R.id.num_4:

                if (ed1.getText().length() == 0) {
                    ed1.setText("4");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("4");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("4");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("4");
                }
                break;
            case R.id.num_5:

                if (ed1.getText().length() == 0) {
                    ed1.setText("5");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("5");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("5");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("5");
                }
                break;
            case R.id.num_6:
                if (ed1.getText().length() == 0) {
                    ed1.setText("6");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("6");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("6");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("6");
                }
                break;
            case R.id.num_7:
                if (ed1.getText().length() == 0) {
                    ed1.setText("7");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("7");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("7");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("7");
                }
                break;
            case R.id.num_8:
                if (ed1.getText().length() == 0) {
                    ed1.setText("8");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("8");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("8");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("8");
                }
                break;
            case R.id.num_9:
                if (ed1.getText().length() == 0) {
                    ed1.setText("9");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("9");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("9");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("9");
                }
                break;
            case R.id.num_0:
                if (ed1.getText().length() == 0) {
                    ed1.setText("0");
                } else if (ed2.getText().length() == 0) {
                    ed2.setText("0");
                } else if (ed3.getText().length() == 0) {
                    ed3.setText("0");
                } else if (ed4.getText().length() == 0) {
                    ed4.setText("0");
                }
                break;
            default:
                //Toast.makeText(this, "no num clicked", Toast.LENGTH_SHORT).show();
                break;

        }
    }
}


