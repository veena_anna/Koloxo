package koloxo.appn.koloxo.koloxo.activity;

import android.app.Fragment;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.paolorotolo.expandableheightlistview.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Post_prprty_placemarks_adptr;
import koloxo.appn.koloxo.koloxo.adapter.Side_mysearches;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class AddingPrpty_part5 extends android.support.v4.app.Fragment  {
ImageView add_prpty_nxt5;
    ExpandableHeightListView list_of_placemark;
    ImageView add_palcemark_img;
    TextView add_palcemark_txt;

    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.img_prpty,
            R.drawable.img_prpty, R.drawable.img_prpty, R.drawable.img_prpty };
    List<Side_mysearchobj> rowItems;
    DialogPop_rating dialogPop_add;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Add Placemarks");
    }
   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_prpty_part5);*/
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.activity_adding_prpty_part5, container, false);
       list_of_placemark=(ExpandableHeightListView)view.findViewById(R.id.list_of_placemark);
       add_palcemark_img=(ImageView) view.findViewById(R.id.add_palcemark_img);
       add_palcemark_txt=(TextView) view.findViewById(R.id.add_palcemark_txt);
       bottom_bar.setVisibility(View.INVISIBLE);

       add_palcemark_txt.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               dialogPop_add.initiatePopupWindow_ADD(getActivity());
           }
       });
       dialogPop_add=new DialogPop_rating(getActivity());
       list_of_placemark.setExpanded(true);


        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++)

        {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);
        }

        Post_prprty_placemarks_adptr adapter = new Post_prprty_placemarks_adptr(getActivity(), rowItems);
        list_of_placemark.setAdapter(adapter);



         add_prpty_nxt5=(ImageView)view.findViewById(R.id.add_prpty_nxt5);
        add_prpty_nxt5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent=new Intent(getActivity(),AddingPrprty_part6.class);
                startActivity(intent);*/
                Title.setText("Property Images");
                getHpBaseActivity().pushFragments(new AddingPrprty_part6(),true,true);
            }
        });
       add_palcemark_img.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               dialogPop_add.initiatePopupWindow_ADD(getActivity());
           }
       });
        return view;
    }
}
