package koloxo.appn.koloxo.koloxo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 23/3/18.
 */

public class PropertyImgAdapter extends RecyclerView.Adapter<PropertyImgAdapter.RecViewHolder> {
    Context context;
    List<Pojo_postprpty_part3> mypojos;

    public PropertyImgAdapter(Context context, List<Pojo_postprpty_part3> mypojos) {
        this.context = context;
        this.mypojos = mypojos;

    }

    @Override
    public RecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recyc_img_property, parent, false);

        return new RecViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecViewHolder holder, int position) {
        Pojo_postprpty_part3 mypojo = mypojos.get(position);
        String str = mypojo.getName();
        String str1 = str.replace("\"", "");
        holder.header.setText(str1);

        if (mypojos.get(position).getSelected() == 0) {
            holder.linearLayout.setVisibility(View.GONE);
            holder.imgbtn.setImageResource(R.drawable.ic_add_circular_outlined_button);
            holder.linear_1.setBackgroundColor(Color.parseColor("#F5F8FA"));
            holder.__view.setVisibility(View.VISIBLE);
        } else {
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.imgbtn.setImageResource(R.drawable.ic_minus_symbol);
            holder.linear_1.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.__view.setVisibility(View.GONE);


            PropertyChildAdapter recCardAdapter = new PropertyChildAdapter(context);
            holder.recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
            holder.recyclerView.setAdapter(recCardAdapter);


        }
    }

    @Override
    public int getItemCount() {
        return mypojos.size();
    }

    public class RecViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linear_1;
        RelativeLayout linearLayout;
        AppCompatImageButton imgbtn;
        TextView header;
        RecyclerView recyclerView;
        View __view;

        public RecViewHolder(View itemView) {
            super(itemView);
            linear_1 = (LinearLayout) itemView.findViewById(R.id.linear_layout);
            linearLayout = (RelativeLayout) itemView.findViewById(R.id.property_img_child1);
            imgbtn = (AppCompatImageButton) itemView.findViewById(R.id.imgbtn);
            header = (TextView) itemView.findViewById(R.id.header);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.add_img_rec);
            __view = itemView.findViewById(R.id.__view);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (mypojos.get(getAdapterPosition()).getSelected() == 0) {


                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);

                        }
                        mypojos.get(getAdapterPosition()).setSelected(1);

                        notifyItemRangeChanged(0, mypojos.size());
                    } else {

                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);
                        }
                        notifyItemRangeChanged(0, mypojos.size());
                    }
                }

            });

            imgbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mypojos.get(getAdapterPosition()).getSelected() == 0) {


                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);

                        }
                        mypojos.get(getAdapterPosition()).setSelected(1);

                        notifyItemRangeChanged(0, mypojos.size());
                    } else {

                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);
                        }
                        notifyItemRangeChanged(0, mypojos.size());
                    }
                }
            });
        }
    }
    public class PropertyChildAdapter extends RecyclerView.Adapter<PropertyChildAdapter.ViewHolder> {
        Context context;
        int img[] = {R.drawable.splash_pic_2, R.drawable.splash_pic_1};

        public PropertyChildAdapter(Context context) {
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.img_item_profile, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.img.setImageResource(img[position]);
        }

        @Override
        public int getItemCount() {
            return 2;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img,close_img;

            public ViewHolder(View itemView) {
                super(itemView);
                img = itemView.findViewById(R.id.img_uploaded);
                close_img = itemView.findViewById(R.id.img_close);
                close_img.setVisibility(View.GONE);
            }
        }
    }

}