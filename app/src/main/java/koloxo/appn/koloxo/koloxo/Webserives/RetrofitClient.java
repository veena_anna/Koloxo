package koloxo.appn.koloxo.koloxo.Webserives;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by appzoc-php on 3/4/18.
 */

public class RetrofitClient {
    private static Retrofit retrofit = null;
    public static Retrofit getClient(String baseUrl) {




        OkHttpClient.Builder client = new OkHttpClient().newBuilder();


        //Log.i("url_base34",base_url+","+web_interface);

        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                Request.Builder builder = original.newBuilder()
                        .header("Authorization", Credentials.basic("koloxo", "123456"));

                Request request = builder.build();
                return chain.proceed(request);
            }
        };

        client.addInterceptor(interceptor);


        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addConverterFactory(GsonConverterFactory.create()).client(client.build())

                    .build();
        }
        return retrofit;
    }
}
