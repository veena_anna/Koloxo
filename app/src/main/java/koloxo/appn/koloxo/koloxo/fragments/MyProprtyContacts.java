package koloxo.appn.koloxo.koloxo.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.MyProperty_revw;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Myreq_innr.Request_accepted_list;
import koloxo.appn.koloxo.koloxo.adapter.MyHistory_innr.MyProprty_myhis;
import koloxo.appn.koloxo.koloxo.adapter.Myprprtycontacts_adaptr;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;


public class MyProprtyContacts extends Fragment implements AdapterView.OnItemClickListener{
    ListView MyPrpty_contcts;
    SwipeRefreshLayout swipeRefreshLayout;
    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.img_prpty,
            R.drawable.img_prpty, R.drawable.img_prpty, R.drawable.img_prpty };
    List<Side_mysearchobj> rowItems;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_my_proprty_contacts, container, false);
        swipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {


                swipeRefreshLayout.setRefreshing(false);
            }
        });
    MyPrpty_contcts=(ListView)view.findViewById(R.id.list_myproprty_conacts);
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);

        }


        Myprprtycontacts_adaptr adapter = new Myprprtycontacts_adaptr(getActivity(), rowItems);
        MyPrpty_contcts.setAdapter(adapter);
        MyPrpty_contcts.setOnItemClickListener(MyProprtyContacts.this);

        //back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
       /* Toast toast = Toast.makeText(getActivity(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();*/

    }


}
