package koloxo.appn.koloxo.koloxo.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.DrawableRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.InfoWindowData;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Google_Map_custom_adapter;
import koloxo.appn.koloxo.koloxo.adapter.Spinner_Adapter;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class AddingPrpty_part4 extends android.support.v4.app.Fragment  implements OnMapReadyCallback,LocationListener {
    private GoogleMap mMap;
    LocationListener mListener;
    LatLngBounds.Builder builder;
    CameraUpdate cu;
    ImageView expan,expan_2;
    ImageView next_page;
    String[] List_country= new String[]{"Country","Country","Country"};
    String[] List_state= new String[]{"State","State","State"};
    String[] List_city= new String[]{"City","City","City"};
        Spinner country,state,city;
    SupportMapFragment mapFragment;
    Animation animation;




    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Add Placemarks");
    }
    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_prpty_part4);*/

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_adding_prpty_part4, container, false);
        expan=(ImageView)view.findViewById(R.id.expan_1);
        expan_2=(ImageView)view.findViewById(R.id.expan_2);
        next_page=(ImageView)view.findViewById(R.id.add_prpty_nxt4);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        bottom_bar.setVisibility(View.INVISIBLE);

        /*animation = AnimationUtils.loadAnimation(getActivity(),
                R.anim.slide_down);*/

        country=(Spinner) view.findViewById(R.id.country);
        city=(Spinner)view.findViewById(R.id.spin_city);
        state=(Spinner) view.findViewById(R.id.state);

        // Create an ArrayAdapter using the string array and a default spinner
        Spinner_Adapter sAdapter = new Spinner_Adapter(getActivity(),List_country);
//        ArrayAdapter<String> staticAdapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item, List_country);
        //Price_spinner.setPrompt("Select");
        // Specify the layout to use when the list of choices appears


//        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        country.setAdapter(sAdapter);


        // Create an ArrayAdapter using the string array and a default spinner
//        ArrayAdapter<String> staticAdapter1 = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item, List_state);
        //Price_spinner.setPrompt("Select");
        // Specify the layout to use when the list of choices appears
//        staticAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner

        state.setAdapter(new Spinner_Adapter(getActivity(),List_state));

        // Create an ArrayAdapter using the string array and a default spinner
//        ArrayAdapter<String> staticAdapter2 = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item, List_city);
        //Price_spinner.setPrompt("Select");
        // Specify the layout to use when the list of choices appears
//        staticAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        city.setAdapter(new Spinner_Adapter(getActivity(),List_city));



      mapFragment = (SupportMapFragment)getChildFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        expan_2.setVisibility(View.INVISIBLE);
        ViewGroup.LayoutParams params = mapFragment.getView().getLayoutParams();
        params.height = 450;
        mapFragment.getView().setLayoutParams(params);
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }

        expan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                expan_2.setVisibility(View.VISIBLE);
                expan.setVisibility(View.INVISIBLE);
                ViewGroup.LayoutParams params = mapFragment.getView().getLayoutParams();
                params.height = 900;
                mapFragment.getView().setLayoutParams(params);


            }
        });
     expan_2.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {
             expan.setVisibility(View.VISIBLE);
             expan_2.setVisibility(View.INVISIBLE);
             ViewGroup.LayoutParams params = mapFragment.getView().getLayoutParams();
             params.height = 450;
             mapFragment.getView().setLayoutParams(params);
         }
     });
       next_page.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent=new Intent(getActivity(),AddingPrpty_part5.class);
                startActivity(intent);*/
                Title.setText("Furnitures");
                getHpBaseActivity().pushFragments(new AddingPrpty_part5(),true,true);
            }
        });
        return view;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.style_json));

            if (!success) {
                Log.e("N", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("N", "Can't find style. Error: ", e);
        }
        // new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location));
        // Add a marker in Sydney and move the camera

        // LatLng sydney = new LatLng(-34, 151);
        //  BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
        //  mMap.addMarker(new MarkerOptions().position(sydney).icon(bitmapDescriptorFromVector(this, R.drawable.ic_location)).title("Marker in Sydney"));

        //  mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


        /**clear the map before redraw to them*/
        mMap.clear();
        /**Create dummy Markers List*/
        List<Marker> markersList = new ArrayList<Marker>();
        Marker appzoc = mMap.addMarker(new MarkerOptions().position(new LatLng(
                9.9996952, 76.294765)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.button_bg_unselect)).title("Delhi"));



        /**Put all the markers into arraylist*/
        markersList.add(appzoc);





        /**create for loop for get the latLngbuilder from the marker list*/
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }


        Google_Map_custom_adapter customInfoWindow = new Google_Map_custom_adapter(getActivity());
        mMap.setInfoWindowAdapter(customInfoWindow);
        InfoWindowData info = new InfoWindowData();
        info.setImage("snowqualmie");
        info.setHotel("Hotel : excellent hotels available");
        info.setFood("Food : all types of restaurants available");
        info.setTransport("Reach the site by bus, car and train.");
        /*Marker m = mMap.addMarker(markersList);
        m.setTag(info);
        m.showInfoWindow();*/

        /**initialize the padding for map boundary*/
        int padding = 50;
        /**create the bounds from latlngBuilder to set into map camera*/
        LatLngBounds bounds = builder.build();
        /**create the camera with bounds and padding to set into map*/
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        /**call the map call back to know map is loaded or not*/
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                /**set animated zoom camera into map*/
                mMap.animateCamera(cu);

            }
        });
        // BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);

    }
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_red);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onLocationChanged(Location location) {
        if( mListener != null )
        {
            mListener.onLocationChanged( location );

        }
    }

}
