package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Myreq_innr.Request_accepted_list;
import koloxo.appn.koloxo.koloxo.fragments.MyProprtyContacts;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 21/3/18.
 */

public class Myprprtycontacts_adaptr extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;

    public Myprprtycontacts_adaptr(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        LinearLayout for_chat_prprty;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Myprprtycontacts_adaptr.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    if (convertView == null) {
        convertView = mInflater.inflate(R.layout.myprpty_contcts_row, parent,false);
       holder = new Myprprtycontacts_adaptr.ViewHolder();
           holder.for_chat_prprty = (LinearLayout) convertView.findViewById(R.id.for_chat_myprprty);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.contctprpty_card_image);
        convertView.setTag(holder);
       }
        else {
            holder = (Myprprtycontacts_adaptr.ViewHolder) convertView.getTag();
       }
       holder.for_chat_prprty.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               Title.setText("Dallas suit");
               getHpBaseActivity().pushFragments1(new Request_accepted_list(),true,true);
           }
       });
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Property Request");
               // Title.setTypeface(Koloxo);
                getHpBaseActivity().pushFragments(new Request_accepted_list(),true,true);
            }
        });
     Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());
       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}