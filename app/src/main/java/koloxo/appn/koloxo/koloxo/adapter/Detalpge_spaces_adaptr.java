package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.ImageSliderPopup;

import static android.content.Intent.FLAG_ACTIVITY_NEW_TASK;

/**
 * Created by Veena on 18-03-2018.
 */

public class Detalpge_spaces_adaptr extends BaseAdapter {
    Context context;
    List<Side_mysearchobj> rowItems;
    DialogPop_rating dialogPop_rating;

    public Detalpge_spaces_adaptr(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Detalpge_spaces_adaptr.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        dialogPop_rating=new DialogPop_rating(context);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.rowdetalpage_spaces, parent,false);
            holder = new Detalpge_spaces_adaptr.ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.img_space);
            convertView.setTag(holder);
        }
        else {
            holder = (Detalpge_spaces_adaptr.ViewHolder) convertView.getTag();
        }

        Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
//        holder.imageView.setImageResource(rowItem.getImageId());
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(context,ImageSliderPopup.class);
                intent.setFlags(FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
             //  dialogPop_rating.initiatePopupWindow_viewslide(context);

            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}
