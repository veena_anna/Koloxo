package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;

import java.util.ArrayList;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.AddingPrpty_part2;
import koloxo.appn.koloxo.koloxo.activity.Facilities;
import koloxo.appn.koloxo.koloxo.activity.ListFacilitiesModel;

/**
 * Created by appzoc-php on 15/3/18.
 */

public class Faclits_adpt_addprpty extends BaseAdapter {

    boolean[] oneSelected = new boolean[Facilities.Facilities.length];
    boolean[] twoSelected = new boolean[Facilities.Facilities.length];
    boolean[] threeSelected = new boolean[Facilities.Facilities.length];
    // SessionManager session;
    /*********** Declare Used Variables *********/
    private Activity activity;

    private ArrayList data;
    private static LayoutInflater inflater = null;
    public Resources res;
    ListFacilitiesModel tempValues = null;
    int i = 0;
    int Choose = 0;

    String textcolor_unselected = "#30000000";

    /*************  CustomAdapter Constructor *****************/
    public Faclits_adpt_addprpty(Activity a, ArrayList d, Resources resLocal) {

        /********** Take passed values **********/
        activity = a;
        data = d;
        res = resLocal;

        /***********  Layout inflator to call external xml layout () **********************/
      inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      //  inflater=(LayoutInflater)fragment.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //  session = new SessionManager(activity.getApplicationContext());

    }




    /******** What is the size of Passed Arraylist Size ************/
    public int getCount() {

        if (data.size() <= 0)
            return 1;
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    /********* Create a holder to contain inflated xml file elements ***********/
    public static class ViewHolder {

        public Button text1;
        public Button text2;
        public Button text3;

    }

    /*********** Depends upon data size called for each row , Create each ListView row ***********/

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {

            convertView = inflater.inflate(R.layout.tabitem, parent,false);
            viewHolder = new ViewHolder();

            viewHolder.text1 = (Button) convertView.findViewById(R.id.button1);
            viewHolder.text2 = (Button) convertView.findViewById(R.id.button2);
            viewHolder.text3 = (Button) convertView.findViewById(R.id.button3);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        /***** Get each Model object from Arraylist ********/
        tempValues = null;
        tempValues = (ListFacilitiesModel) data.get(position);

        /************  Set Model values in Holder elements ***********/
        if (!tempValues.getFirstButton().equals("A")) {
            viewHolder.text1.setVisibility(View.VISIBLE);
            viewHolder.text1.setText(tempValues.getFirstButton());
        } else {
            viewHolder.text1.setVisibility(View.GONE);
        }
        if (!tempValues.getSecondButton().equals("A")) {
            viewHolder.text2.setVisibility(View.VISIBLE);
            viewHolder.text2.setText(tempValues.getSecondButton());
        } else {
            viewHolder.text2.setVisibility(View.GONE);
        }
        if (tempValues.getThirdButton().equals("A")) {
            viewHolder.text3.setVisibility(View.GONE);
        } else {
            viewHolder.text3.setVisibility(View.VISIBLE);
            viewHolder.text3.setText(tempValues.getThirdButton());
        }

        //************** Change Background And TextColor *****************//

        if (oneSelected[position]) {
            viewHolder.text1.setBackgroundResource(R.drawable.button_bg_salected);
            viewHolder.text1.setTextColor(Color.parseColor("#fefffb"));
        } else {
            viewHolder.text1.setBackgroundResource(R.drawable.button_bg_unselect);
            viewHolder.text1.setTextColor(Color.parseColor(textcolor_unselected));
        }

        if (twoSelected[position]) {
            viewHolder.text2.setBackgroundResource(R.drawable.button_bg_salected);
            viewHolder.text2.setTextColor(Color.parseColor("#fefffb"));
        } else {
            viewHolder.text2.setBackgroundResource(R.drawable.button_bg_unselect);
            viewHolder.text2.setTextColor(Color.parseColor(textcolor_unselected));
        }

        if (threeSelected[position]) {
            viewHolder.text3.setBackgroundResource(R.drawable.button_bg_salected);
            viewHolder.text3.setTextColor(Color.parseColor("#fefffb"));
        } else {
            viewHolder.text3.setBackgroundResource(R.drawable.button_bg_unselect);
            viewHolder.text3.setTextColor(Color.parseColor(textcolor_unselected));
        }


        viewHolder.text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!oneSelected[position]) {
                    oneSelected[position] = true;

                    Choose++;
                   // AddingPrpty_part2.ChooseText(Choose);

                    int pos = 0;
                    for (int i = 0; i < Facilities.Facilities.length - 1; i++) {
                        if (viewHolder.text1.getText().toString().equals(Facilities.Facilities[i])) {
                            pos = i;
                            //sendPostRequest(viewHolder.text1.getText().toString(), "selected");
                            //Toast.makeText(activity, "You Selected " + viewHolder.text1.getText().toString() + "  Position = " + String.valueOf(pos), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (oneSelected[position]) {
                    oneSelected[position] = false;

                    Choose--;
                  //  Facilities.ChooseText(Choose);


                    int pos = 0;
                    for (int i = 0; i < Facilities.Facilities.length - 1; i++) {
                        if (viewHolder.text1.getText().toString().equals(Facilities.Facilities[i])) {
                            pos = i;
                            //sendPostRequest(viewHolder.text1.getText().toString(), "unselected");
                            //Toast.makeText(activity, "You UnSelected " + viewHolder.text1.getText().toString() + "  Position = " + String.valueOf(pos), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                //************** Change Background And TextColor *****************//

                if (oneSelected[position]) {
                    viewHolder.text1.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text1.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text1.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text1.setTextColor(Color.parseColor(textcolor_unselected));
                }

                if (twoSelected[position]) {
                    viewHolder.text2.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text2.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text2.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text2.setTextColor(Color.parseColor(textcolor_unselected));
                }

                if (threeSelected[position]) {
                    viewHolder.text3.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text3.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text3.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text3.setTextColor(Color.parseColor(textcolor_unselected));
                }

            }
        });

        viewHolder.text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!twoSelected[position]) {
                    twoSelected[position] = true;

                    Choose++;
                   // AddingPrpty_part2.ChooseText(Choose);

                    int pos = 0;
                    for (int i = 0; i < Facilities.Facilities.length - 1; i++) {
                        if (viewHolder.text2.getText().toString().equals(Facilities.Facilities[i])) {
                            pos = i;
                            // sendPostRequest(viewHolder.text2.getText().toString(), "selected");
                            //Toast.makeText(activity, "You Selected " + viewHolder.text2.getText().toString() + "  Position = " + String.valueOf(pos), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (twoSelected[position]) {
                    twoSelected[position] = false;

                    Choose--;
                   // AddingPrpty_part2.ChooseText(Choose);

                    int pos = 0;
                    for (int i = 0; i < Facilities.Facilities.length - 1; i++) {
                        if (viewHolder.text2.getText().toString().equals(Facilities.Facilities[i])) {
                            pos = i;
                            // sendPostRequest(viewHolder.text2.getText().toString(), "unselected");
                            //Toast.makeText(activity, "You UnSelected " + viewHolder.text2.getText().toString() + "  Position = " + String.valueOf(pos), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                //************** Change Background And TextColor *****************//

                if (oneSelected[position]) {
                    viewHolder.text1.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text1.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text1.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text1.setTextColor(Color.parseColor(textcolor_unselected));
                }

                if (twoSelected[position]) {
                    viewHolder.text2.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text2.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text2.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text2.setTextColor(Color.parseColor(textcolor_unselected));
                }

                if (threeSelected[position]) {
                    viewHolder.text3.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text3.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text3.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text3.setTextColor(Color.parseColor(textcolor_unselected));
                }

            }
        });

        viewHolder.text3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!threeSelected[position]) {
                    threeSelected[position] = true;

                    Choose++;
                   // AddingPrpty_part2.ChooseText(Choose);

                    int pos = 0;
                    for (int i = 0; i < Facilities.Facilities.length - 1; i++) {
                        if (viewHolder.text3.getText().toString().equals(Facilities.Facilities[i])) {
                            pos = i;
                            //   sendPostRequest(viewHolder.text3.getText().toString(), "selected");
                            // Toast.makeText(activity, "You Selected " + viewHolder.text3.getText().toString() + "  Position = " + String.valueOf(pos), Toast.LENGTH_SHORT).show();
                        }
                    }
                } else if (threeSelected[position]) {
                    threeSelected[position] = false;

                    Choose--;
                    //AddingPrpty_part2.ChooseText(Choose);

                    int pos = 0;
                    for (int i = 0; i < Facilities.Facilities.length - 1; i++) {
                        if (viewHolder.text3.getText().toString().equals(Facilities.Facilities[i])) {
                            pos = i;
                            // sendPostRequest(viewHolder.text3.getText().toString(), "unselected");
                            //  Toast.makeText(activity, "You UnSelected " + viewHolder.text3.getText().toString() + "  Position = " + String.valueOf(pos), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                //************** Change Background And TextColor *****************//

                if (oneSelected[position]) {
                    viewHolder.text1.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text1.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text1.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text1.setTextColor(Color.parseColor(textcolor_unselected));
                }

                if (twoSelected[position]) {
                    viewHolder.text2.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text2.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text2.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text2.setTextColor(Color.parseColor(textcolor_unselected));
                }

                if (threeSelected[position]) {
                    viewHolder.text3.setBackgroundResource(R.drawable.button_bg_salected);
                    viewHolder.text3.setTextColor(Color.parseColor("#fefffb"));
                } else {
                    viewHolder.text3.setBackgroundResource(R.drawable.button_bg_unselect);
                    viewHolder.text3.setTextColor(Color.parseColor(textcolor_unselected));
                }

            }
        });

        return convertView;
    }
}