package koloxo.appn.koloxo.koloxo.Webserives;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by appzoc-php on 11/4/18.
 */

public class RegisterData {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("otp")
    @Expose
    private Integer otp;
    @SerializedName("otp_time")
    @Expose
    private Integer otpTime;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOtp() {
        return otp;
    }

    public void setOtp(Integer otp) {
        this.otp = otp;
    }

    public Integer getOtpTime() {
        return otpTime;
    }

    public void setOtpTime(Integer otpTime) {
        this.otpTime = otpTime;
    }}