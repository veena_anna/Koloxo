package koloxo.appn.koloxo.koloxo.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.AddingPrpty_part2;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;


public class Rent_Adding extends Fragment {
    ImageView add_prpty_nxt;
    Spinner Price_spinner,PRprtytype_spinr,Prprty_age_spinner,Prprty_elevator_spinr,Prprty_buildngno;
    String[] List_prpty_type,List_amount,List_age,List_elevators,List_buildngno;
    ImageView reputed_yes1,reputed_no1,reputed_yes11,reputed_no11;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view1= inflater.inflate(R.layout.fragment_rent__adding, container, false);
     // initializevar();
        reputed_yes1=(ImageView)view1.findViewById(R.id.reput_c11);
        reputed_no1=(ImageView)view1.findViewById(R.id.reput_c1);
        reputed_no1.setVisibility(View.VISIBLE);
        reputed_yes11=(ImageView)view1.findViewById(R.id.reput_c22);
        reputed_no11=(ImageView)view1.findViewById(R.id.reput_c2);
     reputed_no11.setVisibility(View.VISIBLE);


        List_prpty_type= new String[]{"select","apartment","houses"};
        List_amount= new String[]{"₹","2000","1000"};
        List_age= new String[]{"select","6 month","8 month"};
        List_elevators= new String[]{"select","1","2","3","4"};
        List_buildngno= new String[]{"select","101","202","304","425"};


        add_prpty_nxt=(ImageView)view1.findViewById(R.id.add_prpty_nxt1);
        Price_spinner=(Spinner) view1.findViewById(R.id.Price_spinner);
        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<String> staticAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, List_amount);
        // Specify the layout to use when the list of choices appears
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        Price_spinner.setAdapter(staticAdapter);

        PRprtytype_spinr=(Spinner) view1.findViewById(R.id.prpty_spinnr_type);
        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<String> staticAdapter1 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, List_prpty_type);
        // Specify the layout to use when the list of choices appears
        staticAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        PRprtytype_spinr.setAdapter(staticAdapter1);


        Prprty_age_spinner=(Spinner) view1.findViewById(R.id.prpty_spinnr_age);
        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<String> staticAdapter2 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, List_age);
        // Specify the layout to use when the list of choices appears
        staticAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        Prprty_age_spinner.setAdapter(staticAdapter2);

        Prprty_elevator_spinr=(Spinner) view1.findViewById(R.id.prpty_spinnr_elevator);

        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<String> staticAdapter3 = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, List_elevators);
        // Specify the layout to use when the list of choices appears
        staticAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        Prprty_elevator_spinr.setAdapter(staticAdapter3);


        Prprty_buildngno=(Spinner) view1.findViewById(R.id.prpty_spinnr_buildno);

        // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<String> staticAdapter4= new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, List_buildngno);
        // Specify the layout to use when the list of choices appears
        staticAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        Prprty_buildngno.setAdapter(staticAdapter4);
        add_prpty_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent=new Intent(getActivity(), AddingPrpty_part2.class);
                startActivity(intent);*/
                Title.setText("Floor Plan");
                getHpBaseActivity().pushFragments(new AddingPrpty_part2(),true,true);
            }
        });
        reputed_no1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reputed_yes1.setVisibility(View.VISIBLE);
                reputed_no1.setVisibility(View.INVISIBLE);

                reputed_no11.setVisibility(View.VISIBLE);
                reputed_yes11.setVisibility(View.INVISIBLE);
            }
        });reputed_yes1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reputed_no1.setVisibility(View.VISIBLE);
                reputed_yes1.setVisibility(View.INVISIBLE);


                reputed_no11.setVisibility(View.VISIBLE);
                reputed_yes11.setVisibility(View.INVISIBLE);
            }
        });

        reputed_no11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reputed_yes11.setVisibility(View.VISIBLE);
                reputed_no11.setVisibility(View.INVISIBLE);

                reputed_no1.setVisibility(View.VISIBLE);
                reputed_yes1.setVisibility(View.INVISIBLE);
            }
        });reputed_yes11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reputed_no11.setVisibility(View.VISIBLE);
                reputed_yes11.setVisibility(View.INVISIBLE);

                reputed_no1.setVisibility(View.VISIBLE);
                reputed_yes1.setVisibility(View.INVISIBLE);
            }
        });
        return view1;
    }

    public void initializevar()
    {
       // add_prpty_nxt=(ImageView)getActivity().findViewById(R.id.add_prpty_nxt1);

    }

}



