package koloxo.appn.koloxo.koloxo.SidemenuPages.Myreq_innr;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.MyRental_prpty;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Notification;
import koloxo.appn.koloxo.koloxo.activity.Chat_Section;
import koloxo.appn.koloxo.koloxo.adapter.MyReqst_inner.Request_accpt_adpter;
import koloxo.appn.koloxo.koloxo.adapter.Notify_adapter;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;

public class Request_accepted_list extends android.support.v4.app.Fragment implements AdapterView.OnItemClickListener {
    ListView Accpt_rqst_list;
    MyRental_prpty request_list_mydash;
    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.img_prpty,
            R.drawable.img_prpty, R.drawable.img_prpty, R.drawable.img_prpty };
    List<Side_mysearchobj> rowItems;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Dallas suit");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //listener.onFragSelected("Dashboard One");
        View view=  inflater.inflate(R.layout.activity_request_accepted_list, container, false);

        Accpt_rqst_list = (ListView) view.findViewById(R.id.list_accptedrequest);
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);

        }


        Request_accpt_adpter adapter = new Request_accpt_adpter(getActivity(), rowItems);
        Accpt_rqst_list.setAdapter(adapter);
        Accpt_rqst_list.setOnItemClickListener(Request_accepted_list.this);

        //back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {

      /*  Toast toast = Toast.makeText(getActivity(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();*/
    }
}
