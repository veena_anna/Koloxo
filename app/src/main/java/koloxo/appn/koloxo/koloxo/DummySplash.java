package koloxo.appn.koloxo.koloxo;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import koloxo.appn.koloxo.koloxo.adapter.DummyAdapter;
import me.relex.circleindicator.CircleIndicator;

public class DummySplash extends Activitybase {
    CircleIndicator indicator;

    ViewPager viewpager;


    private int currentPage = 0;
    private int NUM_PAGES = 0;

    ArrayList<Integer> array_image;
    ArrayList<String> array_txt;
    ArrayList<String> array_txt1;
    DummyAdapter circleIndicator_adapter;
    Timer swipeTimer;
    boolean flag=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dummy_splash);
        viewpager = (ViewPager) findViewById(R.id.Splashpager);
        indicator = (CircleIndicator) findViewById(R.id.indicator);
        array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.splash1);
        array_image.add(R.drawable.globe_edited);
        array_image.add(R.drawable.splash3);
        array_image.add(R.drawable.splash4);
        array_txt=new ArrayList<>();
        array_txt.add("BUY,RENT OR SELL FROM");
        array_txt.add("HOW TO MAKE DEAL");
        array_txt.add("HOW TO FIND A GOOD TENANT");
        array_txt.add("THE SOLUTION IS");
        array_txt1=new ArrayList<>();
        array_txt1.add("AROUND THE WORLD.");
        array_txt1.add("WITHOUT MIDDLE HAND");
        array_txt1.add("AROUND THE WORLD");
        array_txt1.add("KOLOXO HOME");


        initImageSlider();
    }

    private void initImageSlider() {
        circleIndicator_adapter = new DummyAdapter(array_image,array_txt,array_txt1, DummySplash.this);
        viewpager.setAdapter(circleIndicator_adapter);
        indicator.setViewPager(viewpager);
        viewpager.setCurrentItem(0);
        indicator.setViewPager(viewpager);
        circleIndicator_adapter.registerDataSetObserver(indicator.getDataSetObserver());




        NUM_PAGES = array_image.size();
        if (NUM_PAGES > 1) {
            indicator.setVisibility(View.VISIBLE);
        } else {
            indicator.setVisibility(View.GONE);
        }


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                  //  currentPage = 0;
                    if (flag) {
                        Intent intent = new Intent(DummySplash.this, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        finish();
                        startActivity(intent);
                    }

                }
                viewpager.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 0, 2000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }
    public void SKIP(View view)
    {
        flag=false;
        Intent skip_intent=new Intent(DummySplash.this,MainActivity.class);
        startActivity(skip_intent);
        finish();
    }
}

