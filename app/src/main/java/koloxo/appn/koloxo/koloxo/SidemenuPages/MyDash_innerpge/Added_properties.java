package koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MySearches;
import koloxo.appn.koloxo.koloxo.adapter.Mydash_inner.Add_propeties_mydash;
import koloxo.appn.koloxo.koloxo.adapter.Side_mysearches;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;


public class Added_properties extends android.support.v4.app.Fragment implements AdapterView.OnItemClickListener {
    ListView addprprty_list;

    Add_propeties_mydash add_propeties_mydash;
    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.img_prpty,
            R.drawable.img_prpty, R.drawable.img_prpty, R.drawable.img_prpty };
    List<Side_mysearchobj> rowItems;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Added Properties");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //listener.onFragSelected("Dashboard One");
        View view=  inflater.inflate(R.layout.fragment_added_properties, container, false);
        //setContentView(R.layout.fragment_added_properties);


        addprprty_list=(ListView)view.findViewById(R.id.list_add_proprty);
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < images.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i]);
            rowItems.add(item);
        }

        Add_propeties_mydash adapter = new Add_propeties_mydash(getActivity(), rowItems);
        addprprty_list.setAdapter(adapter);
        addprprty_list.setOnItemClickListener(Added_properties.this);

        //back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

return view;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
       /* Toast toast = Toast.makeText(getActivity(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();*/
    }
}
