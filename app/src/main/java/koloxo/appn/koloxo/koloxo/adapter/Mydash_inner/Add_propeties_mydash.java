package koloxo.appn.koloxo.koloxo.adapter.Mydash_inner;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Side_mysearches;

/**
 * Created by appzoc-php on 6/3/18.
 */

public class Add_propeties_mydash  extends BaseAdapter {
    Context context;
    List<Side_mysearchobj> rowItems;

    public Add_propeties_mydash(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView1;
        TextView txtTitle;
        TextView txtDesc;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Add_propeties_mydash.ViewHolder holder = null;


        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.side_mydashinnr_rowadd, parent,false);
            holder = new Add_propeties_mydash.ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView1 = (ImageView) convertView.findViewById(R.id.added_prprty_img_id);
            convertView.setTag(holder);
        }
        else {
            holder = (Add_propeties_mydash.ViewHolder) convertView.getTag();
        }

        Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView1.setImageResource(rowItem.getImageId());
        return convertView;
    }
    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}