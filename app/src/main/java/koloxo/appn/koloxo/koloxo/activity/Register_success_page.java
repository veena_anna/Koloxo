package koloxo.appn.koloxo.koloxo.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.R;

public class Register_success_page extends Activitybase {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_success_page);
      Thread thread=new Thread()
      {
          @Override
          public void run() {
              /*super.run();*/
              try {
                  sleep(1*2000);
                  Intent intent=new Intent(getApplicationContext(),MainLandPage.class);
                  intent.putExtra("YourKey","success_registration");
                  startActivity(intent);
                  finish();

              }
             catch (Exception ex)
             {

             }
          }
      };
      thread.start();
    }
}
