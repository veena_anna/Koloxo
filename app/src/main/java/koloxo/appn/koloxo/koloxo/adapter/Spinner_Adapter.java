package koloxo.appn.koloxo.koloxo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 10/4/18.
 */

public class Spinner_Adapter extends BaseAdapter {
String[] list;
LayoutInflater inflter;
Context context;
    TextView names;

    public Spinner_Adapter(FragmentActivity context, String[] list) {
    this.list = list;
    this.context = context;
        inflter = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return list.length;

    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.custom_spinner_items, null);

        TextView names = (TextView) view.findViewById(R.id.textView);
        String str = list[i];
        String str1 = str.replace("\"", "");
        names.setText(str1);

        if(i!=0)
        {
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,60);


            names.setLayoutParams(layoutParams);
        }

        return view;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return super.getDropDownView(position, convertView, parent);
    }
}
