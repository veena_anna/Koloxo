package koloxo.appn.koloxo.koloxo;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;

public class SpalshStart extends Activitybase {
    private VideoView mVideoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spalsh_start);

       // bgVideoView = (VideoView) findViewById(R.id.bgVideoView);

        mVideoView = (VideoView) findViewById(R.id.bgVideoView);


        Uri uri = Uri.parse("android.resource://"+getPackageName()+"/"+R.raw.kolohome);

        mVideoView.setVideoURI(uri);
        mVideoView.start();
        mVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                Intent intent=new Intent(getApplicationContext(),DummySplash.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
       /* mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
             mediaPlayer.setLooping(true);

            }
        });*/

    }
}
