package koloxo.appn.koloxo.koloxo.Webserives;

/**
 * Created by appzoc-php on 3/4/18.
 */

public class ApiUtils {
    public static final String BASE_URL = "http://app.appzoc.com/koloxo/api/";
    public static final String BASE_URL_PIC = "http://app.appzoc.com/koloxo/storage/app/";

    public static APIs getSOService() {
        return RetrofitClient.getClient(BASE_URL).create(APIs.class);
    }
}