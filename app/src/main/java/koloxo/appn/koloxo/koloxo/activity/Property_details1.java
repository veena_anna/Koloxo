package koloxo.appn.koloxo.koloxo.activity;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import koloxo.appn.koloxo.koloxo.Objects.InfoWindowData;
import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Detalpge_facilty_adptr;
import koloxo.appn.koloxo.koloxo.adapter.Detalpge_spaces_adaptr;
import koloxo.appn.koloxo.koloxo.adapter.Furnitr_of_prprtydetail_adapt;
import koloxo.appn.koloxo.koloxo.adapter.Google_Map_custom_adapter;
import koloxo.appn.koloxo.koloxo.adapter.News_feed_adaptr;
import me.relex.circleindicator.CircleIndicator;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Koloxo;

public class Property_details1 extends android.support.v4.app.Fragment   implements OnMapReadyCallback,LocationListener {

    GridView list_facility,list_spaces;
    RelativeLayout back_detail_full;
    CircleIndicator indicator_newsfeed;
    ViewPager viewpager_newsfeed;
    TextView Proprty_name,Prpty_loc;
    private int currentPage = 0;
    private int NUM_PAGES = 0;

    //ArrayList<Integer> array_image_news;
    ArrayList<String> array_txt_newshead;
    ArrayList<String> array_txt_news_sub;
    News_feed_adaptr circleIndicator_adapter;
    Timer swipeTimer;
    public static final Integer[] images = { R.drawable.splash_pic_1,
            R.drawable.splash_pic_2,R.drawable.splash_pic_1,
            R.drawable.splash_pic_2,R.drawable.splash_pic_1,
            R.drawable.splash_pic_2,R.drawable.splash_pic_2,R.drawable.splash_pic_1,
            R.drawable.splash_pic_2};
    public static final String[] titles = new String[] { "AC",
            "AC", "AC", "AC" ,"AC","AC","AC","AC","AC"};
    List<Side_mysearchobj> rowItems;
    //for map
    private GoogleMap mMap;
    LocationListener mListener;
    LatLngBounds.Builder builder;
    CameraUpdate cu;
    RecyclerView recyclerView;
    String[] header = {"Bedroom", "Kitchen", "Guest Room", "Balcony", "OtherFurniture"};

    /*    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_property_details);*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view=  inflater.inflate(R.layout.activity_property_details, container, false);
        viewpager_newsfeed = (ViewPager) view.findViewById(R.id.viewpger_news);
        indicator_newsfeed = (CircleIndicator)view. findViewById(R.id.indicator_news);
        Proprty_name=(TextView) view.findViewById(R.id.proprty_name) ;
        Prpty_loc=(TextView)view.findViewById(R.id.prpty_location) ;

        back_detail_full=(RelativeLayout)view. findViewById(R.id.back_detail_full);
        back_detail_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  finish();*/
            }
        });
        Proprty_name.setTypeface(Koloxo);
        Prpty_loc.setTypeface(Koloxo);
       /* array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.splash_pic_2);
        array_image.add(R.drawable.splash_pic_1);
        array_image.add(R.drawable.splash_pic_2);*/
        array_txt_newshead=new ArrayList<>();
        array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        array_txt_news_sub=new ArrayList<>();
        array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
        array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
        array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
        initImageSlider();
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i],titles[i]);
            rowItems.add(item);
        }
        list_facility=(GridView)view.findViewById(R.id.details_facilities_list) ;
        list_spaces=(GridView)view.findViewById(R.id.spaces_list) ;
        Detalpge_facilty_adptr adapter = new Detalpge_facilty_adptr(getActivity(), rowItems, true);
        list_facility.setAdapter(adapter);

        Detalpge_spaces_adaptr adapter1 = new Detalpge_spaces_adaptr(getActivity(), rowItems);
        list_spaces.setAdapter(adapter1);
        //for map
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        //getSupportFragmentManager()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
      /*  SupportMapFragment mapFragment = (SupportMapFragment)getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

//for furniture details
        recyclerView =view.findViewById(R.id.recyclerView);

        List<Pojo_postprpty_part3> mypojos = new ArrayList<>();
        for (int i = 0; i < header.length; i++) {
            Pojo_postprpty_part3 mypojo = new Pojo_postprpty_part3();
            mypojo.setName(header[i]);
            mypojo.setSelected(0);
            mypojos.add(mypojo);
        }
        recyclerView.setBackgroundColor(Color.parseColor("#F6F7F8"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new Furnitr_of_prprtydetail_adapt(getActivity(), mypojos));
        return view;
    }

    private void initImageSlider() {
        circleIndicator_adapter = new News_feed_adaptr(array_txt_newshead,array_txt_news_sub,getActivity());
        viewpager_newsfeed.setAdapter(circleIndicator_adapter);
        indicator_newsfeed.setViewPager(viewpager_newsfeed);
        viewpager_newsfeed.setCurrentItem(0);
        indicator_newsfeed.setViewPager(viewpager_newsfeed);
        circleIndicator_adapter.registerDataSetObserver(indicator_newsfeed.getDataSetObserver());
        NUM_PAGES = array_txt_newshead.size();
        if (NUM_PAGES > 1) {
            indicator_newsfeed.setVisibility(View.VISIBLE);
        } else {
            indicator_newsfeed.setVisibility(View.GONE);
        }


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;


                }
                viewpager_newsfeed.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 0, 2000);

        // Pager listener over indicator
        indicator_newsfeed.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }


    @Override
    public void onLocationChanged(Location location) {
        if( mListener != null )
        {
            mListener.onLocationChanged( location );}
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.style_json));

            if (!success) {
                Log.e("N", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("N", "Can't find style. Error: ", e);
        }
        // new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location));
        // Add a marker in Sydney and move the camera

        // LatLng sydney = new LatLng(-34, 151);
        //  BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
        //  mMap.addMarker(new MarkerOptions().position(sydney).icon(bitmapDescriptorFromVector(this, R.drawable.ic_location)).title("Marker in Sydney"));

        //  mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


        /**clear the map before redraw to them*/
        mMap.clear();
        /**Create dummy Markers List*/
        List<Marker> markersList = new ArrayList<Marker>();
        Marker Delhi = mMap.addMarker(new MarkerOptions().position(new LatLng(
                9.997090, 76.302815)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_red)).title("Kaloor"));
        /**Put all the markers into arraylist*/
        markersList.add(Delhi);
        /**create for loop for get the latLngbuilder from the marker list*/
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }


        Google_Map_custom_adapter customInfoWindow = new Google_Map_custom_adapter(getActivity());
        mMap.setInfoWindowAdapter(customInfoWindow);
        InfoWindowData info = new InfoWindowData();
        info.setImage("snowqualmie");
        info.setHotel("Hotel : excellent hotels available");
        info.setFood("Food : all types of restaurants available");
        info.setTransport("Reach the site by bus, car and train.");
        /*Marker m = mMap.addMarker(markersList);
        m.setTag(info);
        m.showInfoWindow();*/

        // create the camera with bounds and padding to set into map
        LatLng kaloor = new LatLng(9.997090, 76.302815);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(kaloor));

       /* //initialize the padding for map boundary
        int padding = 50;
        //create the bounds from latlngBuilder to set into map camera
        LatLngBounds bounds = builder.build();
       // create the camera with bounds and padding to set into map
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        //call the map call back to know map is loaded or not
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //set animated zoom camera into map
                mMap.animateCamera(cu);

            }
        });*/
        // BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);

    }
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_red);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

}