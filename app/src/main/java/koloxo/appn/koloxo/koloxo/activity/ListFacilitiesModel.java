package koloxo.appn.koloxo.koloxo.activity;

/**
 * Created by appzoc-php on 15/3/18.
 */

public class ListFacilitiesModel{
    private String FirstText="";
    private String SecondText="";
    private String ThirdText="";

    private int FirstPos = 0;
    private int SecondPos = 0;
    private int ThirdPos = 0;

    /*********** Set Methods ******************/
    public void setFirstButton(String firsttext, int i) {
        this.FirstText = firsttext;
        this.FirstPos = i;
    }

    public void setSecondButton(String secondtext, int i) {
        this.SecondText = secondtext;
        this.SecondPos = i;
    }

    public void setThirdButton(String thirdtext, int i) {
        this.ThirdText = thirdtext;
        this.ThirdPos = i;
    }

    /*********** Get Methods ****************/
    public String getFirstButton() {
        return this.FirstText;
    }

    public String getSecondButton() {
        return this.SecondText;
    }

    public String getThirdButton() {
        return this.ThirdText;
    }
    public int getFirstPos() {
        return this.FirstPos;
    }

    public int getSecondPos() {
        return this.SecondPos;
    }

    public int getThirdPos() {
        return this.ThirdPos;
    }
}
