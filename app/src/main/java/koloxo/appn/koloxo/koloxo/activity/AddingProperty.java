package koloxo.appn.koloxo.koloxo.activity;

import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.MainLanding.Search_frm_home;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SideMenu.SideMenuPage;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDashboard;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyRequest;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MySearches;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyWishlist;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Notification;
import koloxo.appn.koloxo.koloxo.fragments.BuyFrag;
import koloxo.appn.koloxo.koloxo.fragments.Buy_Adding;
import koloxo.appn.koloxo.koloxo.fragments.RentFrag;
import koloxo.appn.koloxo.koloxo.fragments.Rent_Adding;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class AddingProperty extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    SideMenuPage sideMenuPage_main;
    //side menu clicks initialization
    TextView home_side,dash_side,request_side,history_side,notify_side,mywishlist_side,mysearches_side;
  /*  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_property);*/

  @Override
  public void onResume() {
      super.onResume();
      ((MainLandPage)getActivity()).set_Title("Post property");
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
      View view=inflater.inflate(R.layout.activity_adding_property, container, false);
      bottom_bar.setVisibility(View.INVISIBLE);



        viewPager = (ViewPager)view.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout)view.findViewById(R.id.collapsingToolbarLayout);
        collapsingToolbarLayout.setTitle("What do others know about your property?");
      /*  collapsingToolbarLayout.setCollapsedTitleTextColor(getResources().getColor(R.color.white));*/
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.white));
        collapsingToolbarLayout.setExpandedTitleMarginEnd(10);



        return view;
    }
    private void setupViewPager(ViewPager viewPager) {
        AddingProperty.ViewPagerAdapter adapter = new AddingProperty.ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new Buy_Adding(), "BUY");
        adapter.addFragment(new Rent_Adding(), "RENT");

        viewPager.setAdapter(adapter);
    }
    class ViewPagerAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


}
