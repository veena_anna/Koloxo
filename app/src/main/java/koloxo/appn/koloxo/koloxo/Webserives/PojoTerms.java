package koloxo.appn.koloxo.koloxo.Webserives;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by appzoc-php on 6/4/18.
 */

public class PojoTerms {


    @SerializedName("ErrorCode")
    @Expose
    private String errorCode;
    @SerializedName("Data")
    @Expose
    private List<TermsData> data = null;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public List<TermsData> getData() {
        return data;
    }

    public void setData(List<TermsData> data) {
        this.data = data;
    }


}
