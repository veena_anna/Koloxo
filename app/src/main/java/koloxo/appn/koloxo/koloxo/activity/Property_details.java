package koloxo.appn.koloxo.koloxo.activity;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;

import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sembozdemir.viewpagerarrowindicator.library.ViewPagerArrowIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.BaseFragment;
import koloxo.appn.koloxo.koloxo.Fragmentbase;
import koloxo.appn.koloxo.koloxo.Objects.InfoWindowData;
import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.CircleIndicator_Adapter;
import koloxo.appn.koloxo.koloxo.adapter.Detalpge_facilty_adptr;
import koloxo.appn.koloxo.koloxo.adapter.Detalpge_spaces_adaptr;
import koloxo.appn.koloxo.koloxo.adapter.Furnitr_of_prprtydetail_adapt;
import koloxo.appn.koloxo.koloxo.adapter.Google_Map_custom_adapter;
import koloxo.appn.koloxo.koloxo.adapter.News_feed_adaptr;
import koloxo.appn.koloxo.koloxo.adapter.Post_prprty_part2;
import koloxo.appn.koloxo.koloxo.adapter.proprty_img_detl_adapter;
import me.relex.circleindicator.CircleIndicator;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Koloxo;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class Property_details extends Activitybase implements OnMapReadyCallback,LocationListener {

    private static final int HEIGHT =100 ;
    GridView list_facility,list_spaces;
    RelativeLayout back_detail_full;
    CircleIndicator indicator_newsfeed;
    ViewPager viewpager_newsfeed;
    TextView Proprty_name,Prpty_loc;
    private int currentPage = 0;
    private int NUM_PAGES = 0;
    //more cases
    TextView more_id_prprty,more_id_prprty1;
    RelativeLayout more_id_facilities;
    RelativeLayout more_id_facilities1;
    RelativeLayout more_spaces11,more_spaces1;
    TextView desc_abut_prpty;
    CardView card_facilty_id;

    //ArrayList<Integer> array_image_news;
    ArrayList<String> array_txt_newshead;
    ArrayList<String> array_txt_news_sub;
    News_feed_adaptr circleIndicator_adapter;
    Timer swipeTimer;
    public static final Integer[] images = { R.drawable.splash_pic_1,
            R.drawable.splash_pic_2,R.drawable.splash_pic_1,
            R.drawable.splash_pic_2,R.drawable.splash_pic_1,
            R.drawable.splash_pic_2,R.drawable.splash_pic_2,R.drawable.splash_pic_1,
            R.drawable.splash_pic_2};

    public static final Integer[] images_faci= { R.drawable.ic_bed,
            R.drawable.ic_bed,R.drawable.ic_bed,
            R.drawable.ic_bed,R.drawable.ic_bed,
            R.drawable.ic_bed,R.drawable.ic_bed,R.drawable.ic_bed,
            R.drawable.ic_bed};
    public static final String[] titles = new String[] { "AC",
            "AC", "AC", "AC" ,"AC","AC","AC","AC","AC"};
    List<Side_mysearchobj> rowItems;
    List<Side_mysearchobj> rowItems1;
    //for map
    private GoogleMap mMap;
    LocationListener mListener;
    LatLngBounds.Builder builder;
    CameraUpdate cu;
    RecyclerView recyclerView;

    String[] header = {"Bedroom", "Kitchen", "Guest Room", "Balcony", "OtherFurniture"};

    private ViewPager viewPager;
    private ViewPagerArrowIndicator viewPagerArrowIndicator;
    ArrayList<Integer> array_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_details);
        bottom_bar.setVisibility(View.VISIBLE);
        more_id_prprty=(TextView)findViewById(R.id.more_id_prprty);
        more_id_prprty1=(TextView)findViewById(R.id.more_id_prprty1);
        more_id_facilities=(RelativeLayout) findViewById(R.id.more_id_facilities);
        more_id_facilities1=(RelativeLayout) findViewById(R.id.more_id_facilities1);
        more_spaces11=(RelativeLayout) findViewById(R.id.more_spaces11);
        more_spaces1=(RelativeLayout) findViewById(R.id.more_spaces1);
        card_facilty_id=(CardView) findViewById(R.id.card_facilty_id);
        desc_abut_prpty=(TextView) findViewById(R.id.desc_abut_prpty);


        viewpager_newsfeed = (ViewPager) findViewById(R.id.viewpger_news);
        indicator_newsfeed = (CircleIndicator)findViewById(R.id.indicator_news);
        Proprty_name=(TextView)findViewById(R.id.proprty_name) ;
        Prpty_loc=(TextView)findViewById(R.id.prpty_location) ;

        back_detail_full=(RelativeLayout)findViewById(R.id.back_detail_full);
        back_detail_full.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Proprty_name.setTypeface(Koloxo);
        Prpty_loc.setTypeface(Koloxo);
       /* array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.splash_pic_2);
        array_image.add(R.drawable.splash_pic_1);
        array_image.add(R.drawable.splash_pic_2);*/
        array_txt_newshead=new ArrayList<>();
        array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        array_txt_news_sub=new ArrayList<>();
        array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
        array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
        array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
        initImageSlider();

        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i],titles[i]);
            rowItems.add(item);
        }rowItems1 = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images_faci[i],titles[i]);
            rowItems1.add(item);
        }


        list_facility=(GridView)findViewById(R.id.details_facilities_list) ;

        list_spaces=(GridView)findViewById(R.id.spaces_list) ;



        //

       // ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) desc_abut_prpty.getLayoutParams();
      //  params.height = 80;
        desc_abut_prpty.setMaxLines(300);
       // desc_abut_prpty.setLayoutParams(params);
        more_id_prprty.setVisibility(View.VISIBLE);
        more_id_prprty1.setVisibility(View.INVISIBLE);


       /* ViewGroup.LayoutParams params1 = (ViewGroup.LayoutParams) list_facility.getLayoutParams();
        params1.height = 120;
        list_facility.setLayoutParams(params1);*/
        more_id_facilities.setVisibility(View.VISIBLE);
        more_id_facilities1.setVisibility(View.INVISIBLE);




        ViewGroup.LayoutParams params01 = (ViewGroup.LayoutParams) list_spaces.getLayoutParams();
        params01.height = 510;
        list_spaces.setLayoutParams(params01);
        more_spaces1.setVisibility(View.VISIBLE);
        more_spaces11.setVisibility(View.INVISIBLE);

        //desc_abut_prpty.getLayoutParams().height=20;
        //list_facility.getLayoutParams().height=120;

        Detalpge_facilty_adptr adapter = new Detalpge_facilty_adptr(getApplicationContext(), rowItems1,true);
        list_facility.setAdapter(adapter);

        Detalpge_spaces_adaptr adapter1 = new Detalpge_spaces_adaptr(getApplicationContext(), rowItems);
        list_spaces.setAdapter(adapter1);
        //for map
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        //getSupportFragmentManager()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment)getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//for furniture details
        recyclerView =findViewById(R.id.recyclerView);

        List<Pojo_postprpty_part3> mypojos = new ArrayList<>();
        for (int i = 0; i < header.length; i++) {
            Pojo_postprpty_part3 mypojo = new Pojo_postprpty_part3();
            mypojo.setName(header[i]);
            mypojo.setSelected(0);
            mypojos.add(mypojo);
        }
        recyclerView.setBackgroundColor(Color.parseColor("#F6F7F8"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(new Furnitr_of_prprtydetail_adapt(getApplicationContext(), mypojos));
        //scrolling imges
        viewPager = (ViewPager) findViewById(R.id.viewPager);
       // viewPagerArrowIndicator = (ViewPagerArrowIndicator) findViewById(R.id.img_detail1);
        array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.splash_pic_2);
        array_image.add(R.drawable.splash_pic_1);
        array_image.add(R.drawable.splash_pic_1);
       // viewPagerArrowIndicator.setArrowIndicatorRes(R.drawable.ic_left_arrow, R.drawable.ic_arrow_point_to_right);
        viewPager.setAdapter(new proprty_img_detl_adapter(array_image, Property_details.this));
       // viewPagerArrowIndicator.bind(viewPager);


       final ImageButton leftNav = (ImageButton) findViewById(R.id.left_nav);
      ImageButton  rightNav = (ImageButton) findViewById(R.id.right_nav);

// Images left navigation
        leftNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewPager.setCurrentItem(tab);
                   // leftNav.setVisibility(View.INVISIBLE);
                }
            }
        });

        // Images right navigatin
        rightNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);

            }
        });

        more_id_prprty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  desc_abut_prpty.getLayoutParams().height= 150;
                more_id_prprty.setVisibility(View.INVISIBLE);
                more_id_prprty1.setVisibility(View.VISIBLE);
                desc_abut_prpty.setMaxLines(300);
               /* ViewGroup.LayoutParams params1 = (ViewGroup.LayoutParams) desc_abut_prpty.getLayoutParams();
                params1.height = 180;
                desc_abut_prpty.setLayoutParams(params1);*/
            }
        });
        more_id_prprty1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  desc_abut_prpty.getLayoutParams().height= 150;
                more_id_prprty1.setVisibility(View.INVISIBLE);
                more_id_prprty.setVisibility(View.VISIBLE);
               /* ViewGroup.LayoutParams params1 = (ViewGroup.LayoutParams) desc_abut_prpty.getLayoutParams();
                params1.height = 80;
                desc_abut_prpty.setLayoutParams(params1);*/
                desc_abut_prpty.setMaxLines(150);
            }
        });

        more_id_facilities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // list_facility.getLayoutParams().height=400;
                //card_facilty_id.getLayoutParams().height= 400;
                more_id_facilities1.setVisibility(View.VISIBLE);
                more_id_facilities.setVisibility(View.INVISIBLE);

                Detalpge_facilty_adptr adapter = new Detalpge_facilty_adptr(getApplicationContext(), rowItems1,false);
                list_facility.setAdapter(adapter);

                ViewGroup.LayoutParams params1 = (ViewGroup.LayoutParams) list_facility.getLayoutParams();
                params1.height =150;
                list_facility.setLayoutParams(params1);

               /* card_facilty_id.setLayoutParams(params1);ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) card_facilty_id.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                card_facilty_id.setLayoutParams(params);*/
            }
        });
        more_id_facilities1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // list_facility.getLayoutParams().height=400;
                //card_facilty_id.getLayoutParams().height= 400;
                more_id_facilities.setVisibility(View.VISIBLE);
                more_id_facilities1.setVisibility(View.INVISIBLE);
 ViewGroup.LayoutParams params1 = (ViewGroup.LayoutParams) list_facility.getLayoutParams();
                params1.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                list_facility.setLayoutParams(params1);
                Detalpge_facilty_adptr adapter = new Detalpge_facilty_adptr(getApplicationContext(), rowItems1,true);
                list_facility.setAdapter(adapter);

               /* card_facilty_id.setLayoutParams(params1);ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) card_facilty_id.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                card_facilty_id.setLayoutParams(params);*/
            }
        });
        more_spaces1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // list_facility.getLayoutParams().height=400;
                //card_facilty_id.getLayoutParams().height= 400;
                more_spaces11.setVisibility(View.VISIBLE);
                more_spaces1.setVisibility(View.INVISIBLE);
                ViewGroup.LayoutParams params1 = (ViewGroup.LayoutParams) list_spaces.getLayoutParams();
                params1.height = 950 ;
                list_spaces.setLayoutParams(params1);

               /* card_facilty_id.setLayoutParams(params1);ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) card_facilty_id.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                card_facilty_id.setLayoutParams(params);*/
            }
        });
        more_spaces11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // list_facility.getLayoutParams().height=400;
                //card_facilty_id.getLayoutParams().height= 400;
                more_spaces1.setVisibility(View.VISIBLE);
                more_spaces11.setVisibility(View.INVISIBLE);
                ViewGroup.LayoutParams params1 = (ViewGroup.LayoutParams) list_spaces.getLayoutParams();
                params1.height = 510;
                list_spaces.setLayoutParams(params1);

               /* card_facilty_id.setLayoutParams(params1);ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) card_facilty_id.getLayoutParams();
                params.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                card_facilty_id.setLayoutParams(params);*/
            }
        });
    }
    public static int dpToPx(int dp) {
        float density = getHpBaseActivity().getResources()
                .getDisplayMetrics()
                .density;
        return Math.round((float) dp * density);
    }
    private void initImageSlider() {
      circleIndicator_adapter = new News_feed_adaptr(array_txt_newshead,array_txt_news_sub,Property_details.this);
        viewpager_newsfeed.setAdapter(circleIndicator_adapter);
   indicator_newsfeed.setViewPager(viewpager_newsfeed);
      viewpager_newsfeed.setCurrentItem(0);
  indicator_newsfeed.setViewPager(viewpager_newsfeed);
     circleIndicator_adapter.registerDataSetObserver(indicator_newsfeed.getDataSetObserver());
      NUM_PAGES = array_txt_newshead.size();
        if (NUM_PAGES > 1) {
            indicator_newsfeed.setVisibility(View.VISIBLE);
        } else {
            indicator_newsfeed.setVisibility(View.GONE);
        }


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;


                }
                viewpager_newsfeed.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 0, 2000);

        // Pager listener over indicator
        indicator_newsfeed.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }


    @Override
    public void onLocationChanged(Location location) {
        if( mListener != null )
        {
            mListener.onLocationChanged( location );}
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //april 10 change
        mMap = googleMap;
        /*try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getApplicationContext(), R.raw.style_json));

            if (!success) {
                Log.e("N", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("N", "Can't find style. Error: ", e);
        }*/



        // new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location));
        // Add a marker in Sydney and move the camera

        // LatLng sydney = new LatLng(-34, 151);
        //  BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
        //  mMap.addMarker(new MarkerOptions().position(sydney).icon(bitmapDescriptorFromVector(this, R.drawable.ic_location)).title("Marker in Sydney"));

        //  mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));


        /**clear the map before redraw to them*/

        //april 10 change
       mMap.clear();


        /**Create dummy Markers List*/
        List<Marker> markersList = new ArrayList<Marker>();
        Marker Delhi = mMap.addMarker(new MarkerOptions().position(new LatLng(
                9.997090, 76.302815)).icon(bitmapDescriptorFromVector(getApplicationContext(), R.drawable.button_bg_unselect)).title("Kaloor"));
        /**Put all the markers into arraylist*/
        markersList.add(Delhi);
        /**create for loop for get the latLngbuilder from the marker list*/
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }

//april 10
       /* Google_Map_custom_adapter customInfoWindow = new Google_Map_custom_adapter(getApplicationContext());
        mMap.setInfoWindowAdapter(customInfoWindow);
        InfoWindowData info = new InfoWindowData();
        info.setImage("snowqualmie");
        info.setHotel("Hotel : excellent hotels available");
        info.setFood("Food : all types of restaurants available");
        info.setTransport("Reach the site by bus, car and train.");*/


        /*Marker m = mMap.addMarker(markersList);
        m.setTag(info);
        m.showInfoWindow();*/

        // create the camera with bounds and padding to set into map
        LatLng kaloor = new LatLng(9.997090, 76.302815);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(kaloor));

       /* //initialize the padding for map boundary
        int padding = 50;
        //create the bounds from latlngBuilder to set into map camera
        LatLngBounds bounds = builder.build();
       // create the camera with bounds and padding to set into map
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        //call the map call back to know map is loaded or not
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //set animated zoom camera into map
                mMap.animateCamera(cu);

            }
        });*/
        // BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);

    }
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_blue);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

}

