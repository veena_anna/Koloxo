package koloxo.appn.koloxo.koloxo.activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.R;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

/**
 * A simple {@link Fragment} subclass.
 */
public class Chat_Section extends Fragment {
ImageView chat_sending;

    public Chat_Section() {
        // Required empty public constructor
    }


    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Dallas Cape Suite");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_chat__section, container, false);
        bottom_bar.setVisibility(View.VISIBLE);
        chat_sending=view.findViewById(R.id.ch_sending);
        chat_sending.setImageResource(R.drawable.ic_send_chatng);

        return view;
    }

}
