package koloxo.appn.koloxo.koloxo.MainLanding;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import koloxo.appn.koloxo.koloxo.BaseActivity;
import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.MainActivity;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.SideMenu.SideMenuPage;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDashboard;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyRequest;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MySearches;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyWishlist;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Notification;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Notification_side;
import koloxo.appn.koloxo.koloxo.activity.AddingProperty;
import koloxo.appn.koloxo.koloxo.activity.ChatBasePage;
import koloxo.appn.koloxo.koloxo.activity.LoginPage;
import koloxo.appn.koloxo.koloxo.activity.ProfilePage;
import koloxo.appn.koloxo.koloxo.activity.Profile_startup1;

public class MainLandPage extends BaseActivity {
    SideMenuPage sideMenuPage_main;
    android.support.v4.app.FragmentTransaction ftr;
    Search_frm_home search_frm_hme;
    public static TextView Title;
    public static Typeface Koloxo;
    ImageView bottom_4_icn, bottom_5_icn;

    private Toolbar toolbar;
    //back arrow
    public static RelativeLayout back_arrow;
    //side menu clicks initialization
    TextView home_side, dash_side, request_side, history_side, notify_side, mywishlist_side, mysearches_side;
    RelativeLayout login_side,log_out;
    String YourtransferredData;
    public static RelativeLayout bottom_bar;
    ImageView profile_edit, profile_edit_ic, profile_pic;
    String YourSuccess;
    String value_fr_home;
    TextView support_call;
    int sample;
    ImageView active,locat;
    TextView   locat_address;
    Button landing_notify;
    Button landing_token1,landing_token2;
    Session_class session_class;

    @Override
    protected void onResume() {
        super.onResume();
        Check_MainLandingPage();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_land_page);
        Intent intent = getIntent();

        YourSuccess = intent.getExtras().getString("YourKey");
        value_fr_home = intent.getExtras().getString("YourKeyhome");
      /*  if(YourSuccess.equals(null)) {*/
        if (YourSuccess == null) {
            YourtransferredData = intent.getExtras().getString("YourValueKey");
        }

        //calling sidemenu class
        sideMenuPage_main = new SideMenuPage(MainLandPage.this);
        sideMenuPage_main.setupSlidemenu(MainLandPage.this);

        sideMenuPage_main.setAnimations(MainLandPage.this);

        Koloxo = Typeface.createFromAsset(getAssets(), "fonts/OpenSans-Regular.ttf");
        SetupActionbar();
        initializing_var();
        initializePage();
        session_class=new Session_class(MainLandPage.this);


        Check_MainLandingPage();



        //side menu clicks
        home_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //sideMenuPage_main.mDrawerLayout.closeDrawers();
                finish();
                // Toast.makeText(getApplicationContext(),"You clicked home_side",Toast.LENGTH_SHORT).show();
            }

        });

        support_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("XXXX", "onClick: cal clicked");

                String uri = "tel:" + "+34 662 121 468";
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse(uri));

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(Intent.createChooser(intent, "Call with"));
                } else {
                    Log.d("XXXX", "onClick:  no App installed");
                    //MyDialogHelper.showSnackbar(context, "No dialer app Installed");
                }



            }
        });


    }

    public void initializing_var() {
        bottom_4_icn = (ImageView) findViewById(R.id.bt_b4);
        bottom_5_icn = (ImageView) findViewById(R.id.bt_b1);


        // bottom_ic_view.setImageResource(R.drawable.ic_eye_i
        //declaration back arror
        back_arrow = (RelativeLayout) findViewById(R.id.back_srch_main);
        //declaration for content in sidemenu
        home_side = (TextView) findViewById(R.id.side_l3_home);
        dash_side = (TextView) findViewById(R.id.side_l3_dash);
        request_side = (TextView) findViewById(R.id.side_l3_rqt);
        history_side = (TextView) findViewById(R.id.side_l3_his);
        notify_side = (TextView) findViewById(R.id.side_l3_notify);
        mywishlist_side = (TextView) findViewById(R.id.side_l3_wish);
        mysearches_side = (TextView) findViewById(R.id.side_l3_srch);
        login_side = (RelativeLayout) findViewById(R.id.side_l4);

        profile_edit = (ImageView) findViewById(R.id.side_l2_img_pic_fade);
        profile_edit_ic = (ImageView) findViewById(R.id.ic_profile_edit);
        profile_pic = (ImageView) findViewById(R.id.side_l2_img_pic);

        bottom_bar = (RelativeLayout) findViewById(R.id.bottom_bar);
        support_call=findViewById(R.id.support_call);

        active=(ImageView) findViewById(R.id.active);
        locat=(ImageView) findViewById(R.id.ic_address_side);
        locat_address=(TextView)findViewById(R.id.addes_side);
        log_out=(RelativeLayout)findViewById(R.id.side_l5);
        landing_notify=(Button)findViewById(R.id.landing_notify);
        landing_token1=(Button)findViewById(R.id.landing_token1);
        landing_token2=(Button)findViewById(R.id.landing_token2);
    }
public void Check_MainLandingPage()
{
    if (session_class.getId()==null)
    {sample=MyConstants.LoggedIn.FALSE;

    }
    else {
        sample=MyConstants.LoggedIn.TRUE;
    }
    if(sample==MyConstants.LoggedIn.FALSE)
    //loged in not
    {
        active.setVisibility(View.INVISIBLE);
        locat.setVisibility(View.INVISIBLE);

        locat_address.setVisibility(View.INVISIBLE);
        landing_notify.setVisibility(View.INVISIBLE);
        login_side.setVisibility(View.VISIBLE);
        log_out.setVisibility(View.INVISIBLE);
        profile_edit.setVisibility(View.INVISIBLE);
        profile_edit_ic.setVisibility(View.INVISIBLE);
        bottom_4_icn.setImageResource(R.drawable.ic_eye_main);
        bottom_5_icn.setImageResource(R.drawable.ic_profile_main);
        landing_token1.setVisibility(View.INVISIBLE);
        landing_token2.setVisibility(View.INVISIBLE);
        dash_side.setClickable(false);
        request_side.setClickable(false);
        history_side.setClickable(false);
        notify_side.setClickable(false);
        mywishlist_side.setClickable(false);

        dash_side.setTextColor(Color.parseColor("#8F73EB"));
        request_side.setTextColor(Color.parseColor("#8F73EB"));
        history_side.setTextColor(Color.parseColor("#8F73EB"));
        notify_side.setTextColor(Color.parseColor("#8F73EB"));
        mywishlist_side.setTextColor(Color.parseColor("#8F73EB"));
        login_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginPage.class);
                startActivity(intent);
                sideMenuPage_main.mDrawerLayout.closeDrawers();


            }
        });
        mysearches_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MySearches mySearches = new MySearches();
                Title.setText("Recently Viewed");
                Title.setTypeface(Koloxo);
//                if (YourSuccess == null) {
//
//                    if (YourtransferredData.equals("SEARCH PAGE")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    }
//                } else {
//                    if (YourSuccess.equals("success_registration")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    }
//                }
                if (YourSuccess == null && value_fr_home == null) {
                    if (YourtransferredData.equals("SEARCH PAGE")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }
                } else if (value_fr_home == null && YourtransferredData == null) {
                    if (YourSuccess.equals("success_registration")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }

                } else if (YourSuccess == null && YourtransferredData == null) {
                    if (value_fr_home.equals("MySearchFromhome")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }
                    else if(value_fr_home.equals("MyDashFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyRequestFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyHistoryFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyNotifyFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyWishlistFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }
                }

                sideMenuPage_main.mDrawerLayout.closeDrawers();
                //Toast.makeText(getApplicationContext(),"You clicked mysearches_side",Toast.LENGTH_SHORT).show();
            }
        });
        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1=new Intent(MainLandPage.this,LoginPage.class);
                startActivity(intent1);
                sideMenuPage_main.mDrawerLayout.closeDrawers();

            }
        });
    }
    else if(sample== MyConstants.LoggedIn.TRUE)
//success login
    {    active.setVisibility(View.VISIBLE);
        locat.setVisibility(View.VISIBLE);
        landing_token1.setVisibility(View.VISIBLE);
        landing_token2.setVisibility(View.VISIBLE);
        locat_address.setVisibility(View.VISIBLE);
        landing_notify.setVisibility(View.VISIBLE);
        log_out.setVisibility(View.VISIBLE);
        login_side.setVisibility(View.INVISIBLE);
        bottom_4_icn.setImageResource(R.drawable.ic_comment);
        bottom_5_icn.setImageResource(R.drawable.ic_notification);

        //  profile_edit.setVisibility(View.VISIBLE);
        // profile_edit_ic.setVisibility(View.VISIBLE);
        // profile_edit.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
        //profile_edit_ic.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));

        dash_side.setClickable(true);
        request_side.setClickable(true);
        history_side.setClickable(true);
        notify_side.setClickable(true);
        mywishlist_side.setClickable(true);

        dash_side.setTextColor(Color.parseColor("#ffffff"));
        request_side.setTextColor(Color.parseColor("#ffffff"));
        history_side.setTextColor(Color.parseColor("#ffffff"));
        notify_side.setTextColor(Color.parseColor("#ffffff"));
        mywishlist_side.setTextColor(Color.parseColor("#ffffff"));

        dash_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (YourSuccess == null && value_fr_home == null) {
                    if (YourtransferredData.equals("SEARCH PAGE")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }

                    else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }
                } else if (value_fr_home == null && YourtransferredData == null) {
                    if (YourSuccess.equals("success_registration")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }

                } else if (YourSuccess == null && YourtransferredData == null) {
                    if (value_fr_home.equals("MySearchFromhome")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }
                    else if(value_fr_home.equals("MyDashFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }else if(value_fr_home.equals("MyRequestFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }else if(value_fr_home.equals("MyHistoryFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }else if(value_fr_home.equals("MyNotifyFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }else if(value_fr_home.equals("MyWishlistFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }
                    else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new MyDashboard(), true, true);
                    }
                }


// ********************************************xxx***************
               /* myFun(new MyDashboard());*/


         /*       Title.setText("My Dashboard");*/
                Title.setTypeface(Koloxo);
                sideMenuPage_main.mDrawerLayout.closeDrawers();
                //Toast.makeText(getApplicationContext(),"You clicked dash_side",Toast.LENGTH_SHORT).show();

            }
        });

        request_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyRequest myRequest = new MyRequest();
                Title.setText("My Request");
                Title.setTypeface(Koloxo);
//                if (YourSuccess == null) {
//                    if (YourtransferredData.equals("SEARCH PAGE")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myRequest, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myRequest, true, true);
//                    }
//                } else {
//                    if (YourSuccess.equals("success_registration")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myRequest, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myRequest, true, true);
//                    }
//                }
//
                if (YourSuccess == null && value_fr_home == null) {
                    if (YourtransferredData.equals("SEARCH PAGE")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }
                } else if (value_fr_home == null && YourtransferredData == null) {
                    if (YourSuccess.equals("success_registration")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }

                } else if (YourSuccess == null && YourtransferredData == null) {
                    if (value_fr_home.equals("MySearchFromhome")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }
                    else if(value_fr_home.equals("MyDashFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }else if(value_fr_home.equals("MyRequestFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }else if(value_fr_home.equals("MyHistoryFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }else if(value_fr_home.equals("MyNotifyFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }else if(value_fr_home.equals("MyWishlistFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }
                    else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myRequest, true, true);
                    }
                }
                sideMenuPage_main.mDrawerLayout.closeDrawers();
                // Toast.makeText(getApplicationContext(),"You clicked request_side",Toast.LENGTH_SHORT).show();
            }
        });
        history_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyHistory myHistory = new MyHistory();
                Title.setText("My History");
                Title.setTypeface(Koloxo);
//                if (YourSuccess == null) {
//                    if (YourtransferredData.equals("SEARCH PAGE")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myHistory, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myHistory, true, true);
//                    }
//                } else {
//                    if (YourSuccess.equals("success_registration")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myHistory, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myHistory, true, true);
//                    }
//                }

                if (YourSuccess == null && value_fr_home == null) {
                    if (YourtransferredData.equals("SEARCH PAGE")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }
                } else if (value_fr_home == null && YourtransferredData == null) {
                    if (YourSuccess.equals("success_registration")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }

                } else if (YourSuccess == null && YourtransferredData == null) {
                    if (value_fr_home.equals("MySearchFromhome")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }
                    else if(value_fr_home.equals("MyDashFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }else if(value_fr_home.equals("MyRequestFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }else if(value_fr_home.equals("MyHistoryFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }else if(value_fr_home.equals("MyNotifyFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }else if(value_fr_home.equals("MyWishlistFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }
                    else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myHistory, true, true);
                    }
                }
                // pushFragments(myHistory,true,true);
                sideMenuPage_main.mDrawerLayout.closeDrawers();
                // Toast.makeText(getApplicationContext(),"You clicked history_side",Toast.LENGTH_SHORT).show();
            }
        });
        notify_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Notification");
                Title.setTypeface(Koloxo);
//                if (YourSuccess == null) {
//                    if (YourtransferredData.equals("SEARCH PAGE")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(new Notification(), true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(new Notification(), true, true);
//                    }
//                } else {
//                    if (YourSuccess.equals("success_registration")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(new Notification(), true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(new Notification(), true, true);
//                    }
//                }
                if (YourSuccess == null && value_fr_home == null) {
                    if (YourtransferredData.equals("SEARCH PAGE")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }
                } else if (value_fr_home == null && YourtransferredData == null) {
                    if (YourSuccess.equals("success_registration")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }

                } else if (YourSuccess == null && YourtransferredData == null) {
                    if (value_fr_home.equals("MySearchFromhome")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }
                    else if(value_fr_home.equals("MyDashFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }else if(value_fr_home.equals("MyRequestFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }else if(value_fr_home.equals("MyHistoryFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }else if(value_fr_home.equals("MyNotifyFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }else if(value_fr_home.equals("MyWishlistFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(new Notification_side(), true, true);
                    }
                }
                pushFragments(new Notification_side(), true, true);
                sideMenuPage_main.mDrawerLayout.closeDrawers();
                // Toast.makeText(getApplicationContext(),"You clicked notify_side",Toast.LENGTH_SHORT).show();
            }
        });
        mywishlist_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MyWishlist myWishlist = new MyWishlist();
                Title.setText("My Wishlist");
                Title.setTypeface(Koloxo);
//                if (YourSuccess == null) {
//                    if (YourtransferredData.equals("SEARCH PAGE")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myWishlist, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myWishlist, true, true);
//                    }
//                } else {
//                    if (YourSuccess.equals("success_registration")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myWishlist, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(myWishlist, true, true);
//                    }
//                }
                if (YourSuccess == null && value_fr_home == null) {
                    if (YourtransferredData.equals("SEARCH PAGE")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }
                } else if (value_fr_home == null && YourtransferredData == null) {
                    if (YourSuccess.equals("success_registration")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }

                    else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }

                } else if (YourSuccess == null && YourtransferredData == null) {
                    if (value_fr_home.equals("MySearchFromhome")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }
                    else if(value_fr_home.equals("MyDashFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }else if(value_fr_home.equals("MyRequestFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }else if(value_fr_home.equals("MyHistoryFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }else if(value_fr_home.equals("MyNotifyFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }else if(value_fr_home.equals("MyWishlistFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(myWishlist, true, true);
                    }
                }

                pushFragments(myWishlist, true, true);
                // myFun(myWishlist);
                sideMenuPage_main.mDrawerLayout.closeDrawers();
                // Toast.makeText(getApplicationContext(),"You clicked my wishlist_side",Toast.LENGTH_SHORT).show();
            }
        });
        mysearches_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MySearches mySearches = new MySearches();
                Title.setText("Recently Viewed");
                Title.setTypeface(Koloxo);
//                if (YourSuccess == null) {
//
//                    if (YourtransferredData.equals("SEARCH PAGE")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    }
//                } else {
//                    if (YourSuccess.equals("success_registration")) {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    } else {
//                        bottom_bar.setVisibility(View.VISIBLE);
//                        pushFragments(mySearches, true, true);
//                    }
//                }
                if (YourSuccess == null && value_fr_home == null) {
                    if (YourtransferredData.equals("SEARCH PAGE")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }
                } else if (value_fr_home == null && YourtransferredData == null) {
                    if (YourSuccess.equals("success_registration")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    } else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }

                } else if (YourSuccess == null && YourtransferredData == null) {
                    if (value_fr_home.equals("MySearchFromhome")) {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }
                    else if(value_fr_home.equals("MyDashFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyRequestFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyHistoryFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyNotifyFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else if(value_fr_home.equals("MyWishlistFromhome")){
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }else {
                        bottom_bar.setVisibility(View.VISIBLE);
                        pushFragments(mySearches, true, true);
                    }
                }

                sideMenuPage_main.mDrawerLayout.closeDrawers();
                //Toast.makeText(getApplicationContext(),"You clicked mysearches_side",Toast.LENGTH_SHORT).show();
            }
        });
        profile_pic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                profile_edit.setVisibility(View.VISIBLE);
                profile_edit_ic.setVisibility(View.VISIBLE);
                profile_edit.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
                profile_edit_ic.startAnimation(AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in));
                Title.setText("Profile");
                sideMenuPage_main.mDrawerLayout.closeDrawers();
                pushFragments(new ProfilePage(), true, true);

            }
        });
        log_out.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                session_class.logoutUser();
                sideMenuPage_main.mDrawerLayout.closeDrawers();
                Check_MainLandingPage();
                finish();
            }
        });
    }
}
    public void MenuBtnMain(View view) {

        /*Picasso.with(getApplicationContext()).load(R.drawable.ic_menu).into(MenuButton);*/
        sideMenuPage_main.mDrawerLayout.openDrawer(Gravity.RIGHT);

    }

    private void SetupActionbar() {
        Title = (TextView) findViewById(R.id.action_bar_title);


    }

    private void initializePage() {
        //myFun(new Search_frm_home());

        if (YourSuccess == null) {
            if (YourtransferredData == null) {
                if (value_fr_home.equals("MySearchFromhome")) {
                    Title.setText("Recently Viewed");
                    pushFragments(new MySearches(), false, true);
                }
                else if(value_fr_home.equals("MyDashFromhome")){
                    bottom_bar.setVisibility(View.VISIBLE);
                    pushFragments(new MyDashboard(), true, true);
                }else if(value_fr_home.equals("MyRequestFromhome")){
                    bottom_bar.setVisibility(View.VISIBLE);
                    pushFragments(new MyRequest(), true, true);
                }else if(value_fr_home.equals("MyHistoryFromhome")){
                    bottom_bar.setVisibility(View.VISIBLE);
                    pushFragments(new MyHistory(), true, true);
                }else if(value_fr_home.equals("MyNotifyFromhome")){
                    bottom_bar.setVisibility(View.VISIBLE);
                    pushFragments(new Notification_side(), true, true);
                }else if(value_fr_home.equals("MyWishlistFromhome")){
                    bottom_bar.setVisibility(View.VISIBLE);
                    pushFragments(new MyWishlist(), true, true);
                }
            } else if (YourtransferredData.equals("SEARCH PAGE")) {
                Title.setText("Search");
                pushFragments(new Search_frm_home(), false, true);
            } else {
                bottom_bar.setVisibility(View.INVISIBLE);
                Title.setText("Post property");

                pushFragments(new AddingProperty(), false, true);
            }

        } else {
            if (YourSuccess.equals("success_registration")) {
                Title.setText("My Profile");
                pushFragments(new Profile_startup1(), false, true);
            }

        }


    }

    //botton icon clicks
    public void Home(View view) {

        finish();
        //Toast.makeText(getApplicationContext(),"You clicked Home",Toast.LENGTH_SHORT).show();
    }

    public void Add_P(View view) {
        if(sample==0)
        {
            Intent intent =new Intent(MainLandPage.this,LoginPage.class);
            startActivity(intent);
        }
        else {
            pushFragments1(new AddingProperty(), true, true);
            //Toast.makeText(getApplicationContext(),"You clicked Add Property",Toast.LENGTH_SHORT).show();
        } }

    public void Search_Land(View view) {
        //Toast.makeText(getApplicationContext(),"You clicked search",Toast.LENGTH_SHORT).show();
    }

    public void Chat(View view) {
        if(sample==0)
        {
          Intent intent =new Intent(MainLandPage.this,LoginPage.class);
          startActivity(intent);
        }
        else {
            pushFragments1(new ChatBasePage(), true, true);
            //Toast.makeText(getApplicationContext(),"You clicked chat",Toast.LENGTH_SHORT).show();
        }

    }

    public void Notify(View view) {
        if(sample==0)
        {
            Intent intent =new Intent(MainLandPage.this,LoginPage.class);
            startActivity(intent);
        }
        else {
            pushFragments1(new Notification(), true, true);
            //Toast.makeText(getApplicationContext(),"You clicked notify",Toast.LENGTH_SHORT).show();
        }
    }

    public void Search_Main_Back(View view) {

        super.onBackPressed();

    }

    public void set_Title(String title_name) {

        Title.setText(title_name);
        //Title.setTypeface(Koloxo);

    }

   /* public void DisableClick(View view)
    {

    }*/

}
