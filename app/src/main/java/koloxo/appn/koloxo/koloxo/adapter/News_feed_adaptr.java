package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;

import android.app.Fragment;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import koloxo.appn.koloxo.koloxo.BaseFragment;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.Property_details;

/**
 * Created by Veena on 18-03-2018.
 */

public class News_feed_adaptr extends PagerAdapter {
    ArrayList<Integer> sliderPojos;
    ArrayList<String> slidetxt_hed;
    ArrayList<String> slidetxt;

    private int mSize;
   Context context;
    Activity activity;




    public News_feed_adaptr(ArrayList<String> array_txt_newshead, ArrayList<String> array_txt_news_sub,Context context) {

        this.slidetxt=array_txt_news_sub;
        this.slidetxt_hed=array_txt_newshead;

        this.context = context;
        mSize = slidetxt_hed.size();
    }



    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View itemView = LayoutInflater.from(view.getContext()).inflate(R.layout.news_feed_row, view, false);
       // ImageView imageView = (ImageView) itemView.findViewById(R.id.splash_image);
        TextView textView_head=(TextView)itemView.findViewById(R.id.newshed);
        TextView textView_sub=(TextView)itemView.findViewById(R.id.news_sub_id);

      //  imageView.setImageResource(sliderPojos.get(position));
        textView_head.setText(slidetxt_hed.get(position));
        textView_sub.setText(slidetxt.get(position));

        view.addView(itemView);

        return itemView;
    }


}