package koloxo.appn.koloxo.koloxo.adapter.MyHistory_innr;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 7/3/18.
 */

public class MyRental_prpty_myhis  extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;

    public MyRental_prpty_myhis(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView rating_myhisrent;
        TextView txtTitle;
        TextView txtDesc;
        ImageView imageView;
       // RelativeLayout checking_review_list;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MyRental_prpty_myhis.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
      if (convertView == null) {
        convertView = mInflater.inflate(R.layout.side_myhis_innr_myrent, parent,false);
         holder = new MyRental_prpty_myhis.ViewHolder();
           holder.rating_myhisrent = (TextView) convertView.findViewById(R.id.rating_myhisrent);
           holder.imageView = (ImageView) convertView.findViewById(R.id.side_my_his_inmyrent_img);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
//holder.checking_review_list = (RelativeLayout) convertView.findViewById(R.id.checking_review_list);
        convertView.setTag(holder);
        }
       else {
            holder = (MyRental_prpty_myhis.ViewHolder) convertView.getTag();
       }
        Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());
        holder.rating_myhisrent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogPop_rating dialogPop_rating=new DialogPop_rating(context);
                dialogPop_rating.initiatePopupWindow_Lawyers(context);
            }
        });


        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}