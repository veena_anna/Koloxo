package koloxo.appn.koloxo.koloxo.adapter.Mydash_inner;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.Chat_Section;
import koloxo.appn.koloxo.koloxo.activity.ProfilePage;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 7/3/18.
 */

public class Reqst_prsn_list_mydash extends BaseAdapter {
    Context context;
    List<Side_mysearchobj> rowItems;


    public Reqst_prsn_list_mydash(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        Button view_profile;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
     Reqst_prsn_list_mydash.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
      if (convertView == null) {
       convertView=mInflater.inflate(R.layout.side_mydash_rqst_persnlist, parent,false);
        holder = new Reqst_prsn_list_mydash.ViewHolder();
          holder.view_profile = (Button) convertView.findViewById(R.id.view_profile);
           holder.txtTitle = (TextView) convertView.findViewById(R.id.acceping_mydash_prsnlist);
          holder.imageView = (ImageView) convertView.findViewById(R.id.Request_prsn_list_mydash_img);
        convertView.setTag(holder);
       }
      else {
           holder = (Reqst_prsn_list_mydash.ViewHolder) convertView.getTag();
       }

       Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());
       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());

*/
        holder.txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Dallas Cape Suite");
                getHpBaseActivity().pushFragments(new Chat_Section(),true,true);
            }
        });
        holder.view_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Profile");
                getHpBaseActivity().pushFragments(new ProfilePage(),true,true);
            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}