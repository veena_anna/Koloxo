package koloxo.appn.koloxo.koloxo.Objects;

/**
 * Created by appzoc-php on 21/3/18.
 */

public class Pojo_postprpty_part3 {

    String name;
    int selected;
    String address;

    public Pojo_postprpty_part3(String name, int selected, String address) {
        this.name = name;
        this.selected = selected;
        this.address = address;
    }

    public Pojo_postprpty_part3() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSelected() {
        return selected;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
