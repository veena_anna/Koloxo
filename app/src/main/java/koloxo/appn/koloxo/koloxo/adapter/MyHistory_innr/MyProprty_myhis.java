package koloxo.appn.koloxo.koloxo.adapter.MyHistory_innr;

import android.app.Activity;
import android.content.Context;
import android.service.quicksettings.Tile;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.BaseActivity;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.Property_review_list;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 7/3/18.
 */

public class MyProprty_myhis extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;

    public MyProprty_myhis(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        RelativeLayout checking_review_list;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        MyProprty_myhis.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.side_myhis_innr_myprpty, parent,false);
            holder = new MyProprty_myhis.ViewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.Myproperty_myhis_img);
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.checking_review_list = (RelativeLayout) convertView.findViewById(R.id.checking_review_list);
            convertView.setTag(holder);
        }
        else {
            holder = (MyProprty_myhis.ViewHolder) convertView.getTag();
        }
        Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());

        holder. checking_review_list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("My History");
                BaseActivity.getHpBaseActivity().pushFragments(new Property_review_list(),true,true);
            }
        });
        // holder = new Add_propeties_mydash.ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
//            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
        //convertView.setTag(holder);
//        }
//        else {
//            holder = (Add_propeties_mydash.ViewHolder) convertView.getTag();
//        }

        // Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}