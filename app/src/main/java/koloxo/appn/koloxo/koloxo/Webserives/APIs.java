package koloxo.appn.koloxo.koloxo.Webserives;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by appzoc-php on 3/4/18.
 */

public interface APIs {
    @FormUrlEncoded
    @POST("login")
    Call<PojoitemLogin> getDetails(@Field("email") String email,
                                   @Field("password") String pass);
    @FormUrlEncoded
    @POST("forgot_password")
    Call<PojoitemReg> getForgetpass(@Field("user_id") String user_id,
                                    @Field("email") String email);

    @FormUrlEncoded
    @POST("registration")
    Call<PojoitemReg> getRegistartion(@Field("first_name") String first_name,
                                      @Field("last_name") String last_name,
                                      @Field("mobile_number") String mobile_number,
                                      @Field("email") String email,
                                      @Field("password") String password,
                                      @Field("confirm_password") String confirm_password,
                                      @Field("country_residence") String country_residence);
    }
