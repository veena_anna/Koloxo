package koloxo.appn.koloxo.koloxo.SidemenuPages;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Added_properties;

import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.MyRequest_B_s;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Request_list;
import koloxo.appn.koloxo.koloxo.Webserives.PojoDasboard;
import koloxo.appn.koloxo.koloxo.Webserives.PojoitemLogin;
import koloxo.appn.koloxo.koloxo.activity.LoginPage;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainActivity.android_id;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;


public class MyDashboard extends Fragment {
  //initilazing cards
    CardView card_myproperty,card_recvd_rqst,card_rqst_B_S;
    TextView card1_count,card1_add_prprty_txt,card2_count,card2_add_prprty_txt,card3_count,card3_add_prprty_txt;
    ImageView card1_icn,card2_icn,card3_icn;
    ImageView handshake;
    Typeface Koloxo;
    Session_class session_class;
    TextView mydash_l1_username,mydash_l1_address;


    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("My Dashboard");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
     //listener.onFragSelected("Dashboard One");
              View view=  inflater.inflate(R.layout.frag_my_dashboardmain, container, false);
        session_class=new Session_class(getActivity());
              Koloxo = Typeface.createFromAsset(getActivity().getAssets(), "fonts/OpenSans-Regular.ttf");
                handshake=(ImageView)view.findViewById(R.id.mydash_lll3_icn);
        mydash_l1_username=(TextView) view.findViewById(R.id.mydash_l1_username);
        mydash_l1_address=(TextView) view.findViewById(R.id.mydash_l1_address);


               // handshake.setColorFilter(Color.BLACK);
       /* handshake.getDrawable().setColorFilter(Color.BLACK, PorterDuff.Mode.MULTIPLY );*/
              ///card1 set
              card_myproperty=(CardView)view.findViewById(R.id.mydash_l2_card1);
              card_recvd_rqst=(CardView)view.findViewById(R.id.mydash_l2_card2);
              card_rqst_B_S=(CardView)view.findViewById(R.id.mydash_l2_card3);

              card1_count=(TextView)view.findViewById(R.id.mydash_l4_cunt);
              card2_count=(TextView)view.findViewById(R.id.mydash_ll4_cunt);
              card3_count=(TextView)view.findViewById(R.id.mydash_lll4_cunt);

              card1_add_prprty_txt=(TextView)view.findViewById(R.id.mydash_l4_cntent);
              card2_add_prprty_txt=(TextView)view.findViewById(R.id.mydash_ll4_cntent);
              card3_add_prprty_txt=(TextView)view.findViewById(R.id.mydash_lll4_cntent);

              card1_icn=(ImageView)view.findViewById(R.id.mydash_l3_icn) ;
              card2_icn=(ImageView)view.findViewById(R.id.mydash_ll3_icn) ;
              card3_icn=(ImageView)view.findViewById(R.id.mydash_lll3_icn) ;





              card_myproperty.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      card_myproperty.setCardBackgroundColor(Color.parseColor("#5756D6"));
                      card1_count.setTextColor(Color.WHITE);
                      card1_add_prprty_txt.setTextColor(Color.WHITE);
                      card1_icn.setImageResource(R.drawable.ic_my_properties1);
                      /*card1_icn.setImageResource(resId);*/
                      //
                      card_recvd_rqst.setCardBackgroundColor(Color.parseColor("#ffffff"));
                      card2_count.setTextColor(Color.BLACK);
                      card2_add_prprty_txt.setTextColor(Color.BLACK);

                      card_rqst_B_S.setCardBackgroundColor(Color.parseColor("#ffffff"));
                      card3_count.setTextColor(Color.BLACK);
                      card3_add_prprty_txt.setTextColor(Color.BLACK);
                     /* Intent intent =new Intent(getContext(), Added_properties.class);
                      startActivity(intent);*/


                      Title.setText("Added Properties");
                      Title.setTypeface(Koloxo);
                      getHpBaseActivity().pushFragments(new Added_properties(),true,true);

                  }
              });
        card_recvd_rqst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                card_recvd_rqst.setCardBackgroundColor(Color.parseColor("#5756D6"));
                card2_count.setTextColor(Color.WHITE);
                card2_add_prprty_txt.setTextColor(Color.WHITE);

                card2_icn.setImageResource(R.drawable.ic_request1);
                      /*card1_icn.setImageResource(resId);*/
                      //
                card_myproperty.setCardBackgroundColor(Color.parseColor("#ffffff"));
                card1_count.setTextColor(Color.BLACK);
                card1_add_prprty_txt.setTextColor(Color.BLACK);

                card_rqst_B_S.setCardBackgroundColor(Color.parseColor("#ffffff"));
                card3_count.setTextColor(Color.BLACK);
                card3_add_prprty_txt.setTextColor(Color.BLACK);
               /* Intent intent =new Intent(Activity, Request_list.class);
                startActivity(intent);*/
                Title.setText("Request List");
                Title.setTypeface(Koloxo);
                getHpBaseActivity().pushFragments(new Request_list(),true,true);
            }
        });
        card_rqst_B_S.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                card_rqst_B_S.setCardBackgroundColor(Color.parseColor("#5756D6"));
                card3_count.setTextColor(Color.WHITE);
                card3_add_prprty_txt.setTextColor(Color.WHITE);
                card3_icn.setImageResource(R.drawable.ic_handshakewhite);
              //  handshake.setColorFilter(Color.WHITE);
              //  handshake.getDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY );

                      /*card1_icn.setImageResource(resId);*/
                      //
                card_myproperty.setCardBackgroundColor(Color.parseColor("#ffffff"));
                card1_count.setTextColor(Color.BLACK);
                card1_add_prprty_txt.setTextColor(Color.BLACK);

                card_recvd_rqst.setCardBackgroundColor(Color.parseColor("#ffffff"));
                card2_count.setTextColor(Color.BLACK);
                card2_add_prprty_txt.setTextColor(Color.BLACK);

               /* Intent intent =new Intent(Activity, MyRequest_B_s.class);
                startActivity(intent);*/

                Title.setText("My Request");
                Title.setTypeface(Koloxo);
                getHpBaseActivity().pushFragments(new MyRequest_B_s(),true,true);
            }

        });


        ////

        final Call<JsonObject> dasboardcall = new Retrofit_Helper().getRetrofitBuilder().getDasboard("38");


        dasboardcall.enqueue(new ResponseHandler(true, getActivity(), new ResponseCallback() {
            @Override
            public void getResponse(int code, JsonObject jsonObject) {
                Log.v("Dashboard", jsonObject.toString());
                PojoDasboard pojoDasboard = new GsonBuilder().create().fromJson(jsonObject, PojoDasboard.class);
                if (pojoDasboard.getErrorCode().equals("0")) {
                    mydash_l1_username.setText(pojoDasboard.getData().getFirstName());
                    mydash_l1_address.setText(pojoDasboard.getData().getCountryResidence());
                    card1_count.setText(String.valueOf(pojoDasboard.getData().getMypropertyCount()));
                   card2_count.setText(String.valueOf(pojoDasboard.getData().getReceivedRequestCount()));
                     card3_count.setText(String.valueOf(pojoDasboard.getData().getMyRequestBuyRentCount()));
                    // session_class.createUserRegisterSession(loginData.getData().getUserId(), loginData.getData().getFirstName(), loginData.getData().getLastName(), loginData.getData().getMobileNumber(), loginData.getData().getEmail(), loginData.getData().getImage(), MyConstants.LoggedIn.TRUE,loginData.getData().getImage());
                    Log.d("dash", "onResponse: message " + pojoDasboard.getData().getMypropertyCount());


                }
                else if (pojoDasboard.getErrorCode().equals("1"))
                {
                    Dialoghelper.showSnackbar(getActivity(), pojoDasboard.getMessage());
                }
                // session_class.createUserLoginSession(response.body().getData().getUserId());
            }


            @Override
            public void getError(Call<JsonObject> call, String message) {
                Dialoghelper.showSnackbar(getActivity(), message);
            }
        }, dasboardcall));

   return view;
    }


}
