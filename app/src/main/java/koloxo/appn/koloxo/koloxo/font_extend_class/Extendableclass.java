package koloxo.appn.koloxo.koloxo.font_extend_class;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;

import koloxo.appn.koloxo.koloxo.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class Extendableclass extends Application {
  /*  public static Typeface main_typeface_light;
    public static Typeface main_typeface_bold;
    public static Typeface main_typeface_regular;
    FragmentActivity activity;
    Context context;*/
    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(
                new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Montserrat-Regular.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build());
    }}
    /*public void fontstyles(FragmentActivity activity)

    {
        this.activity=activity;
        main_typeface_light= Typeface.createFromAsset(activity.getAssets(), "fonts/FiraSans-Light.otf");
        main_typeface_bold= Typeface.createFromAsset(activity.getAssets(), "fonts/Montserrat-Bold.ttf");
        main_typeface_regular= Typeface.createFromAsset(activity.getAssets(), "fonts/Montserrat-Regular.ttf");

    }
    public void fontstyles(Context context)
    {
        this.context=context;
        main_typeface_light= Typeface.createFromAsset(context.getAssets(), "fonts/FiraSans-Light.otf");
        main_typeface_bold= Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Bold.ttf");
        main_typeface_regular= Typeface.createFromAsset(context.getAssets(), "fonts/Montserrat-Regular.ttf");
    }*/

