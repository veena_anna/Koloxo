package koloxo.appn.koloxo.koloxo.adapter;

import android.content.Context;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.R;
import me.relex.circleindicator.CircleIndicator;

/**
 * Created by appzoc-php on 23/3/18.
 */

public class Furnitr_of_prprtydetail_adapt extends RecyclerView.Adapter<Furnitr_of_prprtydetail_adapt.ViewHolder> {

        Context context;
        List<Pojo_postprpty_part3> mypojos;
        ArrayList<Integer> array_image;
    News_feed_adaptr circleIndicator_adapter;

private int currentPage = 0;
private int NUM_PAGES = 0;
    //ArrayList<Integer> array_image_news;
    ArrayList<String> array_txt_newshead;
    ArrayList<String> array_txt_news_sub;

    Timer swipeTimer;

public Furnitr_of_prprtydetail_adapt(Context context, List<Pojo_postprpty_part3> mypojos) {
        this.context = context;
        this.mypojos = mypojos;

        }

@Override
public Furnitr_of_prprtydetail_adapt.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.rec_pro_child_1, parent, false);
    array_txt_newshead=new ArrayList<>();
    array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
    array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
    array_txt_newshead.add("KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
    array_txt_news_sub=new ArrayList<>();
    array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
    array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
    array_txt_news_sub.add("The news of the day give informatiom about wht all this going on in our state.The news of the day give informatiom about wht all this going on in our state");
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(Furnitr_of_prprtydetail_adapt.ViewHolder holder, int position) {
        Pojo_postprpty_part3 mypojo = mypojos.get(position);
        holder.header.setText(mypojo.getName());

        if (mypojos.get(position).getSelected() == 0) {
        holder.cardItem.setVisibility(View.GONE);
        holder.imgbtn.setImageResource(R.drawable.ic_sort_down);
        } else {
        holder.cardItem.setVisibility(View.VISIBLE);
        holder.imgbtn.setImageResource(R.drawable.ic_caret_arrow_up);


        }

        array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.splash_pic_2);
        array_image.add(R.drawable.splash_pic_1);
        array_image.add(R.drawable.splash_pic_2);

        initImageSlider(holder);

        }

    private void initImageSlider(final ViewHolder holder) {
        final ViewHolder myHolder = holder;
        circleIndicator_adapter = new News_feed_adaptr(array_txt_newshead,array_txt_news_sub,context);
        myHolder.viewPager.setAdapter(circleIndicator_adapter);
        myHolder.viewPager.setAdapter(circleIndicator_adapter);
        myHolder.indicator.setViewPager(myHolder.viewPager);
        myHolder.viewPager.setCurrentItem(0);

        circleIndicator_adapter.registerDataSetObserver(myHolder.indicator.getDataSetObserver());
        NUM_PAGES = array_txt_newshead.size();
        if (NUM_PAGES > 1) {
            myHolder.indicator.setVisibility(View.VISIBLE);
        } else {
            myHolder.indicator.setVisibility(View.GONE);
        }


        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;


                }
                myHolder.viewPager.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 0, 2000);

        // Pager listener over indicator
        myHolder.indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });


    }


@Override
public int getItemCount() {
        return mypojos.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder {

    LinearLayout cardItem;
    AppCompatImageButton imgbtn;
    TextView header;
    ViewPager viewPager;
    CircleIndicator indicator;

    public ViewHolder(View itemView) {
        super(itemView);

        imgbtn = (AppCompatImageButton) itemView.findViewById(R.id.imgbtn);
        cardItem = (LinearLayout) itemView.findViewById(R.id.property_img_child);
        header = (TextView) itemView.findViewById(R.id.header);
        viewPager = (ViewPager) itemView.findViewById(R.id.viewpagerProperty);
        indicator = (CircleIndicator) itemView.findViewById(R.id.indicator);


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (mypojos.get(getAdapterPosition()).getSelected() == 0) {


                    for (int i = 0; i < mypojos.size(); i++
                            ) {

                        mypojos.get(i).setSelected(0);

                    }

                    mypojos.get(getAdapterPosition()).setSelected(1);

                    notifyItemRangeChanged(0, mypojos.size());
                } else {

                    for (int i = 0; i < mypojos.size(); i++
                            ) {

                        mypojos.get(i).setSelected(0);

                    }


                    notifyItemRangeChanged(0, mypojos.size());

                }
            }

        });


        imgbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (mypojos.get(getAdapterPosition()).getSelected() == 0) {


                    for (int i = 0; i < mypojos.size(); i++
                            ) {

                        mypojos.get(i).setSelected(0);

                    }

                    mypojos.get(getAdapterPosition()).setSelected(1);

                    notifyItemRangeChanged(0, mypojos.size());
                } else {

                    for (int i = 0; i < mypojos.size(); i++
                            ) {

                        mypojos.get(i).setSelected(0);

                    }

                    notifyItemRangeChanged(0, mypojos.size());
                }
            }

        });

    }

}
    public class News_feed_adaptr extends PagerAdapter {
        ArrayList<Integer> sliderPojos;
        ArrayList<String> slidetxt_hed;
        ArrayList<String> slidetxt;

        private int mSize;
        Context context;


        public News_feed_adaptr(ArrayList<String> array_txt_newshead, ArrayList<String> array_txt_news_sub, Context context) {

            this.slidetxt = array_txt_news_sub;
            this.slidetxt_hed = array_txt_newshead;
this.context=context;
            mSize = slidetxt_hed.size();
        }


        @Override
        public int getCount() {
            return mSize;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup view, int position, Object object) {
            view.removeView((View) object);
        }

        @Override
        public Object instantiateItem(ViewGroup view, final int position) {

            View itemView = LayoutInflater.from(context).inflate(R.layout.property_detail_indi, view, false);
            // ImageView imageView = (ImageView) itemView.findViewById(R.id.splash_image);
            TextView textView_head = (TextView) itemView.findViewById(R.id.newshed);
            TextView textView_sub = (TextView) itemView.findViewById(R.id.news_sub_id);

            //  imageView.setImageResource(sliderPojos.get(position));
            textView_head.setText(slidetxt_hed.get(position));
            textView_sub.setText(slidetxt.get(position));

            view.addView(itemView);

            return itemView;
        }


    }

    }

