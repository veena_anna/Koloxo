package koloxo.appn.koloxo.koloxo.activity;

import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sembozdemir.viewpagerarrowindicator.library.ViewPagerArrowIndicator;

import java.util.ArrayList;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.CircleIndicator_Adapter;

public class ImageSliderPopup extends Activity {
    private ViewPager viewPager;
    private ViewPagerArrowIndicator viewPagerArrowIndicator;
    ArrayList<Integer> array_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_slider_popup);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPagerArrowIndicator = (ViewPagerArrowIndicator) findViewById(R.id.viewPagerArrowIndicator);
        array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.splash_pic_2);
        array_image.add(R.drawable.splash_pic_1);
        array_image.add(R.drawable.splash_pic_1);
        viewPagerArrowIndicator.setArrowIndicatorRes(R.drawable.ic_left_arrow, R.drawable.ic_arrow_point_to_right);
        viewPager.setAdapter(new CircleIndicator_Adapter(array_image, ImageSliderPopup.this));
        viewPagerArrowIndicator.bind(viewPager);
    }
}
