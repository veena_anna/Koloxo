package koloxo.appn.koloxo.koloxo.Objects;

/**
 * Created by appzoc-php on 5/3/18.
 */

public class Side_mysearchobj {
    private int imageId;
    private String title;
    private String desc;

    public Side_mysearchobj(int imageId, String title, String desc) {
        this.imageId = imageId;
        this.title = title;
        this.desc = desc;
    }

    public Side_mysearchobj(Integer image) {
        this.imageId=image;
    }

    public Side_mysearchobj(Integer image, String title) {
        this.imageId=image;
        this.title=title;
    }

    public int getImageId() {
        return imageId;
    }
    public void setImageId(int imageId) {
        this.imageId = imageId;
    }
    public String getDesc() {
        return desc;
    }
    public void setDesc(String desc) {
        this.desc = desc;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    @Override
    public String toString() {
        return title + "\n" + desc;
    }
}