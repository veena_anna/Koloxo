package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 27/3/18.
 */

public class proprty_img_detl_adapter extends PagerAdapter {
    ArrayList<Integer> sliderPojos;

    private int mSize;
    Context context;
    Activity activity;

    public proprty_img_detl_adapter(ArrayList<Integer> sliderPojos, Context context) {
        this.sliderPojos = sliderPojos;
        this.context = context;
        mSize = sliderPojos.size();

    }



    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.viewpager_indicator_new, view, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.sliderImage);
        imageView.setImageResource(sliderPojos.get(position));
        view.addView(itemView);

        return itemView;
    }
}


