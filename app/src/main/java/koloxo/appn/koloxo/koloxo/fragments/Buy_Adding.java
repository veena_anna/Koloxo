package koloxo.appn.koloxo.koloxo.fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import java.util.List;
import java.util.UUID;

import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.Webserives.CountryData;
import koloxo.appn.koloxo.koloxo.Webserives.PojoCountryName;
import koloxo.appn.koloxo.koloxo.Webserives.Pojoadd1_buy;
import koloxo.appn.koloxo.koloxo.Webserives.PojoitemReg;
import koloxo.appn.koloxo.koloxo.activity.AddingPrpty_part2;
import koloxo.appn.koloxo.koloxo.activity.FilePath;
import koloxo.appn.koloxo.koloxo.activity.Otp_Screenpage;
import koloxo.appn.koloxo.koloxo.activity.RegisterPage;
import koloxo.appn.koloxo.koloxo.adapter.CountryCurrencyAdapter;
import koloxo.appn.koloxo.koloxo.adapter.CountrylistAdapter;
import koloxo.appn.koloxo.koloxo.adapter.Spinner_Adapter;
import retrofit2.Call;

import static android.app.Activity.RESULT_OK;
import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;


public class Buy_Adding extends Fragment {
    ImageView add_prpty_nxt;
    Spinner Price_spinner, PRprtytype_spinr, Prprty_age_spinner, Prprty_elevator_spinr, Prprty_buildngno;
    String[] List_prpty_type, List_age, List_elevators, List_buildngno;
    ImageView buy_upload_cv, buy_title_deed;
    EditText prpty_title, building_name_txt, Requrd_deposit;

    String selectedText_prpty, selectedvalue_prpty;
    String selectedText_amount, selectedvalue_amount;
    String selectedText_age, selectedvalue_age;
    String selectedText_elevator, selectedvalue_elevator;
    String selectedText_buildno, selectedvalue_buildno;
    // Pdf upload request code.
    public int PDF_REQ_CODE_1 = 1;
    public int PDF_REQ_CODE_2 = 2;
    // Creating URI .
    Uri uri_1, uri_2;
    TextView upload_cv_txt, title_deed_txt;
    // Define strings to hold given pdf name, path and ID.
    String PdfNameHolder, PdfPathHolder_1, PdfPathHolder_2, PdfID;
    String prpty_title1, building_name_txt1, Requrd_deposit1;
    Session_class session_class;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view1 = inflater.inflate(R.layout.fragment_buy__adding, container, false);
        //  initializevar();
        RequestRunTimePermission();
        session_class=new Session_class(getActivity());
        buy_upload_cv = (ImageView) view1.findViewById(R.id.buy_upload_cv);
        buy_title_deed = (ImageView) view1.findViewById(R.id.buy_title_deed);
        upload_cv_txt = (TextView) view1.findViewById(R.id.upload_cv_txt);
        title_deed_txt = (TextView) view1.findViewById(R.id.title_deed_txt);

        prpty_title = (EditText) view1.findViewById(R.id.prpty_title);
        building_name_txt = (EditText) view1.findViewById(R.id.building_name_txt);
        Requrd_deposit = (EditText) view1.findViewById(R.id.Requrd_deposit);

        List_prpty_type = new String[]{"select", "apartment", "houses"};
       // List_amount = new String[]{"₹", "2000", "1000"};
        List_age = new String[]{"select", "6 month", "8 month"};
        List_elevators = new String[]{"select", "1", "2", "3", "4"};
        List_buildngno = new String[]{"select", "101", "202", "304", "425"};

        add_prpty_nxt = (ImageView) view1.findViewById(R.id.add_prpty_nxt1);
        Price_spinner = (Spinner) view1.findViewById(R.id.Price_spinner);




        getCurrency();






      //  Price_spinner.setAdapter(new Spinner_Adapter(getActivity(), List_amount));
        Price_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                selectedText_amount = List_amount[i];
//                if (!selectedText_amount.equals("₹")) {
//                    selectedvalue_amount = selectedText_amount;
//                    //  Toast.makeText(getApplicationContext(), selected_country, Toast.LENGTH_SHORT).show();
//                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        PRprtytype_spinr = (Spinner) view1.findViewById(R.id.prpty_spinnr_type);

        PRprtytype_spinr.setAdapter(new Spinner_Adapter(getActivity(), List_prpty_type));
        PRprtytype_spinr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedText_prpty = List_prpty_type[i];
                if (!selectedText_prpty.equals("select")) {
                    selectedvalue_prpty = selectedText_prpty;
                    //  Toast.makeText(getApplicationContext(), selected_country, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Prprty_age_spinner = (Spinner) view1.findViewById(R.id.prpty_spinnr_age);

        Prprty_age_spinner.setAdapter(new Spinner_Adapter(getActivity(), List_age));
        Prprty_age_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedText_age = List_age[i];
                if (!selectedText_age.equals("select")) {
                    selectedvalue_age = selectedText_age;
                    //  Toast.makeText(getApplicationContext(), selected_country, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        Prprty_elevator_spinr = (Spinner) view1.findViewById(R.id.prpty_spinnr_elevator);


        Prprty_elevator_spinr.setAdapter(new Spinner_Adapter(getActivity(), List_elevators));
        Prprty_elevator_spinr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedText_elevator = List_elevators[i];
                if (!selectedText_elevator.equals("select")) {
                    selectedvalue_elevator = selectedText_elevator;
                    //  Toast.makeText(getApplicationContext(), selected_country, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        Prprty_buildngno = (Spinner) view1.findViewById(R.id.prpty_spinnr_buildno);


        Prprty_buildngno.setAdapter(new Spinner_Adapter(getActivity(), List_buildngno));
        Prprty_buildngno.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedText_buildno = List_buildngno[i];
                if (!selectedText_buildno.equals("select")) {
                    selectedvalue_buildno = selectedText_buildno;
                    //  Toast.makeText(getApplicationContext(), selected_country, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        buy_upload_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Creating intent object.
                Intent intent = new Intent();

                // Setting up default file pickup time as PDF.
                intent.setType("application/pdf");

                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE_1);
              /*  // After selecting the PDF set PDF is Selected text inside Button.
                upload_cv_txt.setText("PDF is Selected");*/

            }
        });
        buy_title_deed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Creating intent object.
                Intent intent = new Intent();

                // Setting up default file pickup time as PDF.
                intent.setType("application/pdf");

                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Pdf"), PDF_REQ_CODE_2);
                /*// After selecting the PDF set PDF is Selected text inside Button.
                title_deed_txt.setText("PDF is Selected");*/

            }
        });


        add_prpty_nxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent=new Intent(getActivity(), AddingPrpty_part2.class);
                startActivity(intent);*/
                // Getting file path using Filepath class.



          /*      if (PdfPathHolder_1 == null || PdfPathHolder_2 == null) {
                    Dialoghelper.showSnackbar(getActivity(), "Please Upload Proper Document");
                } else if (PdfPathHolder_1 == null) {
                    Dialoghelper.showSnackbar(getActivity(), "Please Upload Another CV");
                } else if (PdfPathHolder_2 == null) {
                    Dialoghelper.showSnackbar(getActivity(), "Please Attach Another Title Deed File");
                } else if ((PdfPathHolder_1 == null) && (PdfPathHolder_2 == null)) {
                    Dialoghelper.showSnackbar(getActivity(), "Please Upload Another CV");
                    Dialoghelper.showSnackbar(getActivity(), "Please Attach Another Title Deed File");
                } else {
                    Toast.makeText(getActivity(), PdfPathHolder_1 + "\n next  : " + PdfPathHolder_2, Toast.LENGTH_SHORT).show();
                }*/


                if (uri_1!=null) {
                    if (uri_2!=null) {

                        PdfPathHolder_1 = FilePath.getPath(getActivity(), uri_1);
                        PdfPathHolder_2 = FilePath.getPath(getActivity(), uri_2);
                    }

                    //else {
                    //  Dialoghelper.showSnackbar(getActivity(), "Please complete the section");
                    //}

                }

                //else {
                //   Dialoghelper.showSnackbar(getActivity(), "Please complete the section");
                // }

                if (!(prpty_title.getText().length() == 0) && !(building_name_txt.getText().length() == 0) && !(Requrd_deposit.getText().length() == 0)) {



                    Log.d("XXX_pdf", "onClick:  path1 : " + PdfPathHolder_1 + "   uri1 : " + uri_1);
                    Log.d("XXX_pdf", "onClick:  path2 : " + PdfPathHolder_2 + "   uri2 : " + uri_2);

                    prpty_title1 = prpty_title.getText().toString();
                    building_name_txt1 = building_name_txt.getText().toString();
                    Requrd_deposit1 = Requrd_deposit.getText().toString();

                    String s = prpty_title1 + building_name_txt1 + Requrd_deposit1;

                    Log.d("spin", "onResponse: message " + s);


                    final Call<JsonObject> add_buy1_call = new Retrofit_Helper().getRetrofitBuilder().getAddPrptyBuy1(session_class.getId(), selectedvalue_prpty, prpty_title1, selectedvalue_age, selectedvalue_elevator, selectedvalue_buildno, selectedvalue_amount, Requrd_deposit1, "0", building_name_txt1, "buy");
                    add_buy1_call.enqueue(new ResponseHandler(true, getActivity(), new ResponseCallback() {
                        @Override
                        public void getResponse(int code, JsonObject jsonObject) {
                            Pojoadd1_buy pojo_data_buy = new GsonBuilder().create().fromJson(jsonObject, Pojoadd1_buy.class);
                            Log.d("addbuy", "ch: error code (3) " + pojo_data_buy.getMessage());
                            if (pojo_data_buy.getErrorCode().equals("0")) {


                                Title.setText("Floor Plan");
                                getHpBaseActivity().pushFragments(new AddingPrpty_part2(), true, true);


                                Log.d("Add_buty", "ch: error code (0) " + pojo_data_buy.getMessage() + " message : " + pojo_data_buy.getMessage());
                            } else {
                                Log.d("Add_buty", "ch: error code (1)  " + pojo_data_buy.getMessage() + " message : " + pojo_data_buy.getMessage());

                                // Toast.makeText(getActivity(), pojo_data_buy.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void getError(Call<JsonObject> call, String message) {
                            Dialoghelper.showSnackbar(getActivity(), message);
                        }
                    }, add_buy1_call));


                } else {
                    if (prpty_title.getText().length() == 0)
                        prpty_title.setError("First name required!");
                    if (building_name_txt.getText().length() == 0)
                        building_name_txt.setError("Last name required!");
                    if (Requrd_deposit.getText().length() == 0)
                        Requrd_deposit.setError("Mobileno. required!");
                    if (selectedText_amount.equals("₹"))
                        Dialoghelper.showSnackbar(getActivity(), "Select Amount");
                    if (selectedText_prpty.equals("select"))
                        Dialoghelper.showSnackbar(getActivity(), "Select Property Type");
                    if (selectedText_age.equals("select"))
                        Dialoghelper.showSnackbar(getActivity(), "Select Age of Building");
                    if (selectedText_elevator.equals("select"))
                        Dialoghelper.showSnackbar(getActivity(), "Select Elevator");
                    if (selectedText_buildno.equals("select"))
                        Dialoghelper.showSnackbar(getActivity(), "Select Building Number");
                }


                // PdfID = UUID.randomUUID().toString();


            }
        });


        getPostPropertydetails();
        return view1;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PDF_REQ_CODE_1 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            uri_1 = data.getData();
            if (uri_1.equals(null)) {
                Dialoghelper.showSnackbar(getActivity(), "Please Upload Another CV");
            }

            Log.d("XXX_pdf", "onActivityResult: uri_1 :" + uri_1);

            Dialoghelper.showSnackbar(getActivity(), "PDF is Selected");
        }
        if (requestCode == PDF_REQ_CODE_2 && resultCode == RESULT_OK && data != null && data.getData() != null) {

            uri_2 = data.getData();
            if (uri_2.equals(null)) {
                Dialoghelper.showSnackbar(getActivity(), "Please Attach Another Title Deed File");
            }


            Log.d("XXX_pdf", "onActivityResult: uri_2 :" + uri_2);

            Dialoghelper.showSnackbar(getActivity(), "PDF is Selected");
        }
    }

    // Requesting run time permission method starts from here.
    public void RequestRunTimePermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {

            Toast.makeText(getActivity(), "READ_EXTERNAL_STORAGE permission Access Dialog", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        }
    }

    @Override
    public void onRequestPermissionsResult(int RC, String per[], int[] Result) {

        switch (RC) {

            case 1:

                if (Result.length > 0 && Result[0] == PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(getActivity(), "Permission Granted", Toast.LENGTH_LONG).show();

                } else {

                    Toast.makeText(getActivity(), "Permission Canceled", Toast.LENGTH_LONG).show();

                }
                break;
        }
    }




    public void getCurrency()
    {
        final Call<JsonObject> countrycall = new Retrofit_Helper().getRetrofitBuilder().getCountry();

        countrycall.enqueue(new ResponseHandler(true, getActivity(), new ResponseCallback() {
            @Override
            public void getResponse(int code, JsonObject jsonObject) {
                Log.v("LoginActivity", jsonObject.toString());
                PojoCountryName countryName = new GsonBuilder().create().fromJson(jsonObject, PojoCountryName.class);
                if (countryName.getErrorCode().equals("0")) {
                    if(countryName.getData().size()>0) {

                        List<CountryData> countryData = countryName.getData();

                       // countryData.add(0,new CountryData("0","Select the country",""));

                        CountryCurrencyAdapter countrylistAdapter = new CountryCurrencyAdapter(getActivity(), countryData);

                        Price_spinner.setAdapter(countrylistAdapter);

                    }
                    else {


                        Dialoghelper.showSnackbar(getActivity(), "No data found");

                    }


                    // brewarray = new String[countryName.getData().size()];
//                        for (int i = 0; i < countryName.getData().size(); i++) {
//                            String data = countryName.getData().get(i).getCountry();
//                            brewarray[i] = "\"" + data + "\"";
//
//                        }}
//
//                    // Apply the adapter to the spinner
//                    staticSpinner.setAdapter(new Spinner_Adapter(getActivity(),brewarray));
                    // session_class.createUserRegisterSession(loginData.getData().getUserId(), loginData.getData().getFirstName(), loginData.getData().getLastName(), loginData.getData().getMobileNumber(), loginData.getData().getEmail(), loginData.getData().getImage(), MyConstants.LoggedIn.TRUE,loginData.getData().getImage());
                    Log.d("Koloxo_login", "onResponse: message " + countryName.getMessage());
                    // Toast.makeText(LoginPage.this, "Login Success", Toast.LENGTH_SHORT).show();

                }
                else if (countryName.getErrorCode().equals("1"))
                {
                    Dialoghelper.showSnackbar(getActivity(), countryName.getMessage());
                }
                // session_class.createUserLoginSession(response.body().getData().getUserId());
            }


            @Override
            public void getError(Call<JsonObject> call, String message) {
                Dialoghelper.showSnackbar(getActivity(), message);
            }
        }, countrycall));
    }




    public void getPostPropertydetails()
    {
        final Call<JsonObject> countrycall = new Retrofit_Helper().getRetrofitBuilder().getPostpropertydetails();

        countrycall.enqueue(new ResponseHandler(true, getActivity(), new ResponseCallback() {
            @Override
            public void getResponse(int code, JsonObject jsonObject) {

                Log.e("TAG",jsonObject.toString());



            }

            @Override
            public void getError(Call<JsonObject> call, String message) {

            }
        },countrycall));

    }

}