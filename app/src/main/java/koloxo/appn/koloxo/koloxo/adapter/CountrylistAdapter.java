package koloxo.appn.koloxo.koloxo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Webserives.CountryData;

/**
 * Created by appzoc-php on 20/4/18.
 */

public class CountrylistAdapter extends BaseAdapter {


    Context context;
    List<CountryData>countryData;

    public CountrylistAdapter(Context context, List<CountryData> countryData) {
        this.context = context;
        this.countryData = countryData;
    }

    @Override
    public int getCount() {
        return countryData.size();
    }

    @Override
    public Object getItem(int i) {
        return countryData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflter=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = inflter.inflate(R.layout.custom_spinner_items, viewGroup,false);

        TextView names = (TextView) view.findViewById(R.id.textView);
        String str = countryData.get(i).getCountry();
        String str1 = str.replace("\"", "");
       // names.setTextColor(Color.BLACK);
        //names.setTextSize(14);
        names.setText(str1);


        if(i!=0)
        {
            LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,80);


            names.setLayoutParams(layoutParams);
        }


        return view;
    }
}
