package koloxo.appn.koloxo.koloxo;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by appzoc-php on 8/3/18.
 */
public class BaseFragment extends Fragment {
    private static final String TAG = "ExpoBaseFragment";
    public BaseActivity Activity;
    public static  int flagURL=0;
    public static  int flagURLOnFilter=0;
    public static String spinnerSelectedId = "";
    public static int spinnerSelectedPosition = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        Activity = (BaseActivity) this.getActivity();
    }
    public void onResume() {
        Log.d(TAG, "onResume" + this.getClass().getName());
        super.onResume();
        ((BaseActivity) getActivity()).BaseFragment = this;

    }
}