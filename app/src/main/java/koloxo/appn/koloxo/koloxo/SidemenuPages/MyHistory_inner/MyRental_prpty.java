package koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.MyHistory_innr.MyRental_prpty_myhis;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;


public class MyRental_prpty extends Fragment implements AdapterView.OnItemClickListener {
        ListView myRental_prptylist;
        MyRental_prpty request_list_mydash;
public static final String[] titles = new String[] { "Strawberry",
        "Banana", "Orange", "Mixed" };

public static final String[] descriptions = new String[] {
        "It is an aggregate accessory fruit",
        "It is the largest herbaceous flowering plant", "Citrus Fruit",
        "Mixed Fruits" };
public static final Integer[] images = { R.drawable.img_prpty,
        R.drawable.img_prpty, R.drawable.img_prpty, R.drawable.img_prpty };
        List<Side_mysearchobj> rowItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_rental_prpty, container, false);
        myRental_prptylist = (ListView) view.findViewById(R.id.list_myrental_myhis);
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);

        }


        MyRental_prpty_myhis adapter = new MyRental_prpty_myhis(getActivity(), rowItems);
        myRental_prptylist.setAdapter(adapter);
        myRental_prptylist.setOnItemClickListener(MyRental_prpty.this);

        //back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

return view;
    }
@Override
public void onItemClick(AdapterView<?> parent, View view, int position,
        long id) {
       /* Toast toast = Toast.makeText(getActivity(),
        "Item " + (position + 1) + ": " + rowItems.get(position),
        Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();*/
        }
        }
