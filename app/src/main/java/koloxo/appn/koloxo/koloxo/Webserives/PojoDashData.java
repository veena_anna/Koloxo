package koloxo.appn.koloxo.koloxo.Webserives;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by appzoc-php on 17/4/18.
 */

public class PojoDashData {
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("country_residence")
    @Expose
    private String countryResidence;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("mobile_number")
    @Expose
    private String mobileNumber;
    @SerializedName("myproperty_count")
    @Expose
    private Integer mypropertyCount;
    @SerializedName("receivedRequest_count")
    @Expose
    private Integer receivedRequestCount;
    @SerializedName("myRequestBuyRent_count")
    @Expose
    private Integer myRequestBuyRentCount;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountryResidence() {
        return countryResidence;
    }

    public void setCountryResidence(String countryResidence) {
        this.countryResidence = countryResidence;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Integer getMypropertyCount() {
        return mypropertyCount;
    }

    public void setMypropertyCount(Integer mypropertyCount) {
        this.mypropertyCount = mypropertyCount;
    }

    public Integer getReceivedRequestCount() {
        return receivedRequestCount;
    }

    public void setReceivedRequestCount(Integer receivedRequestCount) {
        this.receivedRequestCount = receivedRequestCount;
    }

    public Integer getMyRequestBuyRentCount() {
        return myRequestBuyRentCount;
    }

    public void setMyRequestBuyRentCount(Integer myRequestBuyRentCount) {
        this.myRequestBuyRentCount = myRequestBuyRentCount;
    }

}
