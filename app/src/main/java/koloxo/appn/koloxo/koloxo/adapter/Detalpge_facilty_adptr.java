package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 16/3/18.
 */

public class Detalpge_facilty_adptr extends BaseAdapter {
    Context context;
    List<Side_mysearchobj> rowItems;
boolean status;
    public Detalpge_facilty_adptr(Context context, List<Side_mysearchobj> items, boolean status_facil) {
        this.context = context;
        this.rowItems = items;
        this.status=status_facil;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;

    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Detalpge_facilty_adptr.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.row_detalpge_facilities, parent,false);
            holder = new Detalpge_facilty_adptr.ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//           holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
           holder.imageView = (ImageView) convertView.findViewById(R.id.ic_prpty_facil_img);
            convertView.setTag(holder);
        }
        else {
            holder = (Detalpge_facilty_adptr.ViewHolder) convertView.getTag();
        }
if(status==true) {
            for(int i=position;i<3;i++){
    Side_mysearchobj rowItem = (Side_mysearchobj) getItem(i);

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
    holder.imageView.setImageResource(rowItem.getImageId());
}}
else if(status==false){
    for(int i=position;i<8;i++){
        Side_mysearchobj rowItem = (Side_mysearchobj) getItem(i);

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        holder.imageView.setImageResource(rowItem.getImageId());
    }
}
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}
