package koloxo.appn.koloxo.koloxo.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.MapsActivity_detalprprty;
import koloxo.appn.koloxo.koloxo.adapter.Spinner_Adapter;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Koloxo;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;


public class RentFrag extends Fragment {
    Spinner staticSpinner, For_Month;
    TextView start_date;
    ImageView rent_nxt2;
    DatePickerDialog.OnDateSetListener listener1, listener2;

    public RentFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rent, container, false);
        staticSpinner = view.findViewById(R.id.static_spinner);
        rent_nxt2=(ImageView)view.findViewById(R.id.rent_nxt2);
        rent_nxt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Search Results");
                //Title.setTypeface(Koloxo);
              /*  Intent intent=new Intent(getActivity(), MapsActivity_detalprprty.class);
                startActivity(intent);*/
                getHpBaseActivity().pushFragments(new MapsActivity_detalprprty(),true,true);
               // Toast.makeText(getActivity(),"View to map",Toast.LENGTH_SHORT).show();
            }
        });
        For_Month = view.findViewById(R.id.spin_fr_month);
       /* // Create an ArrayAdapter using the string array and a default spinner
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(getActivity(), R.array.brew_array,
                        android.R.layout.simple_spinner_item);
       // staticSpinner.setPrompt("Select the country");
        // Specify the layout to use when the list of choices appears
        staticAdapter
                .setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // Apply the adapter to the spinner
        staticSpinner.setAdapter(staticAdapter);*/

        // Apply the adapter to the spinner
        String[] brewarray = {"Select the country","Dubai","Saudi Arabia","Oman","London"};
        staticSpinner.setAdapter(new Spinner_Adapter(getActivity(),brewarray));

        String[] items = new String[]{"Months","6 months", "5 months", "10 months"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, items);
        //For_Month.setPrompt("Months");
        For_Month.setAdapter(adapter);

        For_Month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.v("item", (String) parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        start_date = view.findViewById(R.id.start_date);
        start_date.setTypeface(Koloxo);
        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
              DatePickerDialog  datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text

                               // String value_month= getMonthName(monthOfYear);
                                start_date.setText(dayOfMonth + "/" + (monthOfYear) + "/" + year);




                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });




        return view;
    }

}