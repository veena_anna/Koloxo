package koloxo.appn.koloxo.koloxo.activity;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;


import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.Fragmentbase;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Faclits_adpt_addprpty;
import koloxo.appn.koloxo.koloxo.adapter.Post_prprty_part2;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class AddingPrpty_part2 extends android.support.v4.app.Fragment  {
    GridView floorplan_list;
    ListView facilities_list;
    ImageView add_prpty_nxt2;

    Faclits_adpt_addprpty faclits_adpt_addprpty;
    public  AddingPrpty_part2 CustomListView = null;
    public ArrayList<ListFacilitiesModel> CustomListViewValuesArr = new ArrayList<ListFacilitiesModel>();


    public static final Integer[] images = { R.drawable.floorplan,
            R.drawable.floorplan, R.drawable.floorplan, R.drawable.floorplan };
    List<Side_mysearchobj> rowItems;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Floor Plan");
    }
   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_prpty_part2);*/
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.activity_adding_prpty_part2, container, false);

        floorplan_list=(GridView)view.findViewById(R.id.flor_pln_grid);
        facilities_list=(ListView)view.findViewById(R.id.list);
        add_prpty_nxt2=(ImageView)view.findViewById(R.id.add_prpty_nxt2);
       bottom_bar.setVisibility(View.INVISIBLE);

        final RelativeLayout activityRootView = (RelativeLayout) view.findViewById(R.id.relativeLayout);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();

            }
        });

        CustomListView = this;

        /******** Take some data in Arraylist ( CustomListViewValuesArr ) ***********/
        setListData();



        Resources res =getResources();
        facilities_list=(ListView)view.findViewById(R.id.list);

        /**************** Create Custom Adapter *********/
        faclits_adpt_addprpty=new Faclits_adpt_addprpty(getActivity(), CustomListViewValuesArr,res);
        facilities_list.setAdapter(faclits_adpt_addprpty);
       faclits_adpt_addprpty.notifyDataSetChanged();

//try {
//    int a[]={0,1,2};
//    JSONArray jsonArray = new JSONArray();
//
//    for (int i=0;i<a.length; i++
//         ) {
//
//        jsonArray.put(i, a[i]);
//    }
//
//
//
//
//
//    jsonArray.toString();
//
//
//}catch (Exception e)
//{
//
//}


        /////////////////////////////////////////////////////////////////////////////////////
        //floorplan_list.setVerticalScrollBarEnabled(false);
        rowItems = new ArrayList<Side_mysearchobj>();

        for (int i = 0; i < images.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i]);
            rowItems.add(item);
        }

        Post_prprty_part2 adapter = new Post_prprty_part2(getActivity(), rowItems);
        floorplan_list.setAdapter(adapter);
        add_prpty_nxt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent intent =new Intent(getActivity(),AddingPrprty_part3.class);
                startActivity(intent);*/
                Title.setText("Furnitures");
                getHpBaseActivity().pushFragments(new AddingPrprty_part3(),true,true);
            }
        });
       // floorplan_list.setOnItemClickListener(AddingPrpty_part2.this);

       /* //back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/
       return view;
    }
    /****** Function to set data in ArrayList *************/
    public void setListData()
    {

        for (int i = 0; i < Facilities.Facilities.length-3;) {
            int length = 0 ;
            final ListFacilitiesModel sched = new ListFacilitiesModel();

            /******* Firstly take data in model object ******/
            sched.setFirstButton(Facilities.Facilities[i],i);
            length = Facilities.Facilities[i].length()+ Facilities.Facilities[i+1].length();

            if (length<=45) {
                sched.setSecondButton(Facilities.Facilities[i+1],i+1);
                length = length + Facilities.Facilities[i].length()+ Facilities.Facilities[i+1].length()+ Facilities.Facilities[i+2].length();

                if (length<=45) {
                    sched.setThirdButton(Facilities.Facilities[i+2],i+2);
                    i=i+3;
                }else {
                    i=i+2;
                    sched.setThirdButton("A",i+2);
                }

            }else {
                i=i+1;
                sched.setSecondButton("A",i+1);
                sched.setThirdButton("A",i+2);
            }


            /******** Take Model Object in ArrayList **********/
            CustomListViewValuesArr.add(sched);
        }

    }

}
