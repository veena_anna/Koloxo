package koloxo.appn.koloxo.koloxo.Session_class;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.ImageView;

public class Session_class {
    // Shared Preferences reference
    static SharedPreferences pref;

    // Editor reference for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "login";
    // for login
    public static final String id = "id";
    public static final String firstname = "firstname";
    public static final String lastname = "lastname";
    public static final String countryresidence = "countryresidence";
    public static final String mobile = "mobile";
    public static final String imag_login = "imag_login";
    public static final String email_reg = "email_reg";
    public static final String logged_in_status = "log_status";
    //for registersuccess
    public static final String user_id = "user_id";
    public static final String otp_val = "otp_val";

    // Constructor
    public Session_class(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createUserRegisterSession(String id1, String firstname1, String lastname1, String countryresidence1, String email_reg1, String mobile1, int logStatus,String imag_login1) {
        editor.putString(id, id1);
        editor.putString(firstname, firstname1);
        editor.putString(lastname, lastname1);
        editor.putString(countryresidence, countryresidence1);
        editor.putString(mobile, mobile1);
        editor.putString(email_reg, email_reg1);
        editor.putInt(logged_in_status, logStatus);
        editor.putString(imag_login, imag_login1);
        editor.commit();
    }
    public void registerSuccessset(Integer user_id1, Integer otp_val1) {
        editor.putInt(user_id, user_id1);
        editor.putInt(otp_val, otp_val1);
        editor.commit();
    }

    public String getId() {
        String sid = pref.getString(id, null);
        return sid;
    }


    public static String getFirstname() {
        String f_irstname = pref.getString(firstname, null);
        return f_irstname;
    }

    public static String getLastname() {
        String L_astname = pref.getString(lastname, null);
        return L_astname;
    }

    public static String getCountryresidence() {
        String C_ountryres = pref.getString(countryresidence, null);
        return C_ountryres;
    }

    public static String getMobile() {
        String M_obiles = pref.getString(mobile, null);
        return M_obiles;
    }

    public static String getImag_login() {
        String imag_loginl = pref.getString(imag_login, null);
        return imag_loginl;
    }

    public static String getEmail_reg() {
        String email_reg1 = pref.getString(email_reg, null);
        return email_reg1;
    }

    public static int getLoggedInStatus() {
        int log = pref.getInt(logged_in_status, 0);
        return log;
    }

    public static Integer getUser_id() {
        Integer user_id11 = pref.getInt(user_id, 0);
        return user_id11;
    }

    public static Integer getOtp_val() {
        Integer otp_val1 = pref.getInt(otp_val, 0);
        return otp_val1;
    }
    public void logoutUser() {
        editor.clear();
        editor.commit();
    }

    public void setPreferences(Context context, String key, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("Androidwarriors", Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreferences(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("Androidwarriors", Context.MODE_PRIVATE);
        String position = prefs.getString(key, "");
        return position;
    }
}