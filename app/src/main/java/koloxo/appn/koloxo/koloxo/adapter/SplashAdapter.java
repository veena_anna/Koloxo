package koloxo.appn.koloxo.koloxo.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc on 23/2/18.
 */

public class SplashAdapter extends PagerAdapter {
    private ArrayList<Integer> images;
    private ArrayList<String> imgtxt;

    private LayoutInflater inflater;
    private Context context;

    public SplashAdapter(Context context, ArrayList<Integer> images,ArrayList<String> imgtxt) {
        this.context = context;
        this.images=images;
        this.imgtxt=imgtxt;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.splash_img_slide, view, false);
        ImageView myImage = (ImageView) myImageLayout
                .findViewById(R.id.splash_image);


        myImage.setImageResource(images.get(position));
        TextView myText=(TextView)myImageLayout.findViewById(R.id.splash_txt);
        myText.setText(imgtxt.get(position));
        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }
}
