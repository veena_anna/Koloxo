package koloxo.appn.koloxo.koloxo.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.BaseActivity;
import koloxo.appn.koloxo.koloxo.Fragmentbase;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.Property_details;
import koloxo.appn.koloxo.koloxo.adapter.Spinner_Adapter;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class Filter_availability extends android.support.v4.app.Fragment {
 TextView buy,rent,reset,prpty_available,prpty_almstavail,prpty_notavail,prpty_type_huse,prpty_type_apartmt,bedrm_any,bedrm_stdio,bedrm_1,bedrom_2,bedrm_3,bedrm_4,bathrm_any,bathrm_studio,bathrm_1,bathrm_2,bathrm_3,bathrm_4;
    Boolean bedroom_status=true;
    Boolean bathrom_status=true;
    ImageView submit_filter;
    Spinner filter_spinner1,filter_curency;
   String[] List_kilometers= new String[]{"0km","6km","9km"};
   String[] List_filter_curency= new String[]{"Dollers","Dollers","Dollers"};
   /* @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_filter_availability, container, false);*/

   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_filter_availability);*/

    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Filter");
    }

    @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
       // Inflate the layout for this fragment
       View view= inflater.inflate(R.layout.fragment_filter_availability, container, false);
        bottom_bar.setVisibility(View.INVISIBLE);
        filter_spinner1=(Spinner)view.findViewById(R.id.filter_spinner1);
        filter_curency =(Spinner)view.findViewById(R.id.filter_curency);
        // Create an ArrayAdapter using the string array and a default spinner
//        ArrayAdapter<String> staticAdapter = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item, List_kilometers);
        // Specify the layout to use when the list of choices appears
//        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        filter_spinner1.setAdapter(new Spinner_Adapter(getActivity(),List_kilometers));

        // Create an ArrayAdapter using the string array and a default spinner
//        ArrayAdapter<String> staticAdapter1 = new ArrayAdapter<String>(getActivity(),
//                android.R.layout.simple_spinner_item, List_filter_curency);
        // Specify the layout to use when the list of choices appears
//        staticAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        filter_curency.setAdapter(new Spinner_Adapter(getActivity(),List_filter_curency));

        buy=(TextView)view.findViewById(R.id.filtr_buy);
        rent=(TextView)view.findViewById(R.id.filtr_rent) ;
        reset=(TextView)view.findViewById(R.id.filter_reset);
        prpty_available=(TextView)view.findViewById(R.id.filtr_Available);
        prpty_almstavail=(TextView)view.findViewById(R.id.filtr_almstAvailable);
        prpty_notavail=(TextView)view.findViewById(R.id.filtr_notAvailable);
        prpty_type_huse=(TextView)view.findViewById(R.id.filtr_house);
        prpty_type_apartmt=(TextView)view.findViewById(R.id.filtr_apartmnt);

        bedrm_any=(TextView)view.findViewById(R.id.filtr_bedrm_any);
        bedrm_stdio=(TextView)view.findViewById(R.id.filtr_bedrm_studio);
        bedrm_1=(TextView)view.findViewById(R.id.filtr_bedrm_1);
        bedrom_2=(TextView)view.findViewById(R.id.filtr_bedrm_2);
        bedrm_3=(TextView)view.findViewById(R.id.filtr_bedrm_3);
        bedrm_4=(TextView)view.findViewById(R.id.filtr_bedrm_4);

        bathrm_any=(TextView)view.findViewById(R.id.filtr_bsthrm_any);
        bathrm_studio=(TextView)view.findViewById(R.id.filtr_bsthrm_studio);
        bathrm_1=(TextView)view.findViewById(R.id.filtr_bsthrm_1);
        bathrm_2=(TextView)view.findViewById(R.id.filtr_bsthrm_2);
        bathrm_3=(TextView)view.findViewById(R.id.filtr_bsthrm_3);
        bathrm_4=(TextView)view.findViewById(R.id.filtr_bsthrm_4);

        submit_filter=(ImageView)view.findViewById(R.id.submit_filter);





        buy.setBackgroundResource(R.drawable.button_bg_unselect);
        rent.setBackgroundResource(R.drawable.button_bg_unselect);
        reset.setBackgroundResource(R.drawable.button_bg_unselect);
        prpty_available.setBackgroundResource(R.drawable.button_bg_unselect);
        prpty_almstavail.setBackgroundResource(R.drawable.button_bg_unselect);
        prpty_notavail.setBackgroundResource(R.drawable.button_bg_unselect);
        prpty_type_huse.setBackgroundResource(R.drawable.button_bg_unselect);
        prpty_type_apartmt.setBackgroundResource(R.drawable.button_bg_unselect);
        bedrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
        bedrm_stdio.setBackgroundResource(R.drawable.button_bg_unselect);
        bedrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
        bedrom_2.setBackgroundResource(R.drawable.button_bg_unselect);
        bedrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
        bedrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
        bathrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
        bathrm_studio.setBackgroundResource(R.drawable.button_bg_unselect);
        bathrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
        bathrm_2.setBackgroundResource(R.drawable.button_bg_unselect);
        bathrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
        bathrm_4.setBackgroundResource(R.drawable.button_bg_unselect);

         buy.setTextColor(Color.parseColor("#606571"));
        rent.setTextColor(Color.parseColor("#606571"));
        reset.setTextColor(Color.parseColor("#606571"));
        prpty_available.setTextColor(Color.parseColor("#000000"));
        prpty_almstavail.setTextColor(Color.parseColor("#000000"));
        prpty_notavail.setTextColor(Color.parseColor("#000000"));
        prpty_type_huse.setTextColor(Color.parseColor("#000000"));
        prpty_type_apartmt.setTextColor(Color.parseColor("#000000"));
        bedrm_any.setTextColor(Color.parseColor("#000000"));
        bedrm_stdio.setTextColor(Color.parseColor("#000000"));
        bedrm_1.setTextColor(Color.parseColor("#000000"));
        bedrom_2.setTextColor(Color.parseColor("#000000"));
        bedrm_3.setTextColor(Color.parseColor("#000000"));
        bedrm_4.setTextColor(Color.parseColor("#000000"));
        bathrm_any.setTextColor(Color.parseColor("#000000"));
        bathrm_studio.setTextColor(Color.parseColor("#000000"));
        bathrm_1.setTextColor(Color.parseColor("#000000"));
        bathrm_2.setTextColor(Color.parseColor("#000000"));
        bathrm_3.setTextColor(Color.parseColor("#000000"));
        bathrm_4.setTextColor(Color.parseColor("#000000"));



        buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buy.setBackgroundResource(R.drawable.button_bg_salected);
                 buy.setTextColor(Color.parseColor("#fefffb"));
                rent.setBackgroundResource(R.drawable.button_bg_unselect);
                rent.setTextColor(Color.parseColor("#606571"));
            }
        });
        rent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                rent.setBackgroundResource(R.drawable.button_bg_salected);
                rent.setTextColor(Color.parseColor("#fefffb"));
                buy.setBackgroundResource(R.drawable.button_bg_unselect);
                buy.setTextColor(Color.parseColor("#606571"));
            }
        });
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                reset.setBackgroundResource(R.drawable.button_bg_salected);
                reset.setTextColor(Color.parseColor("#fefffb"));
            }
        });

        //proprty availability
        prpty_available.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prpty_available.setBackgroundResource(R.drawable.button_bg_salected);
                prpty_available.setTextColor(Color.parseColor("#fefffb"));

                prpty_almstavail.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_almstavail.setTextColor(Color.parseColor("#000000"));
                prpty_notavail.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_notavail.setTextColor(Color.parseColor("#000000"));
            }
        });
        prpty_almstavail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prpty_almstavail.setBackgroundResource(R.drawable.button_bg_salected);
                prpty_almstavail.setTextColor(Color.parseColor("#fefffb"));

                prpty_available.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_available.setTextColor(Color.parseColor("#000000"));
                prpty_notavail.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_notavail.setTextColor(Color.parseColor("#000000"));
            }
        });
        prpty_notavail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prpty_notavail.setBackgroundResource(R.drawable.button_bg_salected);
                prpty_notavail.setTextColor(Color.parseColor("#fefffb"));

                prpty_almstavail.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_almstavail.setTextColor(Color.parseColor("#000000"));
                prpty_available.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_available.setTextColor(Color.parseColor("#000000"));
            }
        });

        prpty_type_huse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prpty_type_huse.setBackgroundResource(R.drawable.button_bg_salected);
                prpty_type_huse.setTextColor(Color.parseColor("#fefffb"));

                prpty_type_apartmt.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_type_apartmt.setTextColor(Color.parseColor("#000000"));

            }
        });
        prpty_type_apartmt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prpty_type_apartmt.setBackgroundResource(R.drawable.button_bg_salected);
                prpty_type_apartmt.setTextColor(Color.parseColor("#fefffb"));

                prpty_type_huse.setBackgroundResource(R.drawable.button_bg_unselect);
                prpty_type_huse.setTextColor(Color.parseColor("#000000"));

            }
        });
        bedrm_any.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bedrm_any.setBackgroundResource(R.drawable.button_bg_salected);
                bedrm_any.setTextColor(Color.parseColor("#fefffb"));

                bedrm_stdio.setBackgroundResource(R.drawable.button_bg_unselect);
                bedrm_stdio.setTextColor(Color.parseColor("#000000"));

                bedrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                bedrm_1.setTextColor(Color.parseColor("#000000"));

                bedrom_2.setBackgroundResource(R.drawable.button_bg_unselect);
                bedrom_2.setTextColor(Color.parseColor("#000000"));

                bedrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                bedrm_3.setTextColor(Color.parseColor("#000000"));

                bedrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                bedrm_4.setTextColor(Color.parseColor("#000000"));

            }
        });
        bedrm_stdio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bedrm_stdio.setBackgroundResource(R.drawable.button_bg_salected);
                bedrm_stdio.setTextColor(Color.parseColor("#fefffb"));
                bedrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                bedrm_any.setTextColor(Color.parseColor("#000000"));
                bedroom_status=false;

            }
        });

        bedrm_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bedroom_status) {

                    bedrm_1.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrm_1.setTextColor(Color.parseColor("#fefffb"));

                    bedrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_any.setTextColor(Color.parseColor("#000000"));

                    bedrm_stdio.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrm_stdio.setTextColor(Color.parseColor("#fefffb"));

                    bedrom_2.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrom_2.setTextColor(Color.parseColor("#000000"));

                    bedrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_3.setTextColor(Color.parseColor("#000000"));

                    bedrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_4.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        bedrom_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bedroom_status) {
                    bedrom_2.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrom_2.setTextColor(Color.parseColor("#fefffb"));

                    bedrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_any.setTextColor(Color.parseColor("#000000"));

                    bedrm_stdio.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrm_stdio.setTextColor(Color.parseColor("#fefffb"));

                    bedrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_1.setTextColor(Color.parseColor("#000000"));

                    bedrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_3.setTextColor(Color.parseColor("#000000"));

                    bedrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_4.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        bedrm_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bedroom_status) {
                    bedrm_3.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrm_3.setTextColor(Color.parseColor("#fefffb"));

                    bedrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_any.setTextColor(Color.parseColor("#000000"));

                    bedrm_stdio.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrm_stdio.setTextColor(Color.parseColor("#fefffb"));

                    bedrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_1.setTextColor(Color.parseColor("#000000"));

                    bedrom_2.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrom_2.setTextColor(Color.parseColor("#000000"));

                    bedrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_4.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        bedrm_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bedroom_status) {
                    bedrm_4.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrm_4.setTextColor(Color.parseColor("#fefffb"));

                    bedrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_any.setTextColor(Color.parseColor("#000000"));

                    bedrm_stdio.setBackgroundResource(R.drawable.button_bg_salected);
                    bedrm_stdio.setTextColor(Color.parseColor("#fefffb"));

                    bedrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_1.setTextColor(Color.parseColor("#000000"));

                    bedrom_2.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrom_2.setTextColor(Color.parseColor("#000000"));

                    bedrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                    bedrm_3.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        bathrm_any.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bathrm_any.setBackgroundResource(R.drawable.button_bg_salected);
                bathrm_any.setTextColor(Color.parseColor("#fefffb"));

                bathrm_studio.setBackgroundResource(R.drawable.button_bg_unselect);
                bathrm_studio.setTextColor(Color.parseColor("#000000"));

                bathrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                bathrm_1.setTextColor(Color.parseColor("#000000"));

                bathrm_2.setBackgroundResource(R.drawable.button_bg_unselect);
                bathrm_2.setTextColor(Color.parseColor("#000000"));

                bathrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                bathrm_3.setTextColor(Color.parseColor("#000000"));

                bathrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                bathrm_4.setTextColor(Color.parseColor("#000000"));

            }
        });
        bathrm_studio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bathrm_studio.setBackgroundResource(R.drawable.button_bg_salected);
                bathrm_studio.setTextColor(Color.parseColor("#fefffb"));
                bathrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                bathrm_any.setTextColor(Color.parseColor("#000000"));
                bathrom_status=false;

            }
        });

        bathrm_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bathrom_status) {

                    bathrm_1.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_1.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_any.setTextColor(Color.parseColor("#000000"));

                    bathrm_studio.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_studio.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_2.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_2.setTextColor(Color.parseColor("#000000"));

                    bathrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_3.setTextColor(Color.parseColor("#000000"));

                    bathrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_4.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        bathrm_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bathrom_status) {
                    bathrm_2.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_2.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_any.setTextColor(Color.parseColor("#000000"));

                    bathrm_studio.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_studio.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_1.setTextColor(Color.parseColor("#000000"));

                    bathrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_3.setTextColor(Color.parseColor("#000000"));

                    bathrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_4.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        bathrm_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bathrom_status) {
                    bathrm_3.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_3.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_any.setTextColor(Color.parseColor("#000000"));

                    bathrm_studio.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_studio.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_1.setTextColor(Color.parseColor("#000000"));

                    bathrm_2.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_2.setTextColor(Color.parseColor("#000000"));

                    bathrm_4.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_4.setTextColor(Color.parseColor("#000000"));
                }
            }
        });
        bathrm_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!bathrom_status) {
                    bathrm_4.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_4.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_any.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_any.setTextColor(Color.parseColor("#000000"));

                    bathrm_studio.setBackgroundResource(R.drawable.button_bg_salected);
                    bathrm_studio.setTextColor(Color.parseColor("#fefffb"));

                    bathrm_1.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_1.setTextColor(Color.parseColor("#000000"));

                    bathrm_2.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_2.setTextColor(Color.parseColor("#000000"));

                    bathrm_3.setBackgroundResource(R.drawable.button_bg_unselect);
                    bathrm_3.setTextColor(Color.parseColor("#000000"));
                }
            }
        });


        submit_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               BaseActivity.getHpBaseActivity().popFragments(Filter_availability.this);
           //     Toast.makeText(getActivity(),"Submitted for serach",Toast.LENGTH_SHORT).show();
                //Intent intent=new Intent(getActivity(), Property_details.class);
               // startActivity(intent);
            }
        });
return view;
    }


}
