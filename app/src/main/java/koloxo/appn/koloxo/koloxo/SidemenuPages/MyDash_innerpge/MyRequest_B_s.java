package koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge;

import android.app.Activity;
import android.app.Fragment;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.BaseFragment;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Mydash_inner.MyRequest_b_s;
import koloxo.appn.koloxo.koloxo.adapter.Mydash_inner.Request_list_mydash;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;

public class MyRequest_B_s extends android.support.v4.app.Fragment implements AdapterView.OnItemClickListener {
    ListView request_list_b_s;
    MyRequest_b_s request_list_mydash;
    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.img_prpty,
            R.drawable.img_prpty, R.drawable.img_prpty, R.drawable.img_prpty };
    List<Side_mysearchobj> rowItems;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("My Request");
    }
  /*  @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_request__b_s);*/
  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
      // Inflate the layout for this fragment
      //listener.onFragSelected("Dashboard One");
      View view=  inflater.inflate(R.layout.activity_my_request__b_s, container, false);

        request_list_b_s=(ListView)view.findViewById(R.id.list_myrequest_b_s);
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);
        }

        MyRequest_b_s adapter = new MyRequest_b_s(getActivity(), rowItems);
        request_list_b_s.setAdapter(adapter);
        request_list_b_s.setOnItemClickListener(MyRequest_B_s.this);

        //back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
return view;

    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        /*Toast toast = Toast.makeText(getActivity(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();*/
    }
}
