package koloxo.appn.koloxo.koloxo.activity;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDashboard;
import koloxo.appn.koloxo.koloxo.adapter.Document_Profile_adapt;
import koloxo.appn.koloxo.koloxo.adapter.PropertyImgAdapter;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.Webserives.ApiUtils.BASE_URL_PIC;


public class Profile_startup2 extends Fragment {
    RecyclerView recyclerView;
    String[] header = {"Document name", "Document name", "Document name", "Document name"};
    ImageView verify,complete_profile_1;
    DialogPop_rating dialogPop_rating;
    Session_class session_class;
    CircleImageView pro_Profile_2;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Reputed Profile");
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_profile_startup2, container, false);
        dialogPop_rating=new DialogPop_rating(getActivity());
        pro_Profile_2=(CircleImageView)view.findViewById(R.id.pro_Profile_2);
        session_class=new Session_class(getActivity());

        recyclerView = view.findViewById(R.id.recyclerView);
        verify = view.findViewById(R.id.verify);
        complete_profile_1 = view.findViewById(R.id.complete_profile_1);
        Picasso.with(getActivity()).load(BASE_URL_PIC+session_class.getImag_login()).into(pro_Profile_2);

        complete_profile_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPop_rating.initiatePopupWindow_UPLOAD(getActivity());
            }
        });

        verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("My Dashboard");
                getHpBaseActivity().pushFragments(new MyDashboard(), true, true);
            }
        });
        List<Pojo_postprpty_part3> mypojos = new ArrayList<>();
        for (int i = 0; i < header.length; i++) {
            Pojo_postprpty_part3 mypojo = new Pojo_postprpty_part3();
            mypojo.setName(header[i]);
            mypojo.setSelected(0);
            mypojos.add(mypojo);

        }
        recyclerView.setBackgroundColor(Color.parseColor("#F5F8FA"));
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new Document_Profile_adapt(getActivity(), mypojos));
        return view;
    }


}
