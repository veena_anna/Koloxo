package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 5/3/18.
 */

public class Side_mysearches extends BaseAdapter {
    Context context;
    List<Side_mysearchobj> rowItems;

    public Side_mysearches(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.side_rowmysearch, null);
            holder = new ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
           holder.imageView = (ImageView) convertView.findViewById(R.id.mysearch_img);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();
        }

        Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());
       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}