package koloxo.appn.koloxo.koloxo.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.support.annotation.DrawableRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.Fragmentbase;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.InfoWindowData;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Google_Map_custom_adapter;
import koloxo.appn.koloxo.koloxo.fragments.Filter_availability;
import koloxo.appn.koloxo.koloxo.fragments.List_Availabilities_card;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

public class MapsActivity_detalprprty extends android.support.v4.app.Fragment implements OnMapReadyCallback,LocationListener {

    private GoogleMap mMap;
LocationListener mListener;
    LatLngBounds.Builder builder;
    LatLngBounds.Builder builder1;
    CameraUpdate cu;
    ImageView bottom_4_icn,bottom_5_icn;
    ImageView filter_map_id;
    CircleImageView detailspage_click_map;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Search Results");
    }
    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_detalprprty);*/
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_maps_detalprprty, container, false);
        bottom_4_icn=(ImageView)view.findViewById(R.id.bt_b4);
        bottom_5_icn=(ImageView)view. findViewById(R.id.bt_b1);
        filter_map_id=(ImageView)view. findViewById(R.id.filter_map_id);
        bottom_4_icn.setImageResource(R.drawable.ic_comment);
        bottom_5_icn.setImageResource(R.drawable.ic_notification);
        detailspage_click_map=(CircleImageView)view.findViewById(R.id.detailspage_click_map) ;

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        //getSupportFragmentManager()
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        if (ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 101);

        }
        /*mSetUpMap();*/
        filter_map_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent=new Intent(getActivity(),Filter_availability.class);
               // startActivity(intent);
                Title.setText("Filter");
                getHpBaseActivity().pushFragments(new Filter_availability(),true,true);
           // finish();
            }
        });

        detailspage_click_map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Dubai");
                getHpBaseActivity().pushFragments(new List_Availabilities_card(),true,true);
                //Intent intent=new Intent(getActivity(),List_Availabilities_card.class);
                //startActivity(intent);

            }
        });
return view;
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
  try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            getActivity(), R.raw.style_json));

            if (!success) {
                Log.e("N", "Style parsing failed.");
            }
        } catch (Resources.NotFoundException e) {
            Log.e("N", "Can't find style. Error: ", e);
        }
       // new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location));
        // Add a marker in Sydney and move the camera

       /* LatLng sydney = new LatLng(-34, 151);
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);
        mMap.addMarker(new MarkerOptions().position(sydney).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.ic_location)).title("Marker in Sydney"));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));*/


        /**clear the map before redraw to them*/
        mMap.clear();

        /**Create dummy Markers List*/
        List<Marker> markersList = new ArrayList<Marker>();
        Marker Delhi = mMap.addMarker(new MarkerOptions().position(new LatLng(
                28.61, 77.2099)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.button_bg_unselect)).title("Delhi"));

        Marker Chaandigarh = mMap.addMarker(new MarkerOptions().position(new LatLng(30.75, 76.78))
                .icon(bitmapDescriptorFromVector(getActivity(), R.drawable.button_bg_unselect)).title("Chandigarh"));
        Marker SriLanka = mMap.addMarker(new MarkerOptions().position(new LatLng(
                7.000, 81.0000)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.button_bg_unselect)).title("Sri Lanka"));
        Marker America = mMap.addMarker(new MarkerOptions().position(new LatLng(
                38.8833, 77.0167)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.button_bg_unselect)).title("America"));
        Marker Arab = mMap.addMarker(new MarkerOptions().position(new LatLng(
                24.000, 45.000)).icon(bitmapDescriptorFromVector(getActivity(), R.drawable.button_bg_unselect)).title("Arab"));




        /**Put all the markers into arraylist*/
        markersList.add(Delhi);
        markersList.add(SriLanka);
        markersList.add(America);
        markersList.add(Arab);
        markersList.add(Chaandigarh);
        /**create for loop for get the latLngbuilder from the marker list*/
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }


        Google_Map_custom_adapter customInfoWindow = new Google_Map_custom_adapter(getActivity());
        mMap.setInfoWindowAdapter(customInfoWindow);
        InfoWindowData info = new InfoWindowData();
        info.setImage("snowqualmie");
        info.setHotel("Hotel : excellent hotels available");
        info.setFood("Food : all types of restaurants available");
        info.setTransport("Reach the site by bus, car and train.");
        /*Marker m = mMap.addMarker(markersList);
        m.setTag(info);
        m.showInfoWindow();*/

        /**initialize the padding for map boundary*/
        int padding = 50;
        /**create the bounds from latlngBuilder to set into map camera*/
        LatLngBounds bounds = builder.build();
        /**create the camera with bounds and padding to set into map*/
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        /**call the map call back to know map is loaded or not*/
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                /**set animated zoom camera into map*/
                mMap.animateCamera(cu);

            }
        });

       // BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.ic_location);

    }
    /**create method to set map view*/


   /* public void mSetUpMap() {
        *//**clear the map before redraw to them*//*
        mMap.clear();
        *//**Create dummy Markers List*//*
        List<Marker> markersList = new ArrayList<Marker>();
        Marker Delhi = mMap.addMarker(new MarkerOptions().position(new LatLng(
                28.61, 77.2099)).title("Delhi"));
        Marker Chaandigarh = mMap.addMarker(new MarkerOptions().position(new LatLng(
                30.75, 76.78)).title("Chandigarh"));
        Marker SriLanka = mMap.addMarker(new MarkerOptions().position(new LatLng(
                7.000, 81.0000)).title("Sri Lanka"));
        Marker America = mMap.addMarker(new MarkerOptions().position(new LatLng(
                38.8833, 77.0167)).title("America"));
        Marker Arab = mMap.addMarker(new MarkerOptions().position(new LatLng(
                24.000, 45.000)).title("Arab"));

        *//**Put all the markers into arraylist*//*
        markersList.add(Delhi);
        markersList.add(SriLanka);
        markersList.add(America);
        markersList.add(Arab);
        markersList.add(Chaandigarh);

        *//**create for loop for get the latLngbuilder from the marker list*//*
        builder = new LatLngBounds.Builder();
        for (Marker m : markersList) {
            builder.include(m.getPosition());
        }
        *//**initialize the padding for map boundary*//*
        int padding = 50;
        *//**create the bounds from latlngBuilder to set into map camera*//*
        LatLngBounds bounds = builder.build();
        *//**create the camera with bounds and padding to set into map*//*
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        *//**call the map call back to know map is loaded or not*//*
        mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                *//**set animated zoom camera into map*//*
                mMap.animateCamera(cu);

            }
        });
    }
*/
    private BitmapDescriptor bitmapDescriptorFromVector(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_red);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    private BitmapDescriptor bitmapDescriptorFromVector1(Context context, @DrawableRes int vectorDrawableResourceId) {
        Drawable background = ContextCompat.getDrawable(context, R.drawable.ic_green);
        background.setBounds(0, 0, background.getIntrinsicWidth(), background.getIntrinsicHeight());
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorDrawableResourceId);
        vectorDrawable.setBounds(40, 20, vectorDrawable.getIntrinsicWidth() + 40, vectorDrawable.getIntrinsicHeight() + 20);
        Bitmap bitmap = Bitmap.createBitmap(background.getIntrinsicWidth(), background.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        background.draw(canvas);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }
    @Override
    public void onLocationChanged(Location location) {
        if( mListener != null )
        {
            mListener.onLocationChanged( location );

            //Move the camera to the user's location and zoom in!
            //mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 17.0f));
            // Zoom in, animating the camera.
           // mMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
           // mMap.setMaxZoomPreference(14.0f);
        }
    }
}
