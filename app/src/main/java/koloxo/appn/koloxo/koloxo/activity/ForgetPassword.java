package koloxo.appn.koloxo.koloxo.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Webserives.PojoForgetpass;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.MainActivity.android_id;

public class ForgetPassword extends Activitybase {
RelativeLayout forgortpass_back;
ImageView requst_send_succs;
EditText enter_for_email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        forgortpass_back=(RelativeLayout)findViewById(R.id.back_forgotpass);
        requst_send_succs=(ImageView)findViewById(R.id.requst_send_succs);
        enter_for_email=findViewById(R.id.enter_for_email);
        forgortpass_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

                requst_send_succs.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        final Call<JsonObject> forgetcall = new Retrofit_Helper().getRetrofitBuilder().getForgetpass(enter_for_email.getText().toString(), "android", android_id);
                        if (!(enter_for_email.getText().length() == 0))

                        {

                            forgetcall.enqueue(new ResponseHandler(true, ForgetPassword.this, new ResponseCallback() {

                                @Override
                                public void getResponse(int code, JsonObject jsonObject) {
                                    PojoForgetpass forgetpass = new GsonBuilder().create().fromJson(jsonObject, PojoForgetpass.class);

                                    if (forgetpass.getErrorCode().equals("0")) {

                                        Intent intent = new Intent(getApplicationContext(), Send_Request.class);
                                        startActivity(intent);
                                        finish();


                                        // session_class.createUserLoginSession(loginData.getData().getUserId(), loginData.getData().getFirstName(), loginData.getData().getLastName(), loginData.getData().getMobileNumber(), loginData.getData().getEmail(), loginData.getData().getImage());

                                    } else if (forgetpass.getErrorCode().equals("1")) {
                                        Dialoghelper.showSnackbar(ForgetPassword.this, "Email doesn't Exist");

                                    }


                                    Log.d("Koloxo_login", "onResponse: message " + forgetpass.getMessage());
                                    //Toast.makeText(ForgetPassword.this, "Login Success", Toast.LENGTH_SHORT).show();
                                    // session_class.createUserLoginSession(response.body().getData().getUserId());
                                }


                                @Override
                                public void getError(Call<JsonObject> call, String message) {
                                    Dialoghelper.showSnackbar(ForgetPassword.this, message);
                                }
                            }, forgetcall));
                        } else {
                            Dialoghelper.showSnackbar(ForgetPassword.this, "Please enter email ID");
                        }
                    }
                });
              /*  Intent intent=new Intent(getApplicationContext(),Send_Request.class);
                startActivity(intent);
                finish();*/

    }
}
