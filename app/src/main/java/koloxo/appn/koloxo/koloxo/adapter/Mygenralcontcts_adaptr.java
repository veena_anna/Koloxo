package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Myreq_innr.Request_accepted_list;
import koloxo.appn.koloxo.koloxo.activity.Chat_Section;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 21/3/18.
 */

public class Mygenralcontcts_adaptr extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;

    public Mygenralcontcts_adaptr(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        CardView generalcard;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Mygenralcontcts_adaptr.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
       if (convertView == null) {
        convertView = mInflater.inflate(R.layout.mygenralcontacts_row, parent,false);
        holder = new Mygenralcontcts_adaptr.ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
           holder.imageView = (ImageView) convertView.findViewById(R.id.mygenral_img);
           holder.generalcard = (CardView) convertView.findViewById(R.id.generalrlcard);
      convertView.setTag(holder);
       }
       else {
            holder = (Mygenralcontcts_adaptr.ViewHolder) convertView.getTag();
      }

     Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
           holder.imageView.setImageResource(rowItem.getImageId());
        holder.generalcard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Dallas Cape Suite");
                getHpBaseActivity().pushFragments1(new Chat_Section(),true,true);
            }
        });

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}