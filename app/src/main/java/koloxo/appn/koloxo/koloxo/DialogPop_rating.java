package koloxo.appn.koloxo.koloxo;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sembozdemir.viewpagerarrowindicator.library.ViewPagerArrowIndicator;

import java.util.ArrayList;

import koloxo.appn.koloxo.koloxo.activity.ImageSliderPopup;
import koloxo.appn.koloxo.koloxo.adapter.CircleIndicator_Adapter;

/**
 * Created by appzoc-php on 13/3/18.
 */

public class DialogPop_rating {
    Dialog m_dialog;

    //foslide
    private ViewPager viewPager;
    private ViewPagerArrowIndicator viewPagerArrowIndicator;
    ArrayList<Integer> array_image;

    public DialogPop_rating(Context context) {
    }

    public void initiatePopupWindow_Lawyers(Context activity) {

        m_dialog = new Dialog(activity);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        m_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        LayoutInflater m_inflater = LayoutInflater.from(activity);
        View m_view = m_inflater.inflate(R.layout.activity_rating__page, null);
        ImageView rate_close = (ImageView) m_view.findViewById(R.id.rating_close_id);
        ImageView pop_forward = (ImageView)m_view.findViewById(R.id.pop_forward);
        pop_forward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });
        rate_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });

        // RelativeLayout pop_lawyer_done_id_relat = (RelativeLayout) m_view.findViewById(R.id.pop_lawyer_done_id_relat);
        //server call for lawyers listing
        m_dialog.setContentView(m_view);
        buildDialog(R.anim.slide_to_right);
        m_dialog.show();

    }

    public void intiate_terms_condition(Context activity,String content) {

        m_dialog = new Dialog(activity);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        m_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        LayoutInflater m_inflater = LayoutInflater.from(activity);
        View m_view = m_inflater.inflate(R.layout.terms_condition, null);
        ImageView rate_close = (ImageView) m_view.findViewById(R.id.terms_close_id);
        TextView txt_terms1 = (TextView) m_view.findViewById(R.id.txt_terms1);

        txt_terms1.setText(content);
        rate_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });

        // RelativeLayout pop_lawyer_done_id_relat = (RelativeLayout) m_view.findViewById(R.id.pop_lawyer_done_id_relat);
        //server call for lawyers listing
        m_dialog.setContentView(m_view);
        buildDialog(R.anim.slide_to_right);
        m_dialog.show();

    }

    private void buildDialog(int animationSource) {

        m_dialog.getWindow().getAttributes().windowAnimations = animationSource;

    }

    public void initiatePopupWindow_ADD(Context activity) {


        m_dialog = new Dialog(activity);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        m_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        LayoutInflater m_inflater = LayoutInflater.from(activity);
        View m_view = m_inflater.inflate(R.layout.activity_add_plcemark_postcase, null);

        Button UploadNowadd = (Button) m_view.findViewById(R.id.UploadNowadd);

        // ImageView rate_close=(ImageView)m_view.findViewById(R.id.rating_close_id);
       /* rate_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });*/
        UploadNowadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        // RelativeLayout pop_lawyer_done_id_relat = (RelativeLayout) m_view.findViewById(R.id.pop_lawyer_done_id_relat);
        //server call for lawyers listing
        m_dialog.setContentView(m_view);
        buildDialog(R.anim.slide_to_right);
        m_dialog.show();

    }

    public void initiatePopupWindow_UPLOAD(Context activity) {


        m_dialog = new Dialog(activity);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        m_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        LayoutInflater m_inflater = LayoutInflater.from(activity);
        View m_view = m_inflater.inflate(R.layout.activity_upload_file, null);
        RelativeLayout relativeLayout = (RelativeLayout) m_view.findViewById(R.id.closing);
        Button uploadnow = (Button) m_view.findViewById(R.id.uploadnow);
        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });
        uploadnow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                m_dialog.dismiss();
            }
        });
        // ImageView rate_close=(ImageView)m_view.findViewById(R.id.rating_close_id);
       /* rate_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });*/

        // RelativeLayout pop_lawyer_done_id_relat = (RelativeLayout) m_view.findViewById(R.id.pop_lawyer_done_id_relat);
        //server call for lawyers listing
        m_dialog.setContentView(m_view);
        buildDialog(R.anim.slide_to_right);
        m_dialog.show();

    }

    public void initiatePopupWindow_smallRATING(Context activity) {


        m_dialog = new Dialog(activity);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        m_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        LayoutInflater m_inflater = LayoutInflater.from(activity);
        View m_view = m_inflater.inflate(R.layout.rating_page_small, null);
        // ImageView rate_close=(ImageView)m_view.findViewById(R.id.rating_close_id);
       /* rate_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                m_dialog.dismiss();
            }
        });*/

        // RelativeLayout pop_lawyer_done_id_relat = (RelativeLayout) m_view.findViewById(R.id.pop_lawyer_done_id_relat);
        //server call for lawyers listing
        m_dialog.setContentView(m_view);
        buildDialog(R.anim.slide_to_right);
        m_dialog.show();

    }
   /* public void initiatePopupWindow_viewslide(Context activity) {



        m_dialog = new Dialog(activity);
        m_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        m_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        m_dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

        LayoutInflater m_inflater = LayoutInflater.from(activity);
        View m_view = m_inflater.inflate(R.layout.activity_image_slider_popup, null);


        viewPager = (ViewPager)m_view.findViewById(R.id.viewPager);
        viewPagerArrowIndicator = (ViewPagerArrowIndicator) m_view.findViewById(R.id.viewPagerArrowIndicator);
        array_image = new ArrayList<Integer>();
        array_image.add(R.drawable.splash_pic_2);
        array_image.add(R.drawable.splash_pic_1);
        array_image.add(R.drawable.splash_pic_1);
        viewPagerArrowIndicator.setArrowIndicatorRes(R.drawable.ic_arrow_left, R.drawable.ic_arrow_point_to_right);
        viewPager.setAdapter(new CircleIndicator_Adapter(array_image, activity));
        viewPagerArrowIndicator.bind(viewPager);

        m_dialog.setContentView(m_view);
        m_dialog.show();

    }*/
}
