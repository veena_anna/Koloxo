package koloxo.appn.koloxo.koloxo.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 9/3/18.
 */

public class DummyAdapter  extends PagerAdapter {
    ArrayList<Integer> sliderPojos;
    ArrayList<String> slidetxt;
    ArrayList<String> slidetxt1;

    private int mSize;
    Context context;

    public DummyAdapter(ArrayList<Integer> sliderPojos, ArrayList<String> array_txt, ArrayList<String> slidetxtd, Context context) {
        this.sliderPojos = sliderPojos;
        this.slidetxt=slidetxtd;
        this.slidetxt1=array_txt;
        this.context = context;
        mSize = sliderPojos.size();

    }



    @Override
    public int getCount() {
        return mSize;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup view, int position, Object object) {
        view.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        View itemView = LayoutInflater.from(context).inflate(R.layout.dummy_xml, view, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.splash_image);
        TextView textView=(TextView)itemView.findViewById(R.id.splash_txt);
        TextView textView1=(TextView)itemView.findViewById(R.id.splash_txt1);
        imageView.setImageResource(sliderPojos.get(position));
        textView1.setText(slidetxt.get(position));
        textView.setText(slidetxt1.get(position));
        view.addView(itemView);

        return itemView;
    }


}