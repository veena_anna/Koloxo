package koloxo.appn.koloxo.koloxo.SideMenu;

import android.app.Activity;
import android.content.Context;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.MainActivity;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.activity.AddingProperty;
import koloxo.appn.koloxo.koloxo.activity.MySearch_frm_home;

import static koloxo.appn.koloxo.koloxo.Webserives.ApiUtils.BASE_URL_PIC;

/**
 * Created by appzoc-php on 28/2/18.
 */

public class SideMenuPage  {
    //for drawerlayout

    public DrawerLayout mDrawerLayout;
    ViewGroup decor;
    LayoutInflater inflater;
    FrameLayout container;
    View child;
    float statusBarHeight;
    ImageView slHeaderlogo;
    RelativeLayout close_icn;
    RelativeLayout side_back_icn;
    ImageView s1headerimg;

    public boolean isInnerPageShown = false;
    public boolean isSideMenuShown = false;
    Animation fadeIn, fadeOut, fadeOutMenu, fadeInMenu;
    Session_class session_class;
    //setting views for sidemenu texts
    int sample;



    public SideMenuPage(MainActivity mainActivity) {
    }

    public SideMenuPage(MainLandPage mainLandPage) {
    }

    public SideMenuPage(MySearch_frm_home mySearch_frm_home) {
    }
    public SideMenuPage(AddingProperty addingProperty) {
    }



    public void setAnimations(Activity activity) {
        //		fadeOut
        fadeOutMenu = new AlphaAnimation(1, 0);
        fadeOutMenu.setDuration(200);
        fadeOutMenu.setFillAfter(true);
        //		fadeIn
        fadeInMenu = new AlphaAnimation(0, 1);
        fadeInMenu.setDuration(50);
        fadeInMenu.setFillAfter(true);
    }

    public void setupSlidemenu(final Activity activity) {

        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mDrawerLayout = (DrawerLayout) inflater.inflate(R.layout.sidemenu, null); // "null" is important.
        decor = (ViewGroup) activity.getWindow().getDecorView();
     /* activity.getWindow().setFlags(WindowManager.LayoutParams.,R.color.primary_dark);*/
        activity.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        child = decor.getChildAt(0);
        decor.removeView(child);

        container = (FrameLayout) mDrawerLayout.findViewById(R.id.container); // This is the container we defined just now.

        RelativeLayout layout = (RelativeLayout) mDrawerLayout.findViewById(R.id.layout);

        /* Initializing Parent */
        DrawerLayout.LayoutParams params = (DrawerLayout.LayoutParams) layout.getLayoutParams();
        params.topMargin = (int) statusBarHeight;
        params.width= DrawerLayout.LayoutParams.MATCH_PARENT;


session_class=new Session_class(activity);
        slHeaderlogo = (ImageView) mDrawerLayout.findViewById(R.id.sl_bg);
        s1headerimg=mDrawerLayout.findViewById(R.id.side_l2_img_pic);
        close_icn=mDrawerLayout.findViewById(R.id.side_l1_close);
        side_back_icn=mDrawerLayout.findViewById(R.id.side_l1_back);

        if (session_class.getId()==null)
        {sample= MyConstants.LoggedIn.FALSE;

        }
        else {
            sample=MyConstants.LoggedIn.TRUE;
        }
        if(sample== MyConstants.LoggedIn.TRUE) {
            Picasso.with(activity).load(BASE_URL_PIC + session_class.getImag_login()).into(s1headerimg);
        }
        else {
            Picasso.with(activity).load(R.drawable.sample_pic).into(s1headerimg);
        }
        close_icn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawers();
            }
        });
        side_back_icn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.closeDrawers();
            }
        });



     /*   slheadertxt = (TextView) mDrawerLayout.findViewById(R.id.slheader_text);
        slheadertxt.setText(fname);
        wish = (TextView) findViewById(R.id.wish);
        emojitxt = (TextView) findViewById(R.id.sl_emoji_label);*/


        // layout.setLayoutParams(params);

        container.addView(child);
        decor.addView(mDrawerLayout);

    }





}
