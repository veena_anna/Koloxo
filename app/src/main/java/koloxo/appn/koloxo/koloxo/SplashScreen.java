package koloxo.appn.koloxo.koloxo;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import koloxo.appn.koloxo.koloxo.adapter.SplashAdapter;
import me.relex.circleindicator.CircleIndicator;

public class SplashScreen extends Activitybase {
    //intializing
    private static ViewPager splashPager;
    private static int currentPage = 0;
    Timer swipeTimer;
    //textview for skip
    TextView skip_splash;
    View touchView;
    boolean enabled =true;
    private static final Integer[] Simg= {R.drawable.splash_pic_1,R.drawable.splash_pic_2,R.drawable.splash_pic_3};
   // private static final String[] Stxt={"Welcome to keloxo","Welcome to keloxo","Welcome to keloxo"};
    private ArrayList<Integer> SimgArray = new ArrayList<Integer>();
    private ArrayList<String> StxtArray = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        skip_splash=(TextView)findViewById(R.id.splash_skip);
        //slide call
        init();

         touchView = findViewById(R.id.Splashpager);
        touchView.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View v, MotionEvent event)
            {
                return true;
            }
        });
    }



    public void SKIP(View view)
    {
        Intent skip_intent=new Intent(SplashScreen.this,MainActivity.class);
        startActivity(skip_intent);
        finish();
    }
    private void init() {
        int i;
        for( i=0;i<Simg.length;i++)
            SimgArray.add(Simg[i]);
        StxtArray.add(0,"KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        StxtArray.add(1,"KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");
        StxtArray.add(2,"KOLOXO IS A APP FOR BUY AND SELL PRODUCTS");



        splashPager = (ViewPager) findViewById(R.id.Splashpager);
        splashPager.setAdapter(new SplashAdapter(SplashScreen.this,SimgArray,StxtArray));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);

        indicator.setViewPager(splashPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == Simg.length) {

                    Intent intent=new Intent(SplashScreen.this,MainActivity.class);
                    finish();
                    startActivity(intent);





                }
                splashPager.setCurrentItem(currentPage++, true);
            }
        };
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(Update);
            }
        }, 1500, 1500);
    }

}
