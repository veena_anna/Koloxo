package koloxo.appn.koloxo.koloxo.adapter.MyReqst_inner;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.activity.Chat_Section;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 13/3/18.
 */

public class Request_accpt_adpter extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;

    public Request_accpt_adpter(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        CircleImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        CardView dalls;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
      Request_accpt_adpter.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
       if (convertView == null) {
       convertView= mInflater.inflate(R.layout.side_request_accptrow, parent,false);
      holder = new Request_accpt_adpter.ViewHolder();
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
            holder.imageView = (CircleImageView) convertView.findViewById(R.id.side_my_reqaccptrow_img);
            holder.dalls = (CardView) convertView.findViewById(R.id.card_dalls_suit);
       convertView.setTag(holder);
        }
       else {
           holder = (Request_accpt_adpter.ViewHolder) convertView.getTag();
       }

     Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());

        holder.dalls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Dallas Cape Suite");
                getHpBaseActivity().pushFragments(new Chat_Section(),true,true);
            }
        });

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/

        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}