package koloxo.appn.koloxo.koloxo;

import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import koloxo.appn.koloxo.koloxo.MainLanding.Search_frm_home;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Added_properties;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Requsted_prsn_list;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.Property_review_list;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MySearches;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Myreq_innr.Request_accepted_list;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Notification;
import koloxo.appn.koloxo.koloxo.activity.AddingProperty;
import koloxo.appn.koloxo.koloxo.activity.ChatBasePage;
import koloxo.appn.koloxo.koloxo.activity.MapsActivity_detalprprty;
import koloxo.appn.koloxo.koloxo.activity.Profile_startup1;
import koloxo.appn.koloxo.koloxo.fragments.Filter_availability;
import koloxo.appn.koloxo.koloxo.fragments.List_Availabilities_card;


/**
 * Created by seedzz on 7/4/2017.
 */

public class BaseActivity  extends Activitybase {

    protected static BaseActivity BaseActivity;
    public BaseFragment BaseFragment;
    private static final String TAG = "HomePlusBaseActivity";
    RelativeLayout fragment_container, relLayTopBar;
    public static Context con;
    public static String downloadUrl;
    public static boolean isTop;
   // Session_Class session_class;
    //toast
    LayoutInflater inflater;
    TextView text;
    View layout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //WindowUtils.setFullScreenWithAction(this);
        BaseActivity = this;
        //session_class=new Session_Class(BaseActivity);
       /* inflater = LayoutInflater.from(getApplicationContext());
        layout = inflater.inflate(R.layout.toast_xml,
                (ViewGroup)findViewById(R.id.custom_toast_container));

        text = (TextView) layout.findViewById(R.id.text);*/


        //VolleyUtils.init(this);
    }

    public static BaseActivity getHpBaseActivity() {

        return BaseActivity;
    }


    public void pushFragments(Fragment fragment, boolean shouldAnimate,
                              boolean shouldAdd) {
        FragmentManager manager = getSupportFragmentManager();
        String backStateName = fragment.getClass().getName();

        if (isNeedTransaction(backStateName)) {
            boolean fragmentPopped = manager.popBackStackImmediate(
                    backStateName, 0);

            if (!fragmentPopped) { // fragment not in back stack, create it.
                FragmentTransaction ft = manager.beginTransaction();
                if (shouldAnimate)
                    ft.setCustomAnimations(R.anim.slide_to_left,R.anim.slide_to_right);
                ft.replace(R.id.fragment_container, fragment, backStateName);
                if (shouldAdd)
                    ft.addToBackStack(backStateName);
                ft.commit();
                manager.executePendingTransactions();
            }
        }
    }
    public void pushFragments1(Fragment fragment, boolean shouldAnimate,
                              boolean shouldAdd) {
        FragmentManager manager = getSupportFragmentManager();
        String backStateName = fragment.getClass().getName();

        if (isNeedTransaction(backStateName)) {
            boolean fragmentPopped = manager.popBackStackImmediate(
                    backStateName, 0);

            if (!fragmentPopped) { // fragment not in back stack, create it.
                FragmentTransaction ft = manager.beginTransaction();
                if (shouldAnimate)
                    ft.setCustomAnimations(R.anim.slide_up,R.anim.slide_down);
                ft.replace(R.id.fragment_container, fragment, backStateName);
                if (shouldAdd)
                    ft.addToBackStack(backStateName);
                ft.commit();
                manager.executePendingTransactions();
            }
        }
    }

    private boolean isNeedTransaction(String backStateName) {
        boolean needTransaction = true;
        if (BaseFragment != null) {
            String baseFrag = BaseFragment.getClass().getName();
            if (baseFrag.equals(backStateName)) {

                needTransaction = false;
            } else
                needTransaction = true;
        }
        return needTransaction;
    }

    public void pushFragments4LanSwitch(Fragment fragment, boolean shouldAnimate) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate)
            // ft.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left);
            ft.replace(R.id.fragment_container, fragment);
        ft.addToBackStack(null);
        ft.commit();
        manager.executePendingTransactions();
    }

    public void clearAllBackStackEntries() {
        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack(0, FragmentManager.POP_BACK_STACK_INCLUSIVE);

    }

    public void popFragments(Fragment frag) {
        Log.e("Enterd here", "Inside pop fragment");
        try {
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction ft = manager.beginTransaction();
            String fragName = frag.getClass().getName();
            manager.popBackStack(fragName,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE);
            ft.remove(frag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public  void myFun(Fragment fragment){
        getSupportFragmentManager().beginTransaction().add(R.id.fragment_container,fragment)
                .addToBackStack("").commit();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) BaseActivity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager
                .getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }



    //.............................For spinewheel ....................................................//

    private Dialog spinWheelDialog;
    Handler spinWheelTimer = new Handler(); // Handler to post a runnable that
    // can dismiss spinweheel afrer a
    // specific time
  /*  public static final int SPINWHEEL_LIFE_TIME = 700; *//*
														 * Dismiss spin wheel
														 * after 5 seconds
														 */

    public void startSpinwheel(boolean isCancelable) {
        // Log.d(TAG, "startSpinwheel"+getCurrentActivity().getClass() );
        // If already showing no need to create.
        if (spinWheelDialog != null && spinWheelDialog.isShowing())
            return;
      ////  spinWheelDialog = new Dialog(BaseActivity, R.style.wait_spinner_style);
        ProgressBar progressBar = new ProgressBar(BaseActivity);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(android.app.ActionBar.LayoutParams.WRAP_CONTENT, android.app.ActionBar.LayoutParams.WRAP_CONTENT);
        spinWheelDialog.addContentView(progressBar, layoutParams);
        spinWheelDialog.setCancelable(isCancelable);
        spinWheelDialog.show();
        // start timer for SPINWHEEL_LIFE_TIME
        spinWheelTimer.removeCallbacks(dismissSpinner);
        //if (setDefaultLifetime) // If requested for default dismiss time.
        //spinWheelTimer.postAtTime(dismissSpinner, SystemClock.uptimeMillis() + SPINWHEEL_LIFE_TIME);

        spinWheelDialog.setCanceledOnTouchOutside(false);
    }

   /* public void startSpinwheel(boolean isCancelable, int layoutid, int timeOutSec) {
        startSpinwheel(true, isCancelable);
        spinWheelTimer.removeCallbacks(dismissSpinner);
        spinWheelTimer.postAtTime(dismissSpinner, SystemClock.uptimeMillis() + timeOutSec);
        spinWheelDialog.setContentView(layoutid);
    }*/

    /**
     * Closes the spin wheel dialog
     */

    public void stopSpinWheel() {
        // Log.d(TAG, "stopSpinWheel"+getCurrentActivity().getClass());
        if (spinWheelDialog != null)
            try {
                spinWheelDialog.dismiss();
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Parent is died while tryingto dismiss spin wheel dialog ");
                e.printStackTrace();
            }
        spinWheelDialog = null;
    }

    Runnable dismissSpinner = new Runnable() {

        @Override
        public void run() {
            stopSpinWheel();
        }

    };

    // Callback for spin wheel dismissal
    protected void onSpinWheelDismissed() {
        Log.d(TAG, "Spin wheel disconnected");
    }

    //.............................For spinewheel ....................................................//

    @Override
    public void onBackPressed() {
        String fragTag = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1).getName();
        Log.e(TAG, "frag Tag : " + fragTag);
       // getSupportFragmentManager().popBackStackImmediate();
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                boolean done = getSupportFragmentManager().popBackStackImmediate();


            }

            else{
              finish();
            }
//        if(fragTag.equals("koloxo.appn.koloxo.koloxo.activity.AddingProperty"))
//        {
//            Log.e(TAG, "frag Tag2 :dghgh");
//            //popFragments(new Search_frm_home());
//           finish();
//            // super.onBackPressed();
//        }
        //april 11 update
      /*  if(fragTag.equals("koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Requsted_prsn_list"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new Requsted_prsn_list());
//           finish();
            // super.onBackPressed();
        }*/
        if(fragTag.equals("koloxo.appn.koloxo.koloxo.MainLanding.Search_frm_home"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            //popFragments(new Search_frm_home());

            finish();
            // super.onBackPressed();
        }
        //april 11 update
       /* if(fragTag.equals("koloxo.appn.koloxo.koloxo.SidemenuPages.Notification"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new Notification());
           // super.onBackPressed();
        }*/
        //april 11 update
        /*if(fragTag.equals("koloxo.appn.koloxo.koloxo.activity.ChatBasePage"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new ChatBasePage());
            // super.onBackPressed();
        }*/
        //april 11 update
      /*  if(fragTag.equals("koloxo.appn.koloxo.koloxo.activity.MapsActivity_detalprprty"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new MapsActivity_detalprprty());
            // super.onBackPressed();
        }*/
        //april 11 update
       /* if(fragTag.equals("koloxo.appn.koloxo.koloxo.fragments.Filter_availability"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new Filter_availability());
            // super.onBackPressed();
        }*/
        //april 11 update
        /*if(fragTag.equals("koloxo.appn.koloxo.koloxo.fragments.List_Availabilities_card"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new List_Availabilities_card());
            // super.onBackPressed();
        }*/

     //april 11 updatesslast
       /* if(fragTag.equals("koloxo.appn.koloxo.koloxo.SidemenuPages.Myreq_innr.Request_accepted_list"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new Request_accepted_list());
            // super.onBackPressed();
        } if(fragTag.equals("koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.Property_review_list"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new Property_review_list());
            // super.onBackPressed();
        }*/


        //aprill 11 update
//        if(fragTag.equals("koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Added_properties"))
//        {
//            Log.e(TAG, "frag Tag2 :dghgh");
//            popFragments(new Added_properties());
//            // super.onBackPressed();
//       }
      /*   if(fragTag.equals("koloxo.appn.koloxo.koloxo.activity.MySearches"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            popFragments(new MySearches());
            // super.onBackPressed();
        }*/

        if(fragTag.equals("koloxo.appn.koloxo.koloxo.activity.Profile_startup1"))
        {
            Log.e(TAG, "frag Tag2 :dghgh");
            finish();
            // super.onBackPressed();
        }


        //if(fragTag.equals("koloxo.appn.koloxo.koloxo.fragments.List_Availabilities_card"))
       // {
           // BaseActivity.popFragments(new List_Availabilities_card());
            //getSupportFragmentManager().popBackStack();
           // getSupportFragmentManager().popBackStackImmediate();
            //finish();
           // super.onBackPressed();
       // }

      /* if(isNetworkAvailable())
        {
            if(fragTag.equals("koloxo.appn.koloxo.koloxo.SidemenuPages.MyDasboard"))

    }*/



}}