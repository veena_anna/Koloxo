package koloxo.appn.koloxo.koloxo.activity;

import android.app.Fragment;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.Webserives.PojoProfile;
import koloxo.appn.koloxo.koloxo.Webserives.PojoTerms;
import koloxo.appn.koloxo.koloxo.adapter.PropertyImgAdapter;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.Webserives.ApiUtils.BASE_URL_PIC;

public class ProfilePage extends android.support.v4.app.Fragment {
    RecyclerView recyclerView;
    //String[]  header ;

/// String[] header = {"Document name", "Document name", "Document name", "Document name"};
    Session_class session_class;
    TextView firstname,lastaname,country_residnce,email,mobilno;
    View view;
    String DocumentTitle,DocumentFile;
    // declare a string array with initial size
    String[] header ;
    CircleImageView profil_img_inner;



    List<Pojo_postprpty_part3> mypojos;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Profile");
    }
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         view=inflater.inflate(R.layout.activity_profile_page, container, false);
        initialvar();
        session_class=new Session_class(getActivity());

       // Toast.makeText(getActivity(),session_class.getId(),Toast.LENGTH_SHORT).show();
        final Call<JsonObject> profilecall = new Retrofit_Helper().getRetrofitBuilder().getProfilePage(session_class.getId());


        profilecall.enqueue(new ResponseHandler(true, getActivity(), new ResponseCallback() {
            @Override
            public void getResponse(int code, JsonObject jsonObject) {
                Log.d("profiletest", "getResponse: "+ jsonObject.toString());
                PojoProfile pojoProfile=new GsonBuilder().create().fromJson(jsonObject, PojoProfile.class);

                if(pojoProfile.getErrorCode().equals("0")) {
                    firstname.setText(pojoProfile.getData().getFirstName());
                    lastaname.setText(pojoProfile.getData().getLastName());
                    country_residnce.setText(pojoProfile.getData().getCountryResidence());
                    email.setText(pojoProfile.getData().getEmail());
                    mobilno.setText(pojoProfile.getData().getMobileNumber());
                    Picasso.with(getActivity()).load(BASE_URL_PIC+session_class.getImag_login()).into(profil_img_inner);
                    for (int i = 0; i < pojoProfile.getData().getDocument().size(); i++)
                       {
                        Log.d("document", "getResponsedoc: " + pojoProfile.getData().getDocument().get(i).getUserId());
                }
              // DocumentTitle=  pojoProfile.getData().getDocument().getDocumentTitle();
              //DocumentFile=  pojoProfile.getData().getDocument().getDocumentFile();

               //header={DocumentTitle};

if(pojoProfile.getData().getDocument().size()>0) {
    header = new String[pojoProfile.getData().getDocument().size()];
    for (int i = 0; i < pojoProfile.getData().getDocument().size(); i++) {
        String data = pojoProfile.getData().getDocument().get(i).getDocumentTitle();
        header[i] = "\"" + data + "\"";

    }
    mypojos = new ArrayList<>();
    for (int j = 0; j < pojoProfile.getData().getDocument().size(); j++) {
        Pojo_postprpty_part3 mypojo = new Pojo_postprpty_part3();
        mypojo.setName(header[j]);
        mypojo.setSelected(0);
        mypojos.add(mypojo);

    }
    recyclerView = view.findViewById(R.id.recyclerView);

    recyclerView.setBackgroundColor(Color.parseColor("#F5F8FA"));
    recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    recyclerView.setAdapter(new PropertyImgAdapter(getActivity(), mypojos));

    Log.d("terms_case", "onResponse: message " + pojoProfile.getMessage());
}
else {

     }
                }

                Log.d("terms", "onResponse: message " + pojoProfile.getErrorCode());

            }
            @Override
            public void getError(Call<JsonObject> call, String message) {
                Dialoghelper.showSnackbar(getActivity(), message);
            }
        }, profilecall));




        return view;
    }
   public void initialvar()
   {
       firstname=view.findViewById(R.id.Profile_frst_name);
       lastaname=view.findViewById(R.id.Profile_last_name);
       country_residnce=view.findViewById(R.id.Profile_country_name);
       email=view.findViewById(R.id.Profile_email_name);
       mobilno=view.findViewById(R.id.Profile_mobile_name);
       profil_img_inner=view.findViewById(R.id.profil_img_inner);
   }

}

