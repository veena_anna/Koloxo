package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.Objects.post_prpty_part3_innr_pojo;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 21/3/18.
 */

public class Post_prpty_part3 extends RecyclerView.Adapter<Post_prpty_part3.MyViewHolder> {

    Context context;
    List<Pojo_postprpty_part3> mypojos;
//pojo for items inside row ogf main ex
    List<post_prpty_part3_innr_pojo> cardItems = new ArrayList<>();


    public Post_prpty_part3(Context context, List<Pojo_postprpty_part3> mypojos) {
        this.context = context;
        this.mypojos = mypojos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.post_porpty_part3_row, parent, false);
        View currentFocus = ((Activity)context).getCurrentFocus();
        if (currentFocus != null) {
            currentFocus.clearFocus();
        }
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Pojo_postprpty_part3 mypojo = mypojos.get(position);
        holder.header.setText(mypojo.getName());

        if (mypojos.get(position).getSelected() == 0) {
            holder.cardItem.setVisibility(View.GONE);
            holder.imgbtn.setImageResource(R.drawable.arrowdown);
        } else {
            holder.cardItem.setVisibility(View.VISIBLE);
            holder.imgbtn.setImageResource(R.drawable.arrowup);

            // List<CardItems>cardItems=new ArrayList<>();

            cardItems.clear();

            for (int i = 0; i < 2; i++) {
                post_prpty_part3_innr_pojo cardItems1 = new post_prpty_part3_innr_pojo();
                cardItems1.setTitle("hello");
                cardItems1.setDescription("Rounded corners relative layout is used sometimes inside gaming android applications or Mobile gallery apps. So it can be easily created through layout file, all we have to do is create another ");

                cardItems.add(cardItems1);


            }

            RecCardAdapter recCardAdapter = new RecCardAdapter(context, cardItems);
            holder.recyCard1.setLayoutManager(new LinearLayoutManager(context));
            holder.recyCard1.setAdapter(recCardAdapter);


        }
    }

    @Override
    public int getItemCount() {
        return mypojos.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        EditText title_1, description_1;

        LinearLayout cardItem;
        AppCompatImageButton imgbtn;
        // LinearLayout ll, ll1, ll2, ll3, ll4;
        RecyclerView recyCard1;
        TextView header;
        ImageView add_1;
        // List<CardItems> cardItems, cardItems1, cardItems2, cardItems3, cardItems4;


        public MyViewHolder(View itemView) {
            super(itemView);

            imgbtn = (AppCompatImageButton) itemView.findViewById(R.id.imgbtn);
            cardItem = (LinearLayout) itemView.findViewById(R.id.cardItem);
            header = (TextView) itemView.findViewById(R.id.header);


            title_1 = (EditText) itemView.findViewById(R.id.title_edt1);
            description_1 = (EditText) itemView.findViewById(R.id.des_edt1);
            add_1 = (ImageView) itemView.findViewById(R.id.but_add1);


            recyCard1 = (RecyclerView) itemView.findViewById(R.id.recy_cards1);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //recyCard1.setLayoutManager(new LinearLayoutManager(context));

                    // List<Mypojo>mypojos_new=new ArrayList<>();
//                    mypojos_new.addAll(mypojos);


                    if (mypojos.get(getAdapterPosition()).getSelected() == 0) {


                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);

                        }

                        mypojos.get(getAdapterPosition()).setSelected(1);

                        notifyItemRangeChanged(0, mypojos.size());
                    } else {

                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);

                        }

                        //  mypojos.get(getAdapterPosition()).setSelected(1);

                        notifyItemRangeChanged(0, mypojos.size());


                    }
                }

            });


            imgbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //recyCard1.setLayoutManager(new LinearLayoutManager(context));

                    // List<Mypojo>mypojos_new=new ArrayList<>();
//                    mypojos_new.addAll(mypojos);


                    if (mypojos.get(getAdapterPosition()).getSelected() == 0) {


                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);

                        }

                        mypojos.get(getAdapterPosition()).setSelected(1);

                        notifyItemRangeChanged(0, mypojos.size());
                    } else {

                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);

                        }

                        //  mypojos.get(getAdapterPosition()).setSelected(1);

                        notifyItemRangeChanged(0, mypojos.size());


                    }
                }

            });


        }

    }


    //adapter class of inside row
    public class RecCardAdapter extends RecyclerView.Adapter<RecCardAdapter.ViewHolderCard> {
        List<post_prpty_part3_innr_pojo> itemsList;
        Context context;

        public RecCardAdapter(Context context, List<post_prpty_part3_innr_pojo> cardItems) {
            this.itemsList = cardItems;
            this.context = context;
        }

        @Override
        public ViewHolderCard onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.cards_in_rec_item, parent, false);

            return new ViewHolderCard(view);
        }

        @Override
        public void onBindViewHolder(ViewHolderCard holder, int position) {
            post_prpty_part3_innr_pojo mycard = itemsList.get(position);
            holder.titletv.setText(mycard.getTitle());
            holder.destv.setText(mycard.getDescription());
        }

        @Override
        public int getItemCount() {
            return itemsList.size();
        }

        public class ViewHolderCard extends RecyclerView.ViewHolder {
            TextView titletv, destv;

            public ViewHolderCard(View itemView) {
                super(itemView);
                titletv = itemView.findViewById(R.id.title_tv);
                destv = itemView.findViewById(R.id.des_tv);

            }
        }
    }

}
