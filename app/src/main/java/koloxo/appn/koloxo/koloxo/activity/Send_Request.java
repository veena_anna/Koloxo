package koloxo.appn.koloxo.koloxo.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import koloxo.appn.koloxo.koloxo.R;

public class Send_Request extends AppCompatActivity {
TextView go_to_login;
RelativeLayout back_sendrq;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send__request);
        go_to_login=(TextView)findViewById(R.id.sendreq_back_to);
        back_sendrq=(RelativeLayout) findViewById(R.id.back_sendrq);
        go_to_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        back_sendrq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
