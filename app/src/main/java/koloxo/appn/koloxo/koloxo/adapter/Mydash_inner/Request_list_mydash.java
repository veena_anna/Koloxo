package koloxo.appn.koloxo.koloxo.adapter.Mydash_inner;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Added_properties;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Requsted_prsn_list;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 7/3/18.
 */

public class Request_list_mydash extends BaseAdapter {
    Context context;
    List<Side_mysearchobj> rowItems;
    Typeface Koloxo;

    public Request_list_mydash(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
        Koloxo = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Request_list_mydash.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

      if (convertView == null) {
          convertView = mInflater.inflate(R.layout.side_mydash_inr_rowrqst, null);;
        holder = new Request_list_mydash.ViewHolder();
           holder.txtDesc = (TextView) convertView.findViewById(R.id.reqst_cunt_id);
          holder.imageView = (ImageView) convertView.findViewById(R.id.Request_list_mydash_img);
          /* holder.txtTitle = (TextView) convertView.findViewById(R.id.title);

           holder.imageView = (ImageView) convertView.findViewById(R.id.icon);*/
        convertView.setTag(holder);

       }       else {
           holder = (Request_list_mydash.ViewHolder) convertView.getTag();

       }


      Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);

       // holder.txtDesc.setText(rowItem.getDesc());
       // holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
        holder.txtDesc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  Intent intent =new Intent(context,Requsted_prsn_list.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);*/
                Title.setText("Property Request");
                Title.setTypeface(Koloxo);
                getHpBaseActivity().pushFragments(new Requsted_prsn_list(),true,true);



            }
        });
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}