package koloxo.appn.koloxo.koloxo.SidemenuPages;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.MyProperty_revw;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.MyRental_prpty;
import koloxo.appn.koloxo.koloxo.fragments.BuyFrag;
import koloxo.appn.koloxo.koloxo.fragments.RentFrag;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;


public class MyHistory  extends Fragment {
    private TabLayout tabLayout;
    private ViewPager viewPager;

    TextView tabOne,tabTwo;
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("My History");
    }
    @SuppressLint("ResourceAsColor")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_history, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewPager);
        bottom_bar.setVisibility(View.VISIBLE);

        setupViewPager(viewPager);
        tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        bottom_bar.setVisibility(View.VISIBLE);
        /*tabLayout.setTabGravity(TabLayout.G);*/
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();

tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
    @Override
    public void onTabSelected(TabLayout.Tab tab) {
       View view1= tab.getCustomView();
      // tabOne.setTextColor(R.color.colorPrimaryDark);
TextView text=(TextView)view1.findViewById(R.id.tab_tittle);
text.setTextColor(ContextCompat.getColor(getActivity(), R.color.black));

    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        View view1= tab.getCustomView();
        TextView text=(TextView)view1.findViewById(R.id.tab_tittle);
        text.setTextColor(ContextCompat.getColor(getActivity(), R.color.your_unselected_text_color));
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }
});


        return view;
    }

    private void setupTabIcons() {

        tabOne = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.customtabs, null);

        Spannable span = new SpannableString("My Rental Property");
        span.setSpan(new RelativeSizeSpan(0.6f), 10, 18, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tabOne.setText(span);
        /* tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_tab_favourite, 0, 0);*/
        tabLayout.getTabAt(0).setCustomView(tabOne);


        tabTwo = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.customtabs, null);
        tabTwo.setText("My Property");
        Spannable span1 = new SpannableString("My Property Review");
        span1.setSpan(new RelativeSizeSpan(0.6f), 12, 18, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        tabTwo.setText(span1);
        tabLayout.getTabAt(1).setCustomView(tabTwo);
        //tabLayout.getTabAt(1).setCustomView(tabTwo1);

    }


    private void setupViewPager(ViewPager viewPager) {
        MyHistory.ViewPagerAdapter adapter = new MyHistory.ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new MyRental_prpty(), "My Rental Property");
        adapter.addFragment(new MyProperty_revw(), "My Property Review");

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
