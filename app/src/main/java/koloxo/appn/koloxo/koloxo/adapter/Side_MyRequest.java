package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.DialogPop_rating;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Request_list;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Requsted_prsn_list;
import koloxo.appn.koloxo.koloxo.SidemenuPages.Myreq_innr.Request_accepted_list;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 7/3/18.
 */

public class Side_MyRequest extends BaseAdapter {
    Context context;
    List<Side_mysearchobj> rowItems;
    Typeface Koloxo;
    DialogPop_rating dialogPop_rating;
    public Side_MyRequest(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
        dialogPop_rating=new DialogPop_rating(context);
        Koloxo = Typeface.createFromAsset(context.getAssets(), "fonts/OpenSans-Regular.ttf");
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        RelativeLayout requst_accpt,requst_rate;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Side_MyRequest.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.side_rowmyrequest, null);
            holder = new Side_MyRequest.ViewHolder();
            holder.requst_accpt=(RelativeLayout)convertView.findViewById(R.id.requst_accpt);
            holder.requst_rate=(RelativeLayout)convertView.findViewById(R.id.requst_rate);
//            holder.txtDesc = (TextView) convertView.findViewById(R.id.desc);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
           holder.imageView = (ImageView) convertView.findViewById(R.id.side_rowmyrequest_img);
            convertView.setTag(holder);
        }
        else {
            holder = (Side_MyRequest.ViewHolder) convertView.getTag();
        }

        Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);
        holder.imageView.setImageResource(rowItem.getImageId());

        holder.requst_accpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Title.setText("Property Request");
                Title.setTypeface(Koloxo);
                getHpBaseActivity().pushFragments(new Request_accepted_list(),true,true);


            }
        });
        holder.requst_rate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPop_rating.initiatePopupWindow_Lawyers(context);
            }
        });

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}