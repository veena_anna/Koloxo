package koloxo.appn.koloxo.koloxo.Objects;

/**
 * Created by appzoc-php on 21/3/18.
 */

public class post_prpty_part3_innr_pojo {
    String title;
    String description;

    public post_prpty_part3_innr_pojo() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }}
