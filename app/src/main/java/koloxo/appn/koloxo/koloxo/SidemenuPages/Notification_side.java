package koloxo.appn.koloxo.koloxo.SidemenuPages;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyHistory_inner.MyRental_prpty;
import koloxo.appn.koloxo.koloxo.adapter.Notify_adapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Notification_side extends Fragment implements AdapterView.OnItemClickListener {
    ListView Notify_list;
    MyRental_prpty request_list_mydash;
    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.splash_pic_1,
            R.drawable.splash_pic_2, R.drawable.splash_pic_3, R.drawable.splash_pic_1 };
    List<Side_mysearchobj> rowItems;


    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Notification");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_notification, container, false);
        Notify_list = (ListView) view.findViewById(R.id.list_notification);
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);

        }


        Notify_adapter adapter = new Notify_adapter(getActivity(), rowItems);
        Notify_list.setAdapter(adapter);
        Notify_list.setOnItemClickListener(Notification_side.this);

        /*//back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });*/

        return view;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
       /* Toast toast = Toast.makeText(getActivity(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();*/
    }
}