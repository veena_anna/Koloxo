package koloxo.appn.koloxo.koloxo.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;
import koloxo.appn.koloxo.koloxo.Objects.post_prpty_part3_innr_pojo;
import koloxo.appn.koloxo.koloxo.R;

/**
 * Created by appzoc-php on 21/3/18.
 */

public class Post_proprty_part6 extends RecyclerView.Adapter<Post_proprty_part6.RecViewHolder> {
    Context context;
    List<Pojo_postprpty_part3> mypojos;
    int img[] = {R.drawable.img_prpty, R.drawable.img_prpty};
    List<post_prpty_part3_innr_pojo> cardItems = new ArrayList<>();


    public Post_proprty_part6(Context context, List<Pojo_postprpty_part3> mypojos) {
        this.context = context;
        this.mypojos = mypojos;

    }

    @Override
    public RecViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.post_porpty_part6_adaptr_row, parent, false);

        return new RecViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecViewHolder holder, int position) {
        Pojo_postprpty_part3 mypojo = mypojos.get(position);
        holder.header.setText(mypojo.getName());

        if (mypojos.get(position).getSelected() == 0) {
            holder.linearLayout.setVisibility(View.GONE);
            holder.imgbtn.setImageResource(R.drawable.arrowdown);
        } else {
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.imgbtn.setImageResource(R.drawable.arrowup);

            // List<CardItems>cardItems=new ArrayList<>();


            PropertyChildAdapter recCardAdapter = new PropertyChildAdapter(context);
            holder.recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
            holder.recyclerView.setAdapter(recCardAdapter);


        }
    }

    @Override
    public int getItemCount() {
        return mypojos.size();
    }

    public class RecViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout linearLayout;
        AppCompatImageButton imgbtn;
        TextView header;
        RecyclerView recyclerView;

        public RecViewHolder(View itemView) {
            super(itemView);
            linearLayout = (RelativeLayout) itemView.findViewById(R.id.property_img_child);
            imgbtn = (AppCompatImageButton) itemView.findViewById(R.id.imgbtn);
            header = (TextView) itemView.findViewById(R.id.header);
            recyclerView = (RecyclerView) itemView.findViewById(R.id.add_img_rec);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (mypojos.get(getAdapterPosition()).getSelected() == 0) {


                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);

                        }
                        mypojos.get(getAdapterPosition()).setSelected(1);

                        notifyItemRangeChanged(0, mypojos.size());
                    } else {

                        for (int i = 0; i < mypojos.size(); i++
                                ) {

                            mypojos.get(i).setSelected(0);
                        }
                        notifyItemRangeChanged(0, mypojos.size());
                    }
                }

            });
        }
    }


    public class PropertyChildAdapter extends RecyclerView.Adapter<PropertyChildAdapter.ViewHolder> {
        Context context;
        int img[] = {R.drawable.img_prpty, R.drawable.img_prpty};

        public PropertyChildAdapter(Context context) {
            this.context = context;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(context);
            View view = inflater.inflate(R.layout.img_item, parent, false);

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.img.setImageResource(img[position]);
        }

        @Override
        public int getItemCount() {
            return 2;
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            ImageView img;

            public ViewHolder(View itemView) {
                super(itemView);
                img = itemView.findViewById(R.id.img_uploaded);
            }
        }
    }

}

