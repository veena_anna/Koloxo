package koloxo.appn.koloxo.koloxo.activity;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Pojo_postprpty_part3;

import koloxo.appn.koloxo.koloxo.adapter.Post_prpty_part3;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;

public class AddingPrprty_part3 extends android.support.v4.app.Fragment {
    RecyclerView recyclerView;
    ImageView add_prpty_nxt3;

    String[] header = { "Bedroom", "Kitchen", "Guest Room", "Balcony","OtherFurniture" };
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("Furnitures");
    }
   /* @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_prprty_part3);*/
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
                            Bundle savedInstanceState) {
       View view=inflater.inflate(R.layout.activity_adding_prprty_part3, container, false);

            recyclerView=view.findViewById(R.id.recyclerView);
        add_prpty_nxt3=view.findViewById(R.id.add_prpty_nxt3);
       bottom_bar.setVisibility(View.INVISIBLE);


        add_prpty_nxt3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent intent=new Intent(getActivity(),AddingPrpty_part4.class);
                startActivity(intent);*/
                Title.setText("Add Placemarks");
                getHpBaseActivity().pushFragments(new AddingPrpty_part4(),true,true);
            }
        });

            List<Pojo_postprpty_part3> mypojos=new ArrayList<>();
            for(int i=0;i<header.length;i++)
            {
                Pojo_postprpty_part3 mypojo=new Pojo_postprpty_part3();
                mypojo.setName(header[i]);
                mypojo.setSelected(0);
                mypojos.add(mypojo);
            }

            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(new Post_prpty_part3(getActivity(),mypojos));

return view;
    }


    }

