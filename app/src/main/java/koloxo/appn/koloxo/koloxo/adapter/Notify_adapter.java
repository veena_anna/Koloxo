package koloxo.appn.koloxo.koloxo.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import koloxo.appn.koloxo.koloxo.BaseActivity;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MyDash_innerpge.Requsted_prsn_list;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;

/**
 * Created by appzoc-php on 9/3/18.
 */

public class Notify_adapter extends BaseAdapter

{
    Context context;
    List<Side_mysearchobj> rowItems;


    public Notify_adapter(Context context, List<Side_mysearchobj> items) {
        this.context = context;
        this.rowItems = items;
    }

    /*private view holder class*/
    private class ViewHolder {
        ImageView imageView;
        TextView txtTitle;
        TextView txtDesc;
        CardView nofity_card;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        Notify_adapter.ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
       if (convertView == null) {
        convertView = mInflater.inflate(R.layout.side_notify_row, parent,false);
        holder = new Notify_adapter.ViewHolder();
            holder.nofity_card = (CardView) convertView.findViewById(R.id.nofity_card);
//            holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
//            holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
        convertView.setTag(holder);
        }
       else {            holder = (Notify_adapter.ViewHolder) convertView.getTag();
        }
holder.nofity_card.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {
        Title.setText("Property Request");
        BaseActivity.getHpBaseActivity().pushFragments(new Requsted_prsn_list(),true,true);
    }
});
        // Side_mysearchobj rowItem = (Side_mysearchobj) getItem(position);

       /* holder.txtDesc.setText(rowItem.getDesc());
        holder.txtTitle.setText(rowItem.getTitle());
        holder.imageView.setImageResource(rowItem.getImageId());
*/
        return convertView;
    }

    @Override
    public int getCount() {
        return rowItems.size();
    }

    @Override
    public Object getItem(int position) {
        return rowItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return rowItems.indexOf(getItem(position));
    }
}