package koloxo.appn.koloxo.koloxo.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.MainActivity;
import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.SideMenu.SideMenuPage;
import koloxo.appn.koloxo.koloxo.SidemenuPages.MySearches;
import koloxo.appn.koloxo.koloxo.adapter.Side_mysearches;

import static koloxo.appn.koloxo.koloxo.BaseActivity.getHpBaseActivity;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.Title;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;

public class MySearch_frm_home extends Activity implements AdapterView.OnItemClickListener {
    ListView mysearch_list;
    Side_mysearches side_mysearches;
    Button notify_token,notify_token2;
    ImageView bottom_ic_view,bottom_ic_notlogin,settings_button;
    RelativeLayout   back_srch_main;
    SideMenuPage sideMenuPage;
    TextView home_side,mydash,myreq,myhis,mynotify,mywish,mysearches_side;
    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.splash_pic_1,
            R.drawable.splash_pic_2, R.drawable.splash_pic_3, R.drawable.splash_pic_1 };
    List<Side_mysearchobj> rowItems;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_search_frm_home);

       sideMenuPage=new SideMenuPage(MySearch_frm_home.this);

        sideMenuPage.setupSlidemenu(MySearch_frm_home.this);

        sideMenuPage.setAnimations(MySearch_frm_home.this);
        initialize_var();
        home_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
                sideMenuPage.mDrawerLayout.closeDrawers();

            }
        });

        mysearches_side.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MySearch_frm_home.this, MySearch_frm_home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                sideMenuPage.mDrawerLayout.closeDrawers();

                Toast.makeText(getApplicationContext(),"You clicked mysearches_side",Toast.LENGTH_SHORT).show();
            }
        });

        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++)

        {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);
        }

        Side_mysearches adapter = new Side_mysearches(getApplicationContext(), rowItems);
        mysearch_list.setAdapter(adapter);
        mysearch_list.setOnItemClickListener(MySearch_frm_home.this);

 //back arrow case
        back_srch_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                sideMenuPage.mDrawerLayout.closeDrawers();
            }
        });

    }
    public void initialize_var()
    {
        mysearch_list = (ListView)findViewById(R.id.list_mysearches);
        settings_button=(ImageView)findViewById(R.id.settings_button);
        back_srch_main=(RelativeLayout)findViewById(R.id.back_srch_main);

        bottom_ic_view=(ImageView)findViewById(R.id.bt_b4);
        bottom_ic_notlogin=(ImageView) findViewById(R.id.bt_b1);
        bottom_ic_view.setImageResource(R.drawable.ic_eye_main);
        bottom_ic_notlogin.setImageResource(R.drawable.ic_profile_main);

        notify_token=(Button)findViewById(R.id.landing_token1);
        notify_token2=(Button)findViewById(R.id.landing_token2);

        notify_token.setVisibility(View.INVISIBLE);
        notify_token2.setVisibility(View.INVISIBLE);

        //for sidemenu
         home_side=(TextView)findViewById(R.id.side_l3_home);
       mydash=(TextView)findViewById(R.id.side_l3_dash);
         myreq=(TextView)findViewById(R.id.side_l3_rqt);
        myhis=(TextView)findViewById(R.id.side_l3_his);
       mynotify=(TextView)findViewById(R.id.side_l3_notify);
        mywish=(TextView)findViewById(R.id.side_l3_wish);
       mysearches_side=(TextView)findViewById(R.id.side_l3_srch);
        mydash.setClickable(false);
        myreq.setClickable(false);
        myhis.setClickable(false);
        mynotify.setClickable(false);
        mywish.setClickable(false);

        mydash.setTextColor(Color.parseColor("#8F73EB"));
        myreq.setTextColor(Color.parseColor("#8F73EB"));
        myhis.setTextColor(Color.parseColor("#8F73EB"));
        mynotify.setTextColor(Color.parseColor("#8F73EB"));
        mywish.setTextColor(Color.parseColor("#8F73EB"));
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        Toast toast = Toast.makeText(getApplicationContext(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
    //botton icon clicks
    public void Home(View view)
    {

        finish();
        sideMenuPage.mDrawerLayout.closeDrawers();
        Toast.makeText(getApplicationContext(),"You clicked Home",Toast.LENGTH_SHORT).show();
    }
    public void Add_P(View view)
    {
        Intent intent=new Intent(MySearch_frm_home.this, AddingProperty.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        Toast.makeText(getApplicationContext(),"You clicked Add Property",Toast.LENGTH_SHORT).show();
    }
    public void Search_Land(View view)
    {
        Toast.makeText(getApplicationContext(),"You clicked search",Toast.LENGTH_SHORT).show();
    }
    public void Chat(View view)
    {
       /* MySearches mySearches=new MySearches();
        Title.setText("Recently Viewed");
        Title.setTypeface(Koloxo);
        pushFragments(mySearches,true,true);*/
        Intent intent=new Intent(MySearch_frm_home.this, MySearch_frm_home.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        Toast.makeText(getApplicationContext(),"You clicked view",Toast.LENGTH_SHORT).show();
    }
    public void Notify(View view)
    {
        Intent intent=new Intent(MySearch_frm_home.this,LoginPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
       // Toast.makeText(getApplicationContext(),"You clicked notify",Toast.LENGTH_SHORT).show();
    }

}
