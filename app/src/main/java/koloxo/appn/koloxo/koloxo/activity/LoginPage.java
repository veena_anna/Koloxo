package koloxo.appn.koloxo.koloxo.activity;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import koloxo.appn.koloxo.koloxo.Activitybase;
import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.Dialoghelper;
import koloxo.appn.koloxo.koloxo.Page_aftr_fblogin;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.SideMenu.SideMenuPage;
import koloxo.appn.koloxo.koloxo.Webserives.APIs;
import koloxo.appn.koloxo.koloxo.Webserives.PojoitemLogin;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.MainActivity.android_id;

public class LoginPage extends Activitybase {
    //loginpage close
    RelativeLayout login_closing;
    TextView sign_up,forgot_password;
    SpannableString spannableString;
    EditText login_card1_edt1, login_card1_edt2;

    //for web set
    APIs apIs;
    ImageView login_check;
    Session_class session_class;
    String id, name,propic,first_name,last_name;
    CallbackManager callbackManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(LoginPage.this);
        setContentView(R.layout.activity_login_page);
        initialize_var();
        session_class = new Session_class(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {

//                        Toast.makeText(LoginPage.this, "Login Success", Toast.LENGTH_SHORT).show();
//                        Intent intent=new Intent(LoginPage.this, Page_aftr_fblogin.class);
//                        startActivity(intent);
//                        Profile profile = Profile.getCurrentProfile();
//                        Toast.makeText(LoginPage.this, "F name: "+profile.getFirstName(), Toast.LENGTH_SHORT).show();
//                        Toast.makeText(LoginPage.this, "l name: "+profile.getLastName(), Toast.LENGTH_SHORT).show();
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());

                                        // Application code
                                        try {
                                            //String email = object.getString("email");
                                            id = object.getString("id"); // 01/31/1980 format
                                            name = object.getString("name");
                                            first_name=object.getString("first_name");
                                            last_name=object.getString("last_name");

                                            //for pro pic
                                            String urlid = object.getString("id");
                                            Uri.Builder builder = new Uri.Builder();
                                            builder.scheme("https")
                                                    .authority("graph.facebook.com")
                                                    .appendPath(urlid)
                                                    .appendPath("picture")
                                                    .appendQueryParameter("width", "1000")
                                                    .appendQueryParameter("height", "1000");

                                            Uri propic1;
                                            propic1 = builder.build();
                                            propic = propic1.toString();
                                            Intent intent=new Intent(LoginPage.this, Page_aftr_fblogin.class);
                                            intent.putExtra("face_id",id);
                                            intent.putExtra("face_firstname",first_name);
                                            intent.putExtra("face_lastname",last_name);
                                            intent.putExtra("face_img",propic);
                                              startActivity(intent);
                                           // Toast.makeText(LoginPage.this, "id: " + id + "\n name: " + name, Toast.LENGTH_LONG).show();
                                            Log.v("LoginActivity", id+name);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        request.executeAsync();

//
                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(LoginPage.this, "Login Cancel", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(FacebookException error) {
                        Toast.makeText(LoginPage.this, error.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });


        login_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //apIs = ApiUtils.getSOService();

                if (!(login_card1_edt1.getText().length() == 0) && !(login_card1_edt2.getText().length() == 0)) {
                    final Call<JsonObject> loginCall = new Retrofit_Helper().getRetrofitBuilder().getDetails(login_card1_edt1.getText().toString(), login_card1_edt2.getText().toString(),"android",android_id);


                    loginCall.enqueue(new ResponseHandler(true, LoginPage.this, new ResponseCallback() {
                        @Override
                        public void getResponse(int code, JsonObject jsonObject) {
                            Log.v("LoginActivity", jsonObject.toString());
                            PojoitemLogin loginData = new GsonBuilder().create().fromJson(jsonObject, PojoitemLogin.class);
                            if (loginData.getErrorCode().equals("0")) {

                                finish();
                              session_class.createUserRegisterSession(loginData.getData().getUserId(), loginData.getData().getFirstName(), loginData.getData().getLastName(), loginData.getData().getMobileNumber(), loginData.getData().getEmail(), loginData.getData().getImage(), MyConstants.LoggedIn.TRUE,loginData.getData().getImage());
                                Log.d("Koloxo_login", "onResponse: message " + loginData.getMessage());
                               // Toast.makeText(LoginPage.this, "Login Success", Toast.LENGTH_SHORT).show();

                            }
                            else if (loginData.getErrorCode().equals("1"))
                            {
                                Dialoghelper.showSnackbar(LoginPage.this, loginData.getMessage());
                            }
                            // session_class.createUserLoginSession(response.body().getData().getUserId());
                        }


                        @Override
                        public void getError(Call<JsonObject> call, String message) {
                            Dialoghelper.showSnackbar(LoginPage.this, message);
                        }
                    }, loginCall));



                } else {
                    login_card1_edt1.setError("Please enter email ID");
                    login_card1_edt2.setError("Please enter Password");
                }
            }
        });
        login_closing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(LoginPage.this,RegisterPage.class);
                startActivity(intent);
                finish();
            }
        });


    }
    public void initialize_var()
    {
        login_closing=(RelativeLayout)findViewById(R.id.login_l1_close);
        sign_up=(TextView)findViewById(R.id.sign_up_id);
        login_check = (ImageView) findViewById(R.id.login_check);
        login_card1_edt1 = (EditText) findViewById(R.id.login_card1_edt1);
        login_card1_edt2 = (EditText) findViewById(R.id.login_card1_edt2);
        forgot_password = (TextView) findViewById(R.id.forgot_password_txt_login1);
        spannableString = new SpannableString("Sign up");
        spannableString.setSpan(new UnderlineSpan(), 0, spannableString.length(), 0);
        sign_up.setText(spannableString);
        forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ForgetPassword.class);
                startActivity(intent);

            }
        });
    }
    public void FB_LOGIN(View view) {

        LoginManager.getInstance().logInWithReadPermissions(LoginPage.this, Arrays.asList("public_profile", "user_friends", "email"));
finish();

        // Toast.makeText(getApplicationContext(),"Login to facebook",Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
