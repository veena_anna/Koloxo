package koloxo.appn.koloxo.koloxo.SidemenuPages;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage;
import koloxo.appn.koloxo.koloxo.Objects.Side_mysearchobj;
import koloxo.appn.koloxo.koloxo.R;
import koloxo.appn.koloxo.koloxo.adapter.Side_MyRequest;
import koloxo.appn.koloxo.koloxo.adapter.Side_mysearches;

import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.back_arrow;
import static koloxo.appn.koloxo.koloxo.MainLanding.MainLandPage.bottom_bar;


public class MyRequest extends Fragment implements AdapterView.OnItemClickListener {
    ListView MyRequest_list;
    Side_MyRequest side_mysearches;
    public static final String[] titles = new String[] { "Strawberry",
            "Banana", "Orange", "Mixed" };

    public static final String[] descriptions = new String[] {
            "It is an aggregate accessory fruit",
            "It is the largest herbaceous flowering plant", "Citrus Fruit",
            "Mixed Fruits" };
    public static final Integer[] images = { R.drawable.img_prpty,
            R.drawable.img_prpty, R.drawable.img_prpty, R.drawable.img_prpty };
    List<Side_mysearchobj> rowItems;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
    @Override
    public void onResume() {
        super.onResume();
        ((MainLandPage)getActivity()).set_Title("My Request");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view  = inflater.inflate(R.layout.fragment_myrequest, container, false);
        MyRequest_list=(ListView)view.findViewById(R.id.list_MyRequest);
        bottom_bar.setVisibility(View.VISIBLE);
        rowItems = new ArrayList<Side_mysearchobj>();
        for (int i = 0; i < titles.length; i++) {
            Side_mysearchobj item = new Side_mysearchobj(images[i], titles[i], descriptions[i]);
            rowItems.add(item);
        }

        Side_MyRequest adapter = new Side_MyRequest(getActivity(), rowItems);
        MyRequest_list.setAdapter(adapter);
        MyRequest_list.setOnItemClickListener(MyRequest.this);

        //back arrow case
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        return view;
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
       /* Toast toast = Toast.makeText(getContext(),
                "Item " + (position + 1) + ": " + rowItems.get(position),
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();*/
    }


}

