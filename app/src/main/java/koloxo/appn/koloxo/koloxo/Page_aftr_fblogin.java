package koloxo.appn.koloxo.koloxo;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.foloww.webservicehelper.ResponseCallback;
import com.foloww.webservicehelper.ResponseHandler;
import com.foloww.webservicehelper.Retrofit_Helper;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;
import koloxo.appn.koloxo.koloxo.Commons.MyConstants;
import koloxo.appn.koloxo.koloxo.Session_class.Session_class;
import koloxo.appn.koloxo.koloxo.Webserives.FacebookData;
import koloxo.appn.koloxo.koloxo.Webserives.PojoFacebook;
import koloxo.appn.koloxo.koloxo.Webserives.PojoitemLogin;
import koloxo.appn.koloxo.koloxo.activity.LoginPage;
import koloxo.appn.koloxo.koloxo.activity.Otp_Screenpage;
import koloxo.appn.koloxo.koloxo.activity.Register_success_page;
import koloxo.appn.koloxo.koloxo.adapter.Spinner_Adapter;
import retrofit2.Call;

import static koloxo.appn.koloxo.koloxo.MainActivity.android_id;

public class Page_aftr_fblogin extends Activitybase {
    String[] List_country;
    Spinner country;
    RelativeLayout back_fb1_main;
    ImageView back_fb1_main1;
    Button submit_detail_aftrfb;
    String facebook_id,facebook_name,facebook_imgpropic,facebook_firstname,facebook_lastname;
    Session_class session_class;
    EditText text_name_of_user,text_name_of_user2,text_name_of_user3,text_name_of_user4;
    String selected_country;
    String selectedText;
    CircleImageView page_aftr_fb_propic;
    ProgressBar progress_fb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page_aftr_fblogin);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        List_country= new String[]{"Country of Residence","Dubai","Dubai"};
        session_class=new Session_class(Page_aftr_fblogin.this);
        country=(Spinner)findViewById(R.id.text_name_of_spiner);
        submit_detail_aftrfb=(Button) findViewById(R.id.submit_detail_aftrfb);
        back_fb1_main=(RelativeLayout) findViewById(R.id.back_fb_reg);
        back_fb1_main1=(ImageView) findViewById(R.id.back_fb1_main1);
        page_aftr_fb_propic=(CircleImageView) findViewById(R.id.page_aftr_fb_propic);

        text_name_of_user=findViewById(R.id.text_name_of_user);
        text_name_of_user2=findViewById(R.id.text_name_of_user2);
        text_name_of_user3=findViewById(R.id.text_name_of_user3);
        text_name_of_user4=findViewById(R.id.text_name_of_user4);
        progress_fb=findViewById(R.id.progress_fb);

        Intent intent = getIntent();
        facebook_id=intent.getExtras().getString("face_id");
        facebook_firstname=intent.getExtras().getString("face_firstname");
        facebook_lastname=intent.getExtras().getString("face_lastname");
        facebook_imgpropic=intent.getExtras().getString("face_img");
        Picasso.with(Page_aftr_fblogin.this).load(facebook_imgpropic).into(page_aftr_fb_propic,
                new Callback() {
            @Override
            public void onSuccess() {
                progress_fb.setVisibility(View.GONE);
            }

                    @Override
                    public void onError() {

                    }
                });

       // Toast.makeText(this, facebook_imgpropic, Toast.LENGTH_SHORT).show();
        text_name_of_user.setText(facebook_firstname);
        text_name_of_user2.setText(facebook_lastname);
//        ArrayAdapter<String> staticAdapter = new ArrayAdapter<String>(Page_aftr_fblogin.this,
//                android.R.layout.simple_spinner_item, List_country);
        // Specify the layout to use when the list of choices appears
//        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        country.setAdapter(new Spinner_Adapter(this,List_country));
        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedText=List_country[i];
                if(!selectedText.equals("Country of Residence")) {
                    selected_country=selectedText;
                    //  Toast.makeText(getApplicationContext(), selected_country, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        back_fb1_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        back_fb1_main1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Page_aftr_fblogin.this.finish();
            }
        });
        submit_detail_aftrfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              String frstname=  text_name_of_user.getText().toString();
                String lastnme=  text_name_of_user2.getText().toString();
                String emailid= text_name_of_user3.getText().toString();
                if (emailValidator(emailid) == true) {

                } else {
                    text_name_of_user3.setError("Invalid Email");
                }

                String mobileno= text_name_of_user4.getText().toString();
                if(isValidMobile(mobileno)==true)
                {

                }
                else {
                    text_name_of_user4.setError("Invalid Mobile");
                }
                if (isValidMobile(mobileno)==true && emailValidator(emailid) == true && !(text_name_of_user.getText().length() == 0) && !(text_name_of_user2.getText().length() == 0)&& !(text_name_of_user3.getText().length() == 0)&& !(text_name_of_user4.getText().length() == 0))
                {
                    final Call<JsonObject> facebookcall = new Retrofit_Helper().getRetrofitBuilder().getFacebook_login(frstname, lastnme, mobileno, emailid, selected_country,"android",android_id);


                    facebookcall.enqueue(new ResponseHandler(true, Page_aftr_fblogin.this, new ResponseCallback() {
                        @Override
                        public void getResponse(int code, JsonObject jsonObject) {
                            Log.v("LoginActivity", jsonObject.toString());
                            PojoFacebook facebookda = new GsonBuilder().create().fromJson(jsonObject, PojoFacebook.class);
                            if (facebookda.getErrorCode().equals("0")) {
                                session_class.createUserRegisterSession(facebookda.getData().getUserId(), facebookda.getData().getFirstName(), facebookda.getData().getLastName(), facebookda.getData().getMobileNumber(), facebookda.getData().getEmail(), facebookda.getData().getMobileNumber(), MyConstants.LoggedIn.TRUE,facebookda.getData().getImage());
                                Intent intent = new Intent(getApplicationContext(), Register_success_page.class);
                                startActivity(intent);
                                finish();
                                Log.d("facx", "ch: error code (0) " + facebookda.getErrorCode() + " message : " + facebookda.getMessage());
                            }
                            Log.d("Koloxo_login", "onResponse: message " + facebookda.getMessage());
                            //Toast.makeText(Page_aftr_fblogin.this, "Login Success", Toast.LENGTH_SHORT).show();
                            // session_class.createUserLoginSession(response.body().getData().getUserId());
                        }


                        @Override
                        public void getError(Call<JsonObject> call, String message) {
                            Dialoghelper.showSnackbar(Page_aftr_fblogin.this, message);
                        }
                    }, facebookcall));


                }
               else{

                    if (text_name_of_user.getText().length() == 0)
                        text_name_of_user.setError("First name required!");
                    if (text_name_of_user2.getText().length() == 0)
                        text_name_of_user2.setError("Last name required!");
                    if (text_name_of_user4.getText().length() == 0)
                        text_name_of_user4.setError("Mobileno. required!");
                    if (text_name_of_user3.getText().length() == 0)
                        text_name_of_user3.setError("Email required!");

                    if(selectedText.equals("Country of Residence"))
                        Dialoghelper.showSnackbar(Page_aftr_fblogin.this, "Select Country of Residence");
                }

            }

        });

    }

    public boolean emailValidator(String email)
    {
        Pattern pattern;
        Matcher matcher;
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();
    }
    private boolean isValidMobile(String phone) {
        boolean check=false;
        if(!Pattern.matches("[a-zA-Z]+", phone)) {
            if(phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
                text_name_of_user4.setError("Not Valid Number");
            } else {
                check = true;
            }
        } else {
            check=false;
        }
        return check;
    }


}
