package com.foloww.webservicehelper;

import com.google.gson.JsonObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by az-sys on 19/8/17.
 */

public interface Web_Interface {

    @FormUrlEncoded
    @POST("login")
    Call<JsonObject> getDetails(@Field("email") String email,

                                @Field("password") String pass,
                                @Field("device_type") String device_type,
                                @Field("device_token") String device_token);

    @POST("otp_verify")
    Call<JsonObject> getOTPVerify(@Field("email") String email,

                                @Field("otp") String otp);
    @FormUrlEncoded
    @POST("forgot_password")
    Call<JsonObject> getForgetpass(@Field("email") String email,
                                   @Field("device_type") String device_type,
                                   @Field("device_token") String device_token);

    @FormUrlEncoded
    @POST("registration")
    Call<JsonObject> getRegistartion(@Field("first_name") String first_name,
                                     @Field("last_name") String last_name,
                                     @Field("mobile_number") String mobile_number,
                                     @Field("email") String email,
                                     @Field("password") String password,
                                     @Field("confirm_password") String confirm_password,
                                     @Field("country_residence") String country_residence,
                                     @Field("device_type") String device_type,
                                     @Field("device_token") String device_token);
    @FormUrlEncoded
    @POST("add_property_detail")
    Call<JsonObject> getAddPrptyBuy1(@Field("user_id") String user_id,
                                     @Field("property_type") String property_type,
                                     @Field("property_name") String property_name,
                                     @Field("age_building") String age_building,
                                     @Field("elevator") String elevator,
                                     @Field("floor_no") String floor_no,
                                     @Field("Price") String Price,
                                     @Field("deposit") String deposit,
                                     @Field("reputation") String reputation,
                                     @Field("building_name") String building_name,
                                     @Field("type") String type);
    @FormUrlEncoded
    @POST("resend_otp")
    Call<JsonObject> getResentotp(@Field("email") String email);


    @GET("dashboard/{id}")
    Call<JsonObject> getDasboard(@Path("id") String id);


    @GET("getmy_profile/{user_id}")
    Call<JsonObject> getProfilePage(@Path("user_id") String user_id);


    @FormUrlEncoded
    @POST("facebook_login")
    Call<JsonObject> getFacebook_login(@Field("first_name") String first_name,
                                       @Field("last_name") String last_name,
                                       @Field("mobile_number") String mobile_number,
                                       @Field("email") String email,
                                       @Field("country_residence") String country_residence,
                                       @Field("device_type") String device_type,
                                       @Field("device_token") String device_token);


    @GET("terms_condition")
    Call<JsonObject> getterms();

    @GET("get_countyresidence")
    Call<JsonObject> getCountry();

    @GET("get_postpropertydetail")
    Call<JsonObject>getPostpropertydetails();



}
