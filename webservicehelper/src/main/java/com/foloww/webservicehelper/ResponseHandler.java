package com.foloww.webservicehelper;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.gson.JsonObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by az-sys on 19/8/17.
 */

public class ResponseHandler implements Callback<JsonObject> {

    Context context;
    ResponseCallback web_interface;
    Call<JsonObject> jsonObjectCall;


    ProgressDialog progressDialog;

    boolean showprogress;


    public ResponseHandler(boolean showprogress, Context context, ResponseCallback web_interface, Call<JsonObject> jsonObjectCall) {
        this.context = context;
        this.web_interface = web_interface;
        this.jsonObjectCall = jsonObjectCall;
        this.showprogress = showprogress;

        // if(!jsonObjectCall.request().url().toString().contains("api/search/listing")) {

        if (showprogress) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setMessage("Loading.....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        Log.v("Request", jsonObjectCall.request().url().toString());

    }


    @Override
    public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
        Log.d("responseXXX", "onResponse:  code : " + response.code());
        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        switch (response.code()) {
            case Responsecode.ok:
                if (web_interface != null) {
                    if (response.body() != null) {

                        web_interface.getResponse(response.code(), response.body());
                    }
                }

                break;

            case Responsecode.server_error:

                // showalert("Server Error");

                String a = convertStreamToString(response.errorBody().byteStream());
                Log.d("responseXXX error : ", a);

                web_interface.getError(call, "Can't Connect with server");
                break;

            case Responsecode.notfound:

                if (web_interface != null) {
                    if (response.body() != null) {
                        web_interface.getResponse(response.code(), response.body());
                    }
                }

                break;
        }

    }

    @Override
    public void onFailure(Call<JsonObject> call, Throwable t) {

        if (progressDialog != null) {
            progressDialog.dismiss();
        }

        if (!Retrofit_Helper.isNetworkAvailable(context)) {


            showalert("No Internet Connection");

        } else {


            //  showalert("Can't Connect with server");

            web_interface.getError(call, "Can't Connect with server");


        }

    }


    private void showalert(String message) {

        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Koloxo");
            builder.setMessage(message);
            builder.setCancelable(false);
            builder.setPositiveButton("Retry", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();

                    jsonObjectCall.clone().enqueue(new ResponseHandler(showprogress, context, web_interface, jsonObjectCall));

                }
            });

            builder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                    dialog.dismiss();


                }
            });


            builder.show();
        } catch (Exception e) {

        }
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
