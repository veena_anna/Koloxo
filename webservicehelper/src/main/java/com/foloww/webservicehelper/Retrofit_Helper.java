package com.foloww.webservicehelper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by az-sys on 19/8/17.
 */

public class Retrofit_Helper {

    public static String base_url = "http://app.appzoc.com/koloxo/api/";

    public static String image_base_url = "http://app.appzoc.com/ExhibizWeb/backend/storage/app/";


    public Retrofit_Helper() {
    }

    public Web_Interface getRetrofitBuilder() {


        OkHttpClient.Builder client = new OkHttpClient().newBuilder();


        //Log.i("url_base34",base_url+","+web_interface);

        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

               String auth= Credentials.basic("koloxo", "123456");
                Log.e("Auth",auth);

                Request.Builder builder = original.newBuilder()
                        .header("Authorization", Credentials.basic("koloxo", "123456"));

                Request request = builder.build();
                return chain.proceed(request);
            }
        };

        client.addInterceptor(interceptor);


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create()).client(client.build())


                .build();

        Web_Interface web_interface = retrofit.create(Web_Interface.class);

        return web_interface;


    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


}
